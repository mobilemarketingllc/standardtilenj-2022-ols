<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

	$post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );

		$store_shortname       = get_post_meta( $queried_object->ID, 'wpsl_store_shortname', true );
		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$zip          = get_post_meta( $queried_object->ID, 'wpsl_zip', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$site_url       = get_post_meta( $queried_object->ID, 'wpsl_site_url', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ',' . $city . ',' . $state.',' .$zip;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

		$no_build_site = get_post_meta( $queried_object->ID, 'no_build_site', true );

	?>

<div class="storelocation">
	<div class="header-banner-image-bg">
		<div class="header-banner-image">
		</div>
	</div>
	
	<div class="header-sale-image">		
		
			<?php echo do_shortcode('[coupon "salebanner"]'); ?>	
		
	</div>
	<div class="container">
		<div class="row">	

				<div class="col-lg-5 col-md-6 col-sm-12 storeloc-content">
					<img src="/wp-content/uploads/2025/01/Standard-Tile-logo.png"/>
					
					<h1 class="entry-title sfnstoretitle"><?php echo get_the_title();  ?></h1>
					

				

					<?php if($phone!=''){?>
			        <div class="store_phone">
						<span class="phNo">PHONE NUMBER </span>
						<span class="phNoDis"><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></span>
					</div>
					<?php } ?>
			
					<div class="storehours">
						<span>HOURS </span>
						<?php echo do_shortcode( '[wpsl_hours]' ); ?>
					</div>
			
				
					
				</div>

				
				<div class="col-lg-7 col-md-6 col-sm-12 storeloc-map">
					<div class="mapcontainer">
						<div class="mapcon_main">
							<div class="mapcon_left">
								<div class="mapcon_left_flex">
								<i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i>
								<div>
									<h2 class="entry-title sfnstoretitle"><?php echo $store_shortname;  ?></h2>
									<span class="add"><?php echo $destination;?></span>
								</div>
								</div>							
							</div>
							<div class="mapcon_right">
								<a href="<?php echo $direction_url ;?>" target="_blank">GET DIRECTIONS</a>
							</div>
						</div>
						
						<div class="store_map_wrap">
														
							<?php echo do_shortcode( '[wpsl_map]' ); ?>

						</div>
										
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="bottomsection">
						<h2>About <?php echo get_the_title();?></h2>
						<p>Looking for a local flooring and luxury vinyl store in <?php echo $city; ?>? <?php echo get_the_title();?> has a wide selection of the highest quality tile and luxury vinyl flooring at the best prices. Conveniently located in  <?php echo $city; ?>, <?php echo $state; ?>, <?php echo get_the_title();?> has an experienced and knowledgeable staff who will guide you through each step of selecting the right flooring for your home or business. Whether you need new tile or luxury vinyl, our friendly flooring experts will help you find exactly what you need to fit your taste, lifestyle, and budget. Come visit our  <?php echo $city; ?> showroom!
</p>
					</div>
					
				</div>
			</div>
	</div>
</div>
<script>
jQuery(window).on("load", function() {
    // page is fully loaded, including all frames, objects and images
    jQuery(".wpsl-gmap-canvas div[role=button] > img").trigger( "click" );
});
</script>
<?php get_footer(); ?>
