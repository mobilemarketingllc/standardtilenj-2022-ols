// header flyer choose location js start here 
jQuery(document).on('click', '.makemystore', function() {
  
    var mystore = jQuery(this).attr("data-store-id");  

    //alert(mystore);

    jQuery.cookie("preferred_store", null, { path: '/' });   
    jQuery.cookie("preferred_storename", null, { path: '/' });
    jQuery.cookie("preferred_address", null, { path: '/' });
    jQuery.cookie("preferred_storephone", null, { path: '/' });
    jQuery.cookie("preferred_storetime", null, { path: '/' });
    jQuery.cookie("preferred_storehour", null, { path: '/' });

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=make_my_store&store_id=' + jQuery(this).attr("data-store-id"),
        dataType: 'JSON',

        success: function(data) {          

            var posts = JSON.parse(JSON.stringify(data));
            var store_name = posts.store_title;
            var store_add = posts.store_address;        
            var store_phone = posts.header_phone;      
           

            jQuery(".header-location p.location-name").html(store_name);
            jQuery(".find_wrap p.location-name").html(store_name);
            jQuery(".header-location span.location-add").html(store_add);
            jQuery(".find_wrap span.location-add").html(store_add);

            //jQuery( "#wpsl-stores ul" ).prepend( jQuery('#'+mystore) );

            jQuery(".bb_location_name").html(store_name);

            jQuery(".fc_location_name h3.uabb-infobox-title").html(store_name);
            if(posts.store_phone !=''){ 
                jQuery(".fc_location_phone a.fc_phone").html(store_phone);
                jQuery('a.fc_phone').attr("href", "tel:" + store_phone);
            }


            if(posts.store_status !=''){jQuery(".fc_open_close").html(posts.store_status);}
             jQuery(".fc_timing").html( posts.store_hour);
            
            jQuery(".bb_loc_address").html(store_add);

            jQuery('#ajaxstorelisting').find(".activebb").removeClass("activebb");
            jQuery('#ajaxstorelisting').find("#"+mystore+"").addClass("activebb");

            jQuery( "#ajaxstorelisting" ).prepend( jQuery("#"+mystore+"") );

            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });           
            jQuery.cookie("preferred_storename", store_name, { expires: 1, path: '/' });
            jQuery.cookie("preferred_address", store_add, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storephone", store_phone, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storetime", posts.store_status, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storehour", posts.store_hour, { expires: 1, path: '/' });

            jQuery('.populate-store select').val(store_name);
            jQuery(".fl-html #wpsl-wrap div[title='"+store_name+"']").trigger( "click" );
            	
        }
    });

});

jQuery(document).on('click', '.makemystore_radio', function() {

    var mystore = jQuery("input[type=radio][name=mystore]:checked").val();   

    jQuery.ajax({
        type: "POST",
        url: "/wp-admin/admin-ajax.php",
        data: 'action=make_my_store&store_id=' + mystore,
        dataType: 'JSON',

        success: function(data) {          

            var posts = JSON.parse(JSON.stringify(data));
            var store_name = posts.store_title;
            var store_add = posts.store_address;        
            var store_phone = posts.header_phone;   
            var store_city = posts.city;         
           
            jQuery(".header-location p.location-name").html(store_name);
            jQuery(".find_wrap p.location-name").html(store_name);
            jQuery(".header-location span.location-add").html(store_add);
            jQuery(".find_wrap span.location-add").html(store_add);

            //jQuery( "#wpsl-stores ul" ).prepend( jQuery('#'+mystore) );

            jQuery(".bb_location_name").html(store_name);

            jQuery(".fc_location_name h3.uabb-infobox-title").html(store_name);
            jQuery(".fc_location_phone a.fc_phone").html(store_phone);
            jQuery('a.fc_phone').attr("href", "tel:" + store_phone);

            jQuery(".fc_open_close").html(posts.store_status);
            jQuery(".fc_timing").html( posts.store_hour);
            
            jQuery(".bb_loc_address").html(store_add);      
            
            jQuery(".fl-html #wpsl-wrap div[title='"+store_name+"']").trigger( "click" );

            jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });           
            jQuery.cookie("preferred_storename", store_name, { expires: 1, path: '/' });
            jQuery.cookie("preferred_address", store_add, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storephone", store_phone, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storetime", posts.store_status, { expires: 1, path: '/' });
            jQuery.cookie("preferred_storehour", posts.store_hour, { expires: 1, path: '/' });
            jQuery.cookie("preferred_city", posts.city, { expires: 1, path: '/' });

            jQuery('#ajaxstorelisting').find(".activebb").removeClass("activebb");
            jQuery('#ajaxstorelisting').find("#"+mystore+"").addClass("activebb");

            jQuery( "#ajaxstorelisting" ).prepend( jQuery("#"+mystore+"") );
		
         
            setTimeout(addFlyerEvent, 1000);	

            var mystore_loc = store_name;
            jQuery(".populate-store select").val(mystore_loc);
    if(mystore_loc == 'Jersey City'){

        jQuery(".populate-staff select").val('8');

    }else if(mystore_loc == 'Watchung'){

        jQuery(".populate-staff select").val('7');

    }else if(mystore_loc == 'Edison'){

        jQuery(".populate-staff select").val('6');

    }else if(mystore_loc == 'Succasunna'){

        jQuery(".populate-staff select").val('5');

    }else if(mystore_loc == 'East Hanover'){

        jQuery(".populate-staff select").val('4');

    }else if(mystore_loc == 'Totowa'){

        jQuery(".populate-staff select").val('1');

    }       

    jQuery('.populate-staff select').trigger('change');


            
        }
    });

});


// flyer close function 
function closeNav() {
    let storeLocation = document.getElementById("storeLocation");
    storeLocation.style.right = "-100%";
    storeLocation.classList.remove("ForOverlay");
}

// flyer event assign function
function addFlyerEvent() {
   jQuery('.locationWrapFlyer .dropIcon').click(function() {
        jQuery('#storeLocation').toggle();
    })
}

jQuery(document).ready(function() {

    jQuery('form').find("input[name=input_20]").each(function(ev){

         jQuery("#input_12_20").attr("placeholder", "Select Location");

    });
             
});


//jQuery( "#wpsl-wrap" ).on( "load",function() {
jQuery(document).ajaxSuccess(function() {

    jQuery(".makemystore").show();
    jQuery(".your_store").html("");
    jQuery('#wpsl-stores ul li').removeClass('mystore_location');

    

    jQuery('#wpsl-stores ul li').each(function() {       

        if(jQuery.cookie("preferred_store") == jQuery(this).attr('data-store-id')){

            var data = jQuery(this).attr('data-store-id');   

            jQuery(this).addClass('mystore_location');
            jQuery( "#wpsl-stores ul" ).prepend( jQuery(this) );

            jQuery(this).find(".makemystore").hide();
            jQuery(this).find(".your_store").html("YOUR STORE IS");
            jQuery(this).find(".visit_store").addClass('redbutton');

            jQuery( "#ajaxstorelisting" ).prepend( jQuery("#"+data+"") );

            
            

        }

    });
});


jQuery(document).ready(function() {

    jQuery("#wpsl-search-input").attr("placeholder", "Enter Zip / Postal Code");
  
    var mystore_loc = jQuery(".header_location_name").html();
    jQuery("#input_29_32").val(mystore_loc);

    jQuery(".fl-html #wpsl-wrap div[title='"+mystore_loc+"']").trigger( "click" );
   
    var preferred_address = jQuery.cookie("preferred_address");
    var preferred_store = jQuery.cookie("preferred_store");
    var preferred_storename = jQuery.cookie("preferred_storename");
    var preferred_storephone = jQuery.cookie("preferred_storephone");
    var preferred_storetime = jQuery.cookie("preferred_storetime");
    var preferred_storehour = jQuery.cookie("preferred_storehour");
    var preferred_city = jQuery.cookie("preferred_city");

    jQuery(".fc_location_name h3.uabb-infobox-title").html(preferred_storename);

    
    jQuery(".fc_location_phone a.fc_phone").html(preferred_storephone);
    jQuery('a.fc_phone').attr("href", "tel:" + preferred_storephone);
    jQuery(".fc_open_close").html(preferred_storetime);
    jQuery(".fc_timing").html(preferred_storehour);
  
    jQuery( "#ajaxstorelisting" ).prepend( jQuery("#"+preferred_store+"") );
    jQuery('#ajaxstorelisting .activebb input').attr('checked',true);
	
	
    
    if(mystore_loc == 'Jersey City'){

        jQuery(".populate-staff select").val('8');

    }else if(mystore_loc == 'Watchung'){

        jQuery(".populate-staff select").val('7');

    }else if(mystore_loc == 'Edison'){

        jQuery(".populate-staff select").val('6');

    }else if(mystore_loc == 'Succasunna'){

        jQuery(".populate-staff select").val('5');

    }else if(mystore_loc == 'East Hanover'){

        jQuery(".populate-staff select").val('4');

    }else if(mystore_loc == 'Totowa'){

        jQuery(".populate-staff select").val('1');

    }       
    jQuery('.populate-staff select').trigger('change');
    
    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var list_data = posts.list;
               var header_phone = posts.header_phone;
               var locname = posts.store_name;          
               var locid = posts.store_name;
               var loc_address = posts.address; 
               var store_city = posts.city; 
  
               
              // jQuery('.fl-html #wpsl-wrap div[title=${locname}]').trigger( "click" );
              jQuery(".fl-html #wpsl-wrap div[title='"+locname+"']").trigger( "click" );
               if(jQuery.cookie("preferred_storename") == null){
  
                jQuery.cookie("preferred_store", posts.store_id, { expires: 1, path: '/' });               
                jQuery.cookie("preferred_storename", locname, { expires: 1, path: '/' });
                jQuery.cookie("preferred_address", loc_address, { expires: 1, path: '/' });
                jQuery.cookie("preferred_storephone", header_phone, { expires: 1, path: '/' });     
                jQuery.cookie("preferred_storetime", posts.store_status, { expires: 1, path: '/' });
                jQuery.cookie("preferred_storehour", posts.store_hour, { expires: 1, path: '/' });
                jQuery.cookie("preferred_city", posts.city, { expires: 1, path: '/' });
    
                jQuery(".fc_location_name h3.uabb-infobox-title").html(posts.store_name);
                jQuery(".fc_location_phone a.fc_phone").html(header_phone);
                jQuery('a.fc_phone').attr("href", "tel:" + header_phone);
                jQuery(".fc_open_close").html(posts.store_status);
                jQuery(".fc_timing").html(posts.store_hour);
				//jQuery('.fl-html #wpsl-wrap div[title=${posts.store_name}]').trigger( "click" );   
                
                jQuery(".fl-html #wpsl-wrap div[title='"+posts.store_name+"']").trigger( "click" );
                
               }            

               var mystore_loc =  posts.store_name 
               jQuery(".populate-store select").val(mystore_loc);
              
               jQuery(".location_div").html(header_data);

               jQuery(".find_wrap").html(header_data);
                   
               jQuery("#ajaxstorelisting").html(list_data);           
               
            
            
             
       }
   });
  
  });

  jQuery(document).ready(function() {
    jQuery('.header-location').click(function() {
        jQuery('#ajaxstorelisting').toggle();
        jQuery('#ajaxstorelisting .activebb input').attr('checked',true);
    })
    var customSearch = jQuery('.header-location');
    
    jQuery(document).mouseup(function (e) {
        if (!customSearch.is(e.target) && customSearch.has(e.target).length === 0 && jQuery('#ajaxstorelisting').css('display') == 'block') {
            jQuery('#ajaxstorelisting').toggle();
        }
    });
});

 jQuery(document).ready(function() {
     jQuery('.searchIcon .fl-icon').click(function() {
         jQuery('.searchModule').slideToggle();
     });

     jQuery(document).mouseup(function(e) {
         var container = jQuery(".searchModule, .searchIcon");
         // if the target of the click isn't the container nor a descendant of the container
         if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.searchModule').css('display') !== 'none') {
             jQuery('.searchModule').slideToggle();
         }
     });
 });

 jQuery(document).ready(function() {
	
	jQuery("span.show_timezone").hide();
    var mystore_loc =  jQuery.cookie("preferred_city");
    //console.log(mystore_loc);
    
    //jQuery(".populate-store select").val(mystore_loc);

    if(mystore_loc == 'Jersey City'){

        jQuery(".populate-staff select").val('8');

    }else if(mystore_loc == 'Watchung'){

        jQuery(".populate-staff select").val('7');

    }else if(mystore_loc == 'Edison'){

        jQuery(".populate-staff select").val('6');

    }else if(mystore_loc == 'Succasunna'){

        jQuery(".populate-staff select").val('5');

    }else if(mystore_loc == 'East Hanover'){

        jQuery(".populate-staff select").val('4');

    }else if(mystore_loc == 'Totowa'){

        jQuery(".populate-staff select").val('1');

    }       
    jQuery('.populate-staff select').trigger('change');

});

jQuery(document).ajaxSuccess(function() {
    jQuery("span.show_timezone").hide();
    });