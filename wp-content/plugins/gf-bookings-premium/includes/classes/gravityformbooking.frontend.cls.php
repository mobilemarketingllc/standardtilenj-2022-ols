<?php

/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



class GravityFormBookingFrontEnd {
	
	function __construct(){
		
		/* Hooks & Filters */
		add_action( 'plugins_loaded', array( &$this, 'gfbFrontEndFiles' ) );
		
		add_action( 'wp_enqueue_scripts', array( &$this, 'gfbSetFrontEndScripts' ), 99 );
		add_action( 'wp_enqueue_scripts', array( &$this, 'gfbSetFrontEndStyles' ) );
	
		add_action( 'wp_ajax_gfb_appoinment_popup', array(&$this, 'gfbAppointmentFormPopUp') );
		add_action( 'wp_ajax_nopriv_gfb_appoinment_popup', array(&$this, 'gfbAppointmentFormPopUp') );
	}
	
	/* Get All Files */
	function gfbFrontEndFiles() {
		$files = array(
			GFB_INCLUDES_SHORTCODE . '/customer-appointments.php',
		);	
		
		foreach($files as $file){
			require_once( $file );	
		}	
	}
	
	/* Add Plugin Scripts */
	function gfbSetFrontEndScripts(){
		
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script('jquery-ui-Core');		

		wp_enqueue_script( 'gfb-magnific-popup-js', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array('jquery'), GFB_VERSION, true );
		
		if(wp_script_is( 'jquery-ui-datepicker', 'enqueued' ) == false){
		
			wp_enqueue_script( 'gfb-ui-datepicker-js', GFB_PATH_URL . '/assets/js/jquery-ui.min.js', array('jquery'), GFB_VERSION, true );
		
		}
		
		wp_enqueue_script( 'gfb-sweetalert-js', 'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.20.4/sweetalert2.js', array('jquery'), GFB_VERSION, true );
		
		wp_enqueue_script( 'gfb-validate-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', array('jquery'), GFB_VERSION, true );
		
		// Fiels For Bootstrap 4 DataTable
        wp_register_script('gfb_data-table-jquery', GFB_URL . '/gfb-booking-fields/assets/data-table/jquery.js',array('jquery'), GFB_VERSION, false);
        wp_register_script('gfb_data-table-dataTables', GFB_URL . '/gfb-booking-fields/assets/data-table/dataTables.js',array('jquery'), GFB_VERSION, false);
        wp_register_script('gfb_data-table-bootstrap4', GFB_URL . '/gfb-booking-fields/assets/data-table/bootstrap4.js',array('jquery'), GFB_VERSION, false);
        wp_register_script('gfb_data-table-responsive', GFB_URL . '/gfb-booking-fields/assets/data-table/responsive.js',array('jquery'), GFB_VERSION, false);
        
		
		wp_localize_script('', '', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'mindate' => $this->gfbMinDate(),
			'maxdate' => $this->gfbMaxDate(),
			'holidayarr' => $this->gfbHolidayJson(),
			'loader' => GFB_INLINE_LOADER
		) );
		
		wp_enqueue_script( 'gfb-inline-script-js', GFB_ASSET . 'js/inline-script.js', array('jquery'), GFB_VERSION, true );	
	}
	
	/* Add Plugin Styles */
	function gfbSetFrontEndStyles(){
		wp_enqueue_style('gfb-admin-style-css', GFB_ADMIN_ASSET . 'css/admin.style.css', array(), GFB_VERSION );		
        wp_enqueue_style('gfb_data-table-bootstrap4-css', GFB_URL . '/gfb-booking-fields/assets/data-table/bootstrap4.css');
        wp_enqueue_style('gfb_data-table-responsive-css', GFB_URL . '/gfb-booking-fields/assets/data-table/responsive.css');
    }
	
	/* Find mindate form prior days */
	public static function gfbMinDate() {
		
		global $gfbHolidays;
		$holidays = array();
		$priorDays = get_option('gfb_prior_days_book_appointment');
		$mindatetm = date('d F, Y', strtotime("+".($priorDays+1)." days"));				
		
		/* Get Holiday List */
		$holidays = json_decode($gfbHolidays->gfbListHolidayFrontEnd());
		
		if( in_array($mindatetm, $holidays) === true ) {
			$mindate = 	date('d F, Y', strtotime("+".($priorDays+2)." days"));	
		}
		else {
			$mindate = 	$mindatetm;
		}
							
		return $mindate;			
	}
	
	/* Find maxdate form prior months */
	public static function gfbMaxDate() {
		
		$priority_months = get_option('gfb_prior_months_book_appointment');
		$maxDateMonth = date('Y-m-d', strtotime("+".$priority_months." months"));
		$maxDate = date('d F, Y', strtotime($maxDateMonth."-1 day"));		
	
		return $maxDate;			
	}
	
	/* Get holiday list in json */
	public static function gfbHolidayJson() {
		
		global $gfbHolidays;
		$holidays = $gfbHolidays->gfbListHolidayFrontEnd();
		return $holidays;	
	}

}

global $gfbFrontEndObj;
$gfbFrontEndObj = new GravityFormBookingFrontEnd;
