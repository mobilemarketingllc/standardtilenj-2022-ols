<?php
defined( 'ABSPATH' ) or exit; // Exit if accessed directly

if ( class_exists( 'GFForms' ) ) {
	class GF_gfb_appointment_services_Category extends GF_Field {
		
		public $type = 'gfb_appointment_services_category';
		
		public function get_form_editor_field_title() {
			return esc_attr__( 'Services Category', 'gravityforms' );
		}

		/*
		* Where to assign this widget
		*/
		public function get_form_editor_button() {
			return array(
				//'group' => 'advanced_fields',
				'group' => 'gfb_appointment_calendar',
				'text'  => $this->get_form_editor_field_title()
			);
		}
		/*
		* Add button to the group
		*/
		public function add_button( $field_groups ) {
			$field_groups = $this->gfb_gfb_appointment_services_gf_group( $field_groups );
			return parent::add_button( $field_groups );
		}
		/*
		* Add our group
		*/
		public function gfb_gfb_appointment_services_gf_group( $field_groups ) {
			foreach ( $field_groups as $field_group ) {
				if ( $field_group['name'] == 'gfb_appointment_calendar' ) {
					return $field_groups;
				}
			}
			$field_groups[] = array(
				'name'   => 'gfb_appointment_calendar',
				'label'  => __( 'Booking Fields', 'simplefieldaddon' ),
				'fields' => array(
				)
			);

			return $field_groups;
		}

		/*
		* Widget settings
		*/
		function get_form_editor_field_settings() {
			return array(
				'enable_enhanced_ui_setting',
				'label_setting',
				'error_message_setting',
				'label_placement_setting',
				'admin_label_setting',
				'size_setting',
				'description_setting',
				'css_class_setting',
				'rules_setting',
				'conditional_logic_field_setting', 
				'visibility_setting',
			);
		}

		public function is_conditional_logic_supported() {
			return false;
		}

		/**
		 * Field Markup
		 */
		public function get_field_input( $form, $value = '', $entry = null ) {
			$form_id         = absint( $form['id'] );
			$is_entry_detail = $this->is_entry_detail();
			$is_form_editor  = $this->is_form_editor();
			$id       = $this->id;
			$field_id = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
			//$logic_event        = $this->get_conditional_logic_event( 'change' );
			$size               = $this->size;
			$class_suffix       = $is_entry_detail ? '_admin' : '';
			$chosenUI           = $this->enableEnhancedUI ? ' chosen-select' : '';
			$field_class        = ' service_category_id';
			$class              = $size . $class_suffix . $chosenUI . $field_class;
            $css_class          = trim( esc_attr( $class ) . ' gfield_select' );
			$tabindex           = $this->get_tabindex();
			$disabled_text      = $is_form_editor ? 'disabled="disabled"' : '';
			$required_attribute = $this->isRequired ? 'aria-required="true"' : '';
			$invalid_attribute  = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
			$choices = '';

			if( $this->is_entry_edit() ) {
			//$choices .= sprintf( "<div class='ginput_container ginput_container_select'><select name='input_%s' id='%s' $logic_event class='%s' $tabindex %s %s %s>%s</select></div>", $id, $field_id, $css_class, $disabled_text, $required_attribute, $invalid_attribute, $this->get_services_category_entry_choices($value, $form) );
                $choices .= sprintf( "<div class='ginput_container ginput_container_select'><select name='input_%s' id='%s'  class='%s' $tabindex %s %s %s>%s</select></div>", $id, $field_id, $css_class, $disabled_text, $required_attribute, $invalid_attribute, $this->get_services_category_entry_choices($value, $form) );
			} elseif( !$this->is_form_editor() ) {
			//$choices .= sprintf( "<div class='ginput_container ginput_container_select'><select name='input_%s' id='%s' $logic_event class='%s' $tabindex %s %s %s form_id='%d'>%s</select></div>", $id, $field_id, $css_class, $disabled_text, $required_attribute, $invalid_attribute, $form_id, $this->get_services_category_choices($value, $form) );
                $choices .= sprintf( "<div class='ginput_container ginput_container_select'><select name='input_%s' id='%s'  class='%s' $tabindex %s %s %s form_id='%d'>%s</select></div>", $id, $field_id, $css_class, $disabled_text, $required_attribute, $invalid_attribute, $form_id, $this->get_services_category_choices( $value, $form ) );
			}

			return $choices;
		}

		/**
		 * Is Entry Edit
		 */
		public function is_entry_edit() {
			if ( rgget( 'page' ) == 'gf_entries' && rgget( 'view' ) == 'entry' && rgpost( 'screen_mode' ) == 'edit' ) {
				return true;
			}
			return false;
		}


		/**
		 * Returns TRUE if the current page is the form editor page. Otherwise, returns FALSE
		 */
		// public function is_form_editor() {
		// 	if ( rgget( 'page' ) == 'gf_edit_forms' && ! rgempty( 'id', $_GET ) && rgempty( 'view', $_GET ) ) {
		// 		return true;
		// 	}
		// 	return false;
		// }

		/**
		 * Get Services Categories Select Options
		 */
		public function get_services_category_choices($value, $form) {
			$options = '';
			$options .= '<option value="">Select Category</option>';
			// The Query Select Services Categories
			$categorylist = new GravityFormBookingCategory();
			$categories = $categorylist->gfbListCategories();
			// The Loop
			 if ($categories) {
				foreach ($categories as $resultData):
					$selected = $value == $resultData['category_id'] ? ' selected="selected"' : '';
					$options .= '<option value="'.$resultData['category_id'].'"'.$selected.'>'.$resultData['category_name'].'</option>' . PHP_EOL;
				endforeach;
			} else {
				// no services found
			} 

			return $options;
		}

		/**
		 * Get Admin Services Category Entry Options
		 */
		public function get_services_category_entry_choices($value, $form) {
			$options = '';
			// The Query Select Categories
			$categorylist = new GravityFormBookingCategory();
			$categories = $categorylist->gfbListCategories();
			// The Loop
			 if ($categories) {
				foreach ($categories as $resultData):
					$selected = $value == $resultData['category_id'] ? ' selected="selected"' : '';
					$options .= '<option value="'.$resultData['category_id'].'"'.$selected.'>'.$resultData['category_name'].'</option>' . PHP_EOL;
				endforeach;
			} else {
				// no services found
			} 
			return $options;
		}


		/**
		 * Validation Failed
		 */
		private function validationFailed( $message = '' ) {
			$this->failed_validation = true;
			$message = esc_html__( $message, 'gravityforms' );
			$this->validation_message = empty( $this->errorMessage ) ? $message : $this->errorMessage;
		}

		/**
		 * Validation
		 */
		public function validate( $value, $form ) {
			 $form_id = absint( $form['id'] );
			if($value !="") {
				# valid field
			} else {
				$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_required_service_category') );
				return;
			}  
			// Check if calendar widget is found
			 if( !gfb_field_type_exists( $form, 'gfb_appointment_calendar' ) ) {
				$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_required_date') );
				return;
			} 

		}

		/**
		 * Save value
		 */
		public function get_value_save_entry( $value, $form, $input_name, $entry_id, $entry ) {
			return $value;
		}

		/**
		* Show service title entry single
		*/
		public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {

			$post_id = absint( $value );
			if( $post_id != "") {
				$categoryObj = new GravityFormBookingCategory();
				$categoryDetails = $categoryObj->gfbCategoryDetails($post_id);
				$value = $categoryDetails[0]['category_name']; 
				return esc_html( $value );
			} else {

				return esc_html( $value );

			}
		}

		/**
		* Merge tag, on notifications, confirmations
		*/
		public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {
			$post_id = absint( $value );
			if($post_id !=""){
				$categoryObj = new GravityFormBookingCategory();
				$categoryDetails = $categoryObj->gfbCategoryDetails($post_id);
				$value = $categoryDetails[0]['category_name'];
				return esc_html( $value );
			} else {

				return esc_html( $value );

			}
		}


		/*
		* Show service title on entry list
		*/
		
		public function get_value_entry_list( $value, $entry, $field_id, $columns, $form ) {
			$post_id = absint( $value );
			if($post_id !=""){
				$categoryObj = new GravityFormBookingCategory();
				$categoryDetails = $categoryObj->gfbCategoryDetails($post_id);
				$value = $categoryDetails[0]['category_name'];
				return esc_html( $value );
			} else {

				return esc_html( $value );

			}

		}
		


		public function get_form_editor_inline_script_on_page_render() {
			return "
			gform.addFilter('gform_form_editor_can_field_be_added', function (canFieldBeAdded, type) {
				if (type == 'gfb_appointment_services_category') {
					if (GetFieldsByType(['gfb_appointment_services_category']).length > 0) {
						alert(" . json_encode( esc_html__( 'Only one Services Category field can be added to the form', 'gravityformscoupons' ) ) . ");
						return false;
					}
				}
				return canFieldBeAdded;
			});";
		}



	} // end class
	GF_Fields::register( new GF_gfb_appointment_services_Category() );
} // end if
