<?php

defined( 'ABSPATH' ) or exit; // Exit if accessed directly

if ( class_exists( 'GFForms' ) ) {
	class GFB_Appointment_Booking_Calendar extends GF_Field {
		public $type = 'gfb_appointment_calendar';

		public function get_form_editor_field_title() {
			return esc_attr__( 'Booking Calendar', 'gravityforms' );
		}

		/*
		* Where to assign this widget
		*/
		public function get_form_editor_button() {
			return array(
				//'group' => 'advanced_fields',
				'group' => 'gfb_appointment_calendar',
				'text'  => $this->get_form_editor_field_title()
			);
		}
		/*
		* Add button to the group
		*/
		public function add_button( $field_groups ) {
			$field_groups = $this->gfb_gfb_appointment_services_gf_group( $field_groups );
			return parent::add_button( $field_groups );
		}
		/*
		* Add our group
		*/
		public function gfb_gfb_appointment_services_gf_group( $field_groups ) {
			foreach ( $field_groups as $field_group ) {
				if ( $field_group['name'] == 'gfb_appointment_calendar' ) {
					return $field_groups;
				}
			}
			$field_groups[] = array(
				'name'   => 'gfb_appointment_calendar',
				'label'  => __( 'Booking Fields', 'gfb' ),
				'fields' => array(
				)
			);

			return $field_groups;
		}

		/*
		* Widget settings
		*/
		function get_form_editor_field_settings() {
			return array(
				'label_setting',
				'error_message_setting',
				'label_placement_setting',
				'admin_label_setting',
				'description_setting',
				'css_class_setting',
				'rules_setting',
				'size_setting',
				'conditional_logic_field_setting',
			);
		}

		public function is_conditional_logic_supported() {
			return true;
		}

		
		public function is_value_submission_empty($form_id) {
			return false;
		}

		/**
		 * Field Markup
		 */
		public function get_field_input( $form, $value = '', $entry = null, $service_id = null, $selected_date = null) {
			$form_id         = absint( $form['id'] );
			$is_entry_detail = $this->is_entry_detail();
			$is_form_editor  = $this->is_form_editor();
			$id                 = $this->id;
			$field_id           = $is_entry_detail || $is_form_editor || $form_id == 0 ? "input_$id" : 'input_' . $form_id . "_$id";
			$size               = $this->size;
			$class_suffix       = $is_entry_detail ? '_admin' : '';
			$class              = $size . $class_suffix;
			$css_class          = trim( esc_attr( $class ) . ' gfield_select' );
			$tabindex           = $this->get_tabindex();
			$disabled_text      = $is_form_editor ? 'disabled="disabled"' : '';
			$required_attribute = $this->isRequired ? 'aria-required="true"' : '';
			$invalid_attribute  = $this->failed_validation ? 'aria-invalid="true"' : 'aria-invalid="false"';
			
			$calendar  = "<div class='ginput_container'>";
			$calendar .= $this->gfb_calendar_theme_color_style($form_id);	
			
			if( $this->is_entry_edit() ) {

				$calendar .= 'This field is not editable';
				$calendar .= "<input type='hidden' name='input_{$id}' id='{$field_id}' value='{$value}'/>";

			} elseif( !$this->is_form_editor() ) {
				ob_start();

				$calendar .= '<div class="grid-row"><div class="'.$this->field_size().' grid-sm-12 grid-xs-12" id="gfbgbfappointments_calendar">' . PHP_EOL;
				$service_id   = gfb_get_field_type_value( $form, 'gfb_appointment_services' );
				if(gfb_field_type_exists( $form, 'gfb_appointment_services' ) ) {
					$continue = true;
				}
				elseif($service_id != null){
					$continue = true;
				}
				else {
					$continue = false;
				}

				if($continue = true){
					$current_date   = gfb_current_date_with_timezone();
					$provider_id = 0;
					$providers_field_value  = gfb_get_field_type_value( $form, 'gfb_appointment_providers' );
					if( is_numeric($providers_field_value)) {
						$provider_id = $providers_field_value;
					}
					// Booking Date/Time Fields
					$date_val = '';
					$time_val = '';
					$cost_val = '0';
					$slot_qty = 1;
					$slot_meeping_id = '';

					$value = gfb_get_field_type_value( $form, 'gfb_appointment_calendar');
					if ( is_array( $value ) ) {
						$date_val  = isset($value['date']) ? $value['date'] : $date_val;
						$time_val  = isset($value['time']) ? $value['time'] : $time_val;
						$cost_val  = isset($value['cost']) ? $value['cost'] : $cost_val;
						$slot_qty  = isset($value['slot_qty']) ? $value['slot_qty'] : $slot_qty;
						$slot_meeping_id  = isset($value['slot_meeping_id']) ? $value['slot_meeping_id'] : $slot_meeping_id;
					}

                    // Form submited date & time
					$selected_date      = false;
					$selected_slot      = $time_val;
					if( gfb_valid_date_format($date_val) ) {
						$current_date   = new DateTime( $date_val, new DateTimeZone(gfb_time_zone()) );
						$selected_date  = clone $current_date;
					}
                    // Form submited date & time

                    // Calendar HTML
                    //removed class disabled from gfb_monthly_schedule_wrapper - developer53.
					$calendar   .= '<div id="gfb_appointments_calendar" form_id="'.$form_id.'"><div class="gfb_monthly_schedule_wrapper">' . PHP_EOL;
					$gfb_calendar = new gfb_calendar( $form_id, $current_date->format('m'), $current_date->format('Y'), $service_id, $provider_id, $selected_date, $selected_slot );
					$calendar   .= $gfb_calendar->show();
                    $calendar   .= '</div></div>' . PHP_EOL; // end #gfb_appointments_calendar
                    // End Calendar HTML
                }
                else{
                	return '<p>' .gfb_get_form_translated_data($form_id, 'error_no_services'). '</p>';
                }
				$calendar .= '</div></div>' . PHP_EOL; // end grid-row
				$calendar .= "<input type='hidden' name='input_{$id}[date]' id='{$field_id}' class='{$class} ginput_{$this->type}_input appointment_booking_date' value='".$date_val."'/>";
				$calendar .= "<input type='hidden' name='input_{$id}[time]' id='{$field_id}_time' class='{$class} ginput_{$this->type}_input appointment_booking_time' value='{$time_val}'/>";
				$calendar .= "<input type='hidden' name='input_{$id}[slot_meeping_id]' id='{$field_id}_slot_meeping' class='{$class} ginput_{$this->type}_input appointment_booking_slot_mapping_id' value='{$slot_meeping_id}'/>";
				$calendar .= "<input type='hidden' name='input_{$id}[slot_qty]' id='{$field_id}_slot_qty' class='{$class} ginput_{$this->type}_input appointment_booking_slot_qty' value='{$slot_qty}'/>";

				// Appointment cost hidden field just in case
				$calendar .= "<input type='hidden' name='input_{$id}[cost]' class='ginput_gfb_appointment_cost_input gform_hidden' value='{$cost_val}'/>";

				$calendar .= ob_get_clean();
			}

			$calendar .= '</div>' . PHP_EOL; // end ginput_container
			return $calendar;
		}

		public function gfb_calendar_theme_color_style($form_id){
			$calendar_theme_color_1  = get_option( 'gfb_cal_gradient_color_top', "#FFFFFF");
			$calendar_theme_color_2  = get_option( 'gfb_cal_gradient_color_bottom', "#FAFAFA");
			$calendar_font_color  = get_option( 'gfb_cal_text_color', "#333333");
			
			$calendar_font_bg_color  = get_option( 'gfb_cal_text_bg_color', "#00D0AC");
			$calendar_font_color_selected  = get_option( 'gfb_cal_text_selected_color', "#FFFFFF");

			$calendar_available_day_bg_color  = get_option( 'gfb_cal_available_day_bg_color', "#00D0AC");
			$calendar_available_day_font_color  = get_option( 'gfb_cal_available_day_text_color', "#FFFFFF");

			return $style_script = '<style>
			:root{ 
				--theme_calendar_active_color:'.$calendar_theme_color_1.';
				--theme_calendar_active_color_secondary:'.$calendar_theme_color_2.';
				--theme_calendar_font_color: '.$calendar_font_color.'; /*calendar font color */
				--theme_calendar_font_bg_color: '.$calendar_font_bg_color.'; /*calendar font bg color */
				--theme_calendar_font_selected_color: '.$calendar_font_color_selected.'; /*calendar font selected color */
				--theme_calendar_available_day_bg_color: '.$calendar_available_day_bg_color.'; /*calendar available day bg color */
				--theme_calendar_available_day_font_color: '.$calendar_available_day_font_color.'; /*calendar available day font color */
				--theme_calendar_header_background_color: #f1f3f6; /* Header or slot Background Color */
				--theme_calendar_active_hover_color: #fff; /* Hover or Active white color */
				--theme_calendar_holiday_color: #f1f3f6; /* Holiday color */
			}			
			</style>';
		}
		
		/**
		 * Is Entry Edit
		 */
		public function is_entry_edit() {
			if ( rgget( 'page' ) == 'gf_entries' && rgget( 'view' ) == 'entry' && rgpost( 'screen_mode' ) == 'edit' ) {
				return true;
			}

			return false;
		}

		public function get_inline_price_styles() {
			return '';
		}


		/**
		 * Field Size Class
		 */
		public function field_size() {

			if( isset( $this->size ) ) {
				switch ($this->size) {
					case "small":
					$gf_size      = 'gfb_wrapper_small grid-lg-4 grid-md-4 grid-sm-6 grid-sx-12';
					break;
					case "medium":
					$gf_size      = 'gfb_wrapper_medium grid-lg-6 grid-md-6';
					break;
					case "large":
					$gf_size      = 'gfb_wrapper_large grid-lg-12 grid-md-12';
					break;
					default:
					$gf_size      = 'gfb_wrapper_medium here grid-lg-6 grid-md-6';
				}
			} else {
				$gf_size              = 'gfb_wrapper_medium grid-lg-6 grid-md-6';
			}

			return $gf_size;
		}

		/**
		 * Validation Failed Message
		 */
		private function validationFailed( $message = '' ) {
			$this->failed_validation = true;
			$message = esc_html__( $message, 'gravityforms' );
			$this->validation_message = empty( $this->errorMessage ) ? $message : $this->errorMessage;
		}

		/**
		 * Validate
		 */
		public function validate( $value, $form ) {
			$form_id = absint( $form['id'] );
			$dateValue = '';
			$timeValue = '';
			$slotID    = '';


			// echo '<pre>validatevalidate';
			// print_r( $value );
			// echo '</pre>';

			if ( is_array( $value ) ) {
				$dateValue = isset($value['date']) ? $value['date'] : $date;
				$timeArray = isset($value['time']) ? explode( "-", $value['time'] ) : array();
				$timeValue = reset( $timeArray );
				$slotID    = isset($value['time']) ? $value['time'] : $slotID;
			}
			
			// Check if services field exists
			if ( gfb_field_type_exists($form, 'gfb_appointment_services') ) {
				// Service & Provider ID

				$service_id   = gfb_get_field_type_value( $form, 'gfb_appointment_services' );

				$provider_id  = gfb_field_type_exists($form, 'gfb_appointment_providers') && 'gfb_providers' == get_post_type(gfb_get_field_type_value($form, 'gfb_appointment_providers'))
				? gfb_get_field_type_value($form, 'gfb_appointment_providers')
				: 0;

			} else {

				if (  get_option( 'gfb_calendar_availability' ) == 'global' && get_option( 'gfb_global_service' ) != '' && get_option( 'gfb_global_service_provider' ) != '' ) {
					$service_id = get_option( 'gfb_global_service' );
					$provider_id = get_option( 'gfb_global_service_provider' );
				} else {
					$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_required_service') );
					return;
				}				
			}
			
			$available_times_mode = (string) get_post_meta( $service_id, 'gfb_service_available_times_mode', true );
			
			/**
			 * Multiple Bookings Validation
			 */
			// Service multiple slots
			$multiple_slots      = (string) get_post_meta( $service_id, 'gfb_service_multiple_selection', true );

			/**
			 * Single Bookings Validation
			 */
			if( gfb_valid_date_format($dateValue) ) {

				$dateTime = new DateTime( $dateValue, new DateTimeZone( gfb_time_zone() ) );

				// Date Slots Mode
				if( $available_times_mode == 'no_slots' )  {

					if( $this->date_valid($form, $service_id, $provider_id, $dateTime) !== true) {
						$message = $this->date_valid($form, $service_id, $provider_id, $dateTime);
						$this->validationFailed( $message );
						return;
					}

					return;
				}

				

				// Time Slots Mode
				if( $this->slot_valid($form, $service_id, $provider_id, $dateTime, $slotID) !== true ) {
					$message = $this->slot_valid($form, $service_id, $provider_id, $dateTime, $slotID);
					$this->validationFailed( $message );
					return;
				}


			} else {
				$this->validationFailed( gfb_get_form_translated_error_message($form_id, 'error_required_date') );
				return;
			}

		} // end validate function



		/**
		 * Date Valid
		 */
		private function date_valid( $form, $service_id, $provider_id, $dateTime ) {
			$form_id = absint( $form['id'] );

			// Date Validation
			$date       = $dateTime->format('Y-m-j');

			// Translation
			$month       = $dateTime->format('F');
			$day         = $dateTime->format('j');
			$year        = $dateTime->format('Y');
			$lang_date   = gfb_get_form_translated_slots_date($form_id, $month, $day, $year);
			// Translation

			if( !class_exists('gfb_calendar') ) {
				require_once( gfb_base_path . '/gbf-fields/gba-calendar.php' );
			}
			
			
			$gfb_calendar       = new gfb_calendar( $form_id, $dateTime->format('n'), $dateTime->format('Y'), $service_id, $provider_id );
			$date_available    = $gfb_calendar->is_date_available( $dateTime );

			if( $date_available ) {
				# valid date
				if( $this->client_booked_date_slot($form, $service_id, $provider_id, $dateTime) ) {
					return gfb_get_form_translated_error_message($form_id, 'error_booked_date', $lang_date);
				}
			} else {
				return gfb_get_form_translated_error_message($form_id, 'error_date_valid', $lang_date);
			}

			return true;
		}

		/**
		 * Slot Valid
		 */
		private function slot_valid( $form, $service_id, $provider_id, $date, $time ) {
			$form_id = absint( $form['id'] );

			// Translation
			$time_display = gfb_service_time_format_display($service_id);			
			if( $time == '' ) {
				return gfb_get_form_translated_error_message($form_id, 'error_required_slot');
			} 

			$gfb_calendar = new gfb_calendar( $form_id, $date->format('n'), $date->format('Y'), $service_id, $provider_id );
			$slots_available = $gfb_calendar->get_slots( $date );

			// Is slot available
			$is_slot_available = array_key_exists($time, $slots_available);
			
			return true;
		}


		/**
		 * Escape SQL RegexP
		 * Characters must be escaped such as: \ ^ . $ | ( ) [ ] * + ? { } ,
		 */
		private function esc_sql_regexp( $str ) {
			return preg_replace('/[.\\\\+*?[\\^\\]$(){}=!|:,\\-]/', '\\\\\\\\\\\\${0}', $str);
		}

		/**
		 * Get Email Value From Submitted Form
		 */
		private function email_field_value( $form ) {

			$exists = gfb_field_type_exists($form, 'email');
			$email_value = '';
			if($exists){
				$email_value = esc_sql(gfb_new_get_field_type_value($form, 'email'));
			}
			return $this->esc_sql_regexp($email_value);
		}



		/**
		 * Get Phone Value From Submitted Form
		 */
		private function phone_field_value( $form ) {
			$phone_value = gfb_field_type_exists($form, 'phone') ? esc_sql(gfb_new_get_field_type_value($form, 'phone')) : '';
			return $this->esc_sql_regexp($phone_value);
		}
		
		
		/**
		 * Client Booked Date Slot
		 * @ $form array
		 * @ $service_id
		 * @ $provider_id
		 * @ $dateTime
		 */
		private function client_booked_date_slot( $form, $service_id, $provider_id, $dateTime ) {
			$date = $dateTime->format("Y-m-j");

			// Prevent Double Bookings
			$double_bookings = gfb_get_service_double_bookings($service_id);
			if( $double_bookings == 'no' ) {
				return false;
			}

			// Client Booked Date Slot
			$email_value  = $this->email_field_value( $form );
			$phone_value  = $this->phone_field_value( $form );

			global $wpdb;
			$querystr = "SELECT $wpdb->posts.ID
			FROM
			$wpdb->posts,
			$wpdb->postmeta AS app_date,
			$wpdb->postmeta AS provider,
			$wpdb->postmeta AS client
			WHERE
			$wpdb->posts.ID = app_date.post_id
			AND
			$wpdb->posts.ID = provider.post_id
			AND
			$wpdb->posts.ID = client.post_id


			AND $wpdb->posts.post_type = 'gfb_appointments'
			AND $wpdb->posts.post_status IN ('completed', 'publish', 'payment', 'pending')

			AND app_date.meta_key   = 'gfb_appointment_date'
			AND app_date.meta_value = %s

			AND provider.meta_key   = 'gfb_appointment_provider'
			AND provider.meta_value = %s

			AND client.meta_key = 'gfb_appointment_new_client'
			AND (client.meta_value REGEXP '\"email\";s:[1-9]+:\"{$email_value}\"'
			OR client.meta_value REGEXP '\"phone\";s:[1-9]+:\"{$phone_value}\"')
			";

			$wpdb->query('SET SQL_BIG_SELECTS = 1');
			$sql_prepare  = $wpdb->prepare($querystr, $date, $provider_id);
			$appointments = $wpdb->get_results( $sql_prepare, ARRAY_A );

			if ( count($appointments) > 0 ) {
				return true;
			} else {
				return false;
			}
		}

		/**
		* gfb save value in database
		**/
		
		public function gfb_save_value($value, $form,$entry_id) {
			
			$form = GFAPI::get_form( $this->formId );

			if ( get_option( 'gfb_calendar_availability' ) == 'global' && get_option( 'gfb_global_service' ) != '' && get_option( 'gfb_global_service_provider' ) != '' ) {
				$service_id   = get_option( 'gfb_global_service' );
				$provider_id   = get_option( 'gfb_global_service_provider' );
			} else {
				$service_id   = gfb_get_field_type_value( $form, 'gfb_appointment_services' );
				$provider_id   = gfb_get_field_type_value( $form, 'gfb_appointment_providers' );
			}

			gform_update_meta( $entry_id, $this->id.'_slot_meeping', $value['slot_meeping_id'] );			

			/* check user is loign */
			if(is_user_logged_in()){
				$user_id = get_current_user_id();
				$customer_id = get_user_meta( $user_id, 'customer_id' , true );
				if ( empty($customer_id) ) {
					/* if customer not registred with customer table */
					$user_details = get_user_by('ID',$user_id);
					$first_name = get_user_meta($user_id, 'first_name', true);
        			$last_name = get_user_meta($user_id, 'last_name', true);
					
					$name = $first_name . ' ' . $last_name;
					$email = $user_details->user_email;
					$created_date = date('Y-m-d H:i:s');
					$ip = $_SERVER['REMOTE_ADDR']; 
					/* Insert Into Customer Table */
					global $wpdb;
					$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
					/* Check Customer Email Exists */
					$customer_result = $wpdb->get_results( 'SELECT c.customer_id,c.customer_email FROM '.$gfb_customer_mst.' as c  WHERE c.customer_email="'.$email.'"', ARRAY_A );				
					if(empty($customer_result)){
						/* Insert Into Customer Table */
						$wpdb->insert($gfb_customer_mst, array( 'customer_name' => $name, 'customer_email' => $email,'created_date'=>$created_date,'ip_address'=>$ip ) );
						$customer_id = $wpdb->insert_id;
						update_user_meta( $user_id, 'customer_id', $customer_id);
					}else{
						$customer_id = $customer_result[0]['customer_id'];
						update_user_meta( $user_id, 'customer_id', $customer_id);
					} 
				}
			} else {
				
				$name_field_id = $form['gfb_booking_addon']['customer_name'];
				$name = gfb_get_name_field_value_by_id($form,$name_field_id);
				$emial_field_id = $form['gfb_booking_addon']['customer_email'];
				$email = gfb_get_field_id_value($form,$emial_field_id);
				$number_field_id = $form['gfb_booking_addon']['customer_number'];
				$number = gfb_get_field_id_value($form,$number_field_id);
				$is_user_email = '';//get_user_by('email', $email);
				
				if(empty($is_user_email)){

					
					$created_date = date('Y-m-d H:i:s');
					$ip = $_SERVER['REMOTE_ADDR']; 
					/* Insert Into Customer Table */
					global $wpdb;
					$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
					/* Check Email Already Exists In Customer Table*/
					$results = $wpdb->get_results( 'SELECT * FROM '.$gfb_customer_mst.' WHERE customer_email= "'.$email.'"',ARRAY_A );
					

					if(empty($results)){
						/* Insert Into Customer Table */
						if(!empty($name) && !empty($email)){
							
							$wpdb->insert($gfb_customer_mst, array('customer_name' => $name, 'customer_email' => $email,'customer_contact' => $number,'created_date'=>$created_date,'ip_address'=>$ip) ); 
							$customer_id = $wpdb->insert_id;
						}else{
							$customer_id="";
						}
					}else{
						
						$customer_id = $results[0]['customer_id'];
					}
				}else{
					$user_id = $is_user_email->ID;
					$customerID = get_user_meta($user_id,'customer_id');
					if(!empty($customerID)){
						$customer_id = $customerID[0];
					}else{
						$name = $is_user_email->user_login;
						$email = $is_user_email->user_email;		
						$created_date = date('Y-m-d H:i:s');
						$ip = $_SERVER['REMOTE_ADDR'];
						/* Insert Into Customer Table */
						global $wpdb;
						$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
						$wpdb->insert($gfb_customer_mst, array('customer_name' => $name, 'customer_email' => $email,'created_date'=>$created_date,'ip_address'=>$ip) ); 
						$customer_id = $wpdb->insert_id;
						/* Add Customer id into customer table */
						update_user_meta( $user_id, 'customer_id', $customer_id);
					}
				}
			}
			
			/* Add New Appointment */
			$ip = $_SERVER['REMOTE_ADDR']; 
			$booking_ref_no = wp_rand( 1, 99999 ).wp_kses_post(date("Ymd"));
			$appointment_date = $value['date'];
			$slot_meeping_id = $value['slot_meeping_id'];
			$slot_qty = $value['slot_qty'];
			$total_amt = $value['cost'];
			$created_date = date('Y-m-d H:i:s');
			$currency_code = get_option('gfb_currency_code');

			global $wpdb;
			global $gfbAppointmentObj;
			global $gfbStaff;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';			
			/* Check Appointment Exists */
			$customer_id =1;
			if(!empty($customer_id) && !empty($appointment_date) && !empty($slot_meeping_id) && !empty($service_id) && !empty($provider_id)){
				$appointment_result = $wpdb->get_results( 'SELECT a.appointment_id FROM '.$gfb_appointments_mst.' as a  WHERE a.customer_id="'.$customer_id.'" AND a.appointment_date="'.$appointment_date.'" AND a.staff_slot_mapping_id='.$slot_meeping_id.' AND a.service_id='.$service_id.' AND a.staff_id='.$provider_id.'', ARRAY_A );				
				if(empty($appointment_result)){
					/* Add Appointment */
					if(!empty($customer_id) && !empty($appointment_date) && !empty($slot_meeping_id) && !empty($service_id) && !empty($provider_id)){

						gform_update_meta( $entry_id, 'slot_qty', $slot_qty );
						gform_update_meta( $entry_id, 'provider_id', $provider_id );
						$wpdb->insert($gfb_appointments_mst, array('customer_id' => $customer_id, 'appointment_date' => $appointment_date,'staff_slot_mapping_id'=>$slot_meeping_id,'service_id'=>$service_id,'staff_id'=>$provider_id,'booking_ref_no'=>$booking_ref_no,'status'=>1,'created_by'=>$customer_id,'created_date'=>$created_date,'ip_address'=>$ip, 'slot_count'=>$slot_qty) ); 
						$appointment_id = $wpdb->insert_id;
						/* Add New Appointment Payment Cost */
						
						$payment_date= $appointment_date." 00:00:00";
						$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
						$wpdb->insert($gfb_payments_mst, array('appointment_id' => $appointment_id,'entry_id'=>$entry_id,'payment_date' => $payment_date,'payment_type'=>3,'total_amt'=>$total_amt,'paid_amt'=>$total_amt,'currency_code'=>$currency_code,'created_by'=>$customer_id,'created_date'=>$created_date,'ip_address'=>$ip) ); 
					}
				}else{
					return $this->validationFailed(gfb_get_form_translated_data($this->formId, 'error_booked_date'));
				}
			}	
			/* get appointment data for google sync entry*/
			if(!empty($appointment_id)){	
				$appointment_res = $gfbAppointmentObj->gfbAppointmentInfoById($appointment_id);
				if(!empty($appointment_res))
					$staffGoogleData = $gfbStaff->gfbStaffGoogleData($appointment_res[0]["staff_id"]);
				
				if(!empty($staffGoogleData)){
					if($staffGoogleData[0]['staff_gcal_data'] != '')
					{
						$staffId = $appointment_res[0]["staff_id"];
						if( $gfbStaff->gfbIsTokenExpire($staffId)) {
							$jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode($staffId ));
							if( !empty( $jsontoken ) ) {						
								$gfbStaff->gfbRefreshToken($jsontoken->refresh_token,$staffId);
								$gcalJSONData = $gfbStaff->gfbStaffGoogleDataEncode($staffId);
							}				
						}
						
						$add_appointments_arg = array(
							'customer_id' 			=> ''.wp_kses_post(trim($appointment_res[0]["customer_id"])).'',
							'appointment_date' 		=> ''.wp_kses_post(trim($appointment_res[0]["appointment_date"])).'',
							'staff_slot_mapping_id' => ''.wp_kses_post(trim($appointment_res[0]["staff_slot_mapping_id"])).'',
							'service_id' 			=> ''.wp_kses_post(trim($appointment_res[0]["service_id"])).'',
							'staff_id'  			=> ''.wp_kses_post(trim($appointment_res[0]["staff_id"])).'',
							'status' 				=> ''.wp_kses_post(trim($appointment_res[0]["status"])).'',
							'booking_ref_no' 		=> ''.wp_kses_post(trim($appointment_res[0]["booking_ref_no"])).'',
						);
						
						$google_event_id = $gfbAppointmentObj->createGoogleCalendarEvent($add_appointments_arg);
						$gfb_ip = GFB_Core::gfbIpAddress();
						$appointment_array = array(
							'google_event_id' => $google_event_id,
							'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',		
							'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
						);
						
						$where_array = array('appointment_id' => ''.wp_kses_post(trim($appointment_id)).'');
						
						$update_data = $wpdb->update($gfb_appointments_mst, $appointment_array , $where_array );
					}
				}
			}
			
		}
		
		
		/**
		* Save value or save single entry edit
		*/
		public function get_value_save_entry( $value, $form, $input_name, $entry_id, $entry ) {
			
			// GF Admin Entry Edit
			if( is_admin() ) {
				return $value;
			}

			$value = gfb_get_field_type_value( $form, 'gfb_appointment_calendar');
			$form_id = absint( $form['id'] );


			
			// Check if services field exists
			if ( gfb_field_type_exists($form, 'gfb_appointment_services') || ( get_option( 'gfb_calendar_availability' ) == 'global' && get_option( 'gfb_global_service' ) != '' && get_option( 'gfb_global_service_provider' ) != '' ) ) {
				
				
				if(!empty($entry_id)){
					$this->gfb_save_value($value, $form, $entry_id);
				}

				return $this->TranslateDateToText($value, $form_id, true, $entry_id);
			} else {
				return '';
			} 
			
		}

		/**
		* Show date on entry single
		*/
		public function get_value_entry_detail( $value, $currency = '', $use_text = false, $format = 'html', $media = 'screen' ) {
			$fieldId = $this->id;
			$inputStr = 'input_' . $fieldId;
			if ( ! empty( $_POST[ 'is_submit_' . $this->formId ] ) ) {
				$value = rgpost($inputStr);
				
				// Service & Provider ID
				$form = GFAPI::get_form( $this->formId );
				$service_id   = gfb_get_field_type_value( $form, 'gfb_appointment_services' );
				$provider_id   = gfb_get_field_type_value( $form, 'gfb_appointment_providers' );
				return $this->TranslateDateToText($value, $this->formId);
			}
			return $value;
		}
		
		/**
		* Merge tag, on notifications, confirmations
		*/
		public function get_value_merge_tag( $value, $input_id, $entry, $form, $modifier, $raw_value, $url_encode, $esc_html, $format, $nl2br ) {
			$dates = explode('&lt;br&gt', $value);

			if( count($dates) > 1 ) {
				return implode(', ', $dates);
			} elseif( count($dates) == 1 ) {
				return reset( $dates );
			} else {
				return '';
			}
		}

		public function get_form_editor_inline_script_on_page_render() {
			return "
			gform.addFilter('gform_form_editor_can_field_be_added', function (canFieldBeAdded, type) {
				if (type == 'gfb_appointment_calendar') {
					if (GetFieldsByType(['gfb_appointment_calendar']).length > 0) {
						alert(" . json_encode( esc_html__( 'Only one Booking Calendar field can be added to the form', 'gravityformscoupons' ) ) . ");
						return false;
					}
				}
				return canFieldBeAdded;
			});";
		}

		/**
		 * Get timestamp from valid date
		 */
		public function get_date_timestamp( $date_time ) {
			$date = new DateTime( $date_time, new DateTimeZone( gfb_time_zone() ) );
			return $date->getTimestamp();
		}


		/**
		* Format the entry value for display on the entries list page.
		* Return a value that's safe to display on the page.
		*/
		public function get_value_entry_list( $value, $entry, $field_id, $columns, $form ) {
			echo $value;
		}
		
		public function save_entry( $entry_value, $entry_id ) {
			gform_update_meta( $entry_id, $this->id, $entry_value );
		}	

		private function TranslateDateToText($value, $formId, $saveOnReturn = false, $entryId = ''){
			$date = '';
			$time = '';
			$slotID = '';

			if ( is_array( $value ) ) {
				$date      = isset($value['date']) ? $value['date'] : $date;
				$timeArray = isset($value['time']) ? explode( "-", $value['time'] ) : array();
				$time      = reset( $timeArray );
				$slotID    = isset($value['time']) ? $value['time'] : $slotID;
			}
			
			$form = GFAPI::get_form( $formId );
			
			
			// Service & Provider ID
			$service_id   = gfb_get_field_type_value( $form, 'gfb_appointment_services' );
			$provider_id   = gfb_get_field_type_value( $form, 'gfb_appointment_providers' );

			if( !class_exists('gfb_calendar') ) {
				require_once( gfb_base_path . '/gbf-fields/gbf-calendar.php');
			}

			// Time Format Display
			$time_display         = gfb_service_time_format_display($service_id);
			// Service Mode
			$available_times_mode = (string) get_post_meta( $service_id, 'gfb_service_available_times_mode', true );
			
			/**
			 * Single Booking
			 */
				// DATE
			$app_date            = (string) $date;
			$date                = gfb_valid_date_format($app_date) ? new DateTime($app_date) : false;
			$app_date_text       = $date ? $date->format('l, F j Y') : '(Date not selected)';

				// Time
			$app_time            = $time;
			$time                = gfb_valid_time_format($app_time) ? new DateTime($app_time) : false;
			$app_time_text       = $time ? $time->format('h:i:s-h:i:s') : '(Time not selected)';
			
			$slotTime = $value['time'];
			$dateFormate = $this->validateDate($slotTime);
			if($dateFormate ==1){
				$Newtime = $value['time'];
				$appointment_date_result = "{$app_date_text} at {$Newtime}";
			}else{
				$Newtime = "Time not selected";
				$appointment_date_result = "{$app_date_text}";
			}
			$appointment_date = $appointment_date_result;
			
			

			$merge = apply_filters('gfb_booking_merge_value', 'date_format');
			$entry_date = $merge == 'timestamp' && $date && $time ? $this->get_date_timestamp( "{$date->format('Y-m-j')} {$time->format('H:i')}" ) : $appointment_date;
			if($saveOnReturn){
				$this->save_entry( $entry_date, $entryId );
			}
			return $entry_date;
		}
		//check validate date function
	public function validateDate($slotTime){
		$tempDate = explode('-', $slotTime);
			// checkdate(month, day, year)
		if(empty($tempDate[2])){
			$result = 1;
			return $result;
		}else{
			$result = 0;
			return $result;
		}
	}
	
	
	
	} // end class
	GF_Fields::register( new GFB_Appointment_Booking_Calendar() );
	
	
	
} // end if
