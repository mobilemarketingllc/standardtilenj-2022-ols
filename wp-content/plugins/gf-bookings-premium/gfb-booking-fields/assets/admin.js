( function($) {
	/**
	 * RENDER CHOSEN JS ON LOAD
	 */
	jQuery(document).ready(function() {
		jQuery(".chosen-select").chosen();
		
	});

	/**
	 * Appointment status change AJAX
	 */
	jQuery('.appointment_status_select').on('change', function(){
		// wp ajax
		var data = {
			'action': 'gfb_change_appointment_status',
			'app_id': jQuery(this).attr('app-id'),
			'status': this.value,
		};
		var tr = '#post-'+jQuery(this).attr('app-id');
		jQuery.post(gfb_change_appointment_status_obj.ajax_url, data, function() {
		}).fail(function() {
			alert('There was a problem changing the appointment status');
		}).done(function(){
			jQuery(tr).addClass('successful_change_tr');
			setTimeout(function(){
				jQuery(tr).removeClass('successful_change_tr');
			}, 100);
		});


	});

	/**
	 * Appointment Reschedule pending hide
	 */
	jQuery('body').on('change', '#appointment_reschedule', function() {
		//alert( jQuery(this).val() );
		var type = jQuery(this).val();
		if( type === 'yes' ) {
			jQuery('#tr_reschedule_appointment_pending').removeClass('hide_reschedule_appointment_pending')
		} else if( type === 'no' ) {
			jQuery('#tr_reschedule_appointment_pending').addClass('hide_reschedule_appointment_pending');
		}

	});


	/**
	 * Appointment Reschedule pending to confirmed user hide
	 */
	jQuery('body').on('change', '#user_set_appointment_pending', function() {
		//alert( jQuery(this).val() );
		var type = jQuery(this).val();
		if( type === 'yes' ) {
			jQuery('#tr_reschedule_pending_to_confirmed').removeClass('hide_user_set_appointment_confirmed_from_pending')
		} else if( type === 'no' ) {
			jQuery('#tr_reschedule_pending_to_confirmed').addClass('hide_user_set_appointment_confirmed_from_pending');
		}

	});


	/**
	 * Disable appointment set to pending after user reschedules
	 */
	jQuery('body').on('change', '#user_set_appointment_confirmed_from_pending', function() {
		//alert( jQuery(this).val() );
		var type = jQuery(this).val();
		if( type === 'yes' ) {
			jQuery('#appointment_reschedule_pending').val('no')
		} else if( type === 'no' ) {
			jQuery('#appointment_reschedule_pending').val('yes');
		}

	});


	/**
	 * Disable user set to confirmed if appointment reschedule to pending is turned on
	 */
	jQuery('body').on('change', '#appointment_reschedule_pending', function() {
		var type = jQuery(this).val();
		if( type === 'yes' ) {
			jQuery('#user_set_appointment_confirmed_from_pending').val('no')
		} else if( type === 'no' ) {
			jQuery('#user_set_appointment_confirmed_from_pending').val('yes');
		}

	});




	/**
	 * Appointment Type
	 */	 		
	jQuery('body').on('change', '#gfb_appointment_type', function() {
		//alert( jQuery(this).val() );
		var type = jQuery(this).val();
		
		if( type == 'time_slot' ) {
			jQuery('.cmb2-id-ga-appointment-duration, .cmb2-id-ga-appointment-time').show();
		} else if( type == 'date' ) {
			jQuery('.cmb2-id-ga-appointment-duration, .cmb2-id-ga-appointment-time').hide();
		}
		
    });

	/**
	 *  Cancellation Policy Custom Timeframe
	 */
	jQuery('body').on('change', '#cancellation_notice', function(){
		var type = jQuery(this).val();
		if( type == 'custom' ) {
			jQuery('#tr_cancelllation_notice_timeframe').show();
		}else{
			jQuery('#tr_cancelllation_notice_timeframe').hide();
		}
	});

	/**
	 * Appointment Cancel Message
	 */	 		
	jQuery('body').on('change', '#cmb2-metabox-gfb_appointment_submitdiv #post-status', function() {
		//alert( jQuery(this).val() );
		var postStatus = jQuery(this).val();
		
		if( postStatus == 'cancelled' ) {
			jQuery('#cmb2-metabox-gfb_appointment_submitdiv .gfb_cancel_message').removeClass('cmb2-hidden');
		} else {
			jQuery('#cmb2-metabox-gfb_appointment_submitdiv .gfb_cancel_message').addClass('cmb2-hidden');
		}
    });

	/**
	 *  Autocomplete appointment custom show/hide
	 */
	jQuery('body').on('change', '#auto_complete', function(){
		var type = jQuery(this).val();
		if( type == 'custom' ) {
			jQuery('#tr_auto_complete_custom').show();
		}else{
			jQuery('#tr_auto_complete_custom').hide();
		}
	});

	/**
	 * Available Times Mode
	 */	 		
	jQuery('body').on('change', '.gfb_service_available_times_mode', function() {
		var timeMode = jQuery(this).val();
		
		var excluded = [
			'cmb2-id-gfb_service_available_times_mode',		
			'cmb2-id-gfb_gbf-service-schedule-lead-time-minutes',
			'cmb2-id-gfb_gbf-service-period-type',
			'cmb2-id-gfb_gbf-service-max-bookings',
			'cmb2-id-gfb_gbf-service-multiple-selection',
			'cmb2-id-gfb_gbf-service-max-selection',
			'cmb2-id-gfb_gbf-service-double-bookings',
		];	
	
		var interval = [
			'cmb2-id-gfb-gbf-service-price',
			'cmb2-id-gfb-gbf-service-duration',
			'cmb2-id-gfb-gbf-service-cleanup',
			'cmb2-id-gfb-gbf-service-capacity',
			'cmb2-id-gfb-gbf-service-gaps',
			'cmb2-id-gfb-gbf-service-format',
			'cmb2-id-gfb-gbf-service-show-end-times',
			'cmb2-id-gfb-gbf-service-remove-am-pm',
		];
		
		var custom = [
			'cmb2-id-gfb_service_custom_slots',
			'cmb2-id-gfb-gbf-service-format',
			'cmb2-id-gfb-gbf-service-show-end-times',
			'cmb2-id-gfb-gbf-service-remove-am-pm',
		];
		
		var dates = [
			'cmb2-id-gfb-gbf-service-price',
			'cmb2-id-gfb-gbf-service-capacity',
		];		
		
		jQuery('#cmb2-metabox-gfb_services_details .cmb-row').each(function() {
			var field_class = jQuery(this).attr('class').match(/cmb2-id-[^ ]+/);
			var data = [];

			if( timeMode == 'interval' ) {
				data = interval;
			} else if( timeMode == 'custom' ) {
				data = custom;
			} else if( timeMode == 'no_slots' ) {
				data = dates;
			}			

			if( $.inArray(field_class[0], data) >= 0 ) {
				jQuery(this).removeClass('cmb2-hidden');
			} else {
				if( $.inArray(field_class[0], excluded) >= 0 ) {
					jQuery(this).removeClass('cmb2-hidden');
				} else {
					jQuery(this).addClass('cmb2-hidden');
				}
			}
		});	
    });			
	
	/**
	 * Calendar Availability Type
	 */	 
	jQuery('body').on('change', '#gfb_service_period_type', function() {
		var period_type = jQuery(this).val();
		
		if( period_type == 'future_days' ) {
			jQuery('.cmb2-id-gfb_service_date_range, .cmb2-id-gfb_service_custom_dates').hide(); // Hide other field
			jQuery('.cmb2-id-gfb-gbf-service-schedule-max-future-days').show();
		} else if( period_type == 'date_range' ) {
			jQuery('.cmb2-id-gfb_service_date_range').show();
			jQuery('.cmb2-id-gfb-gbf-service-schedule-max-future-days, .cmb2-id-gfb_service_custom_dates').hide(); // Hide other field
		} else if( period_type == 'custom_dates' ) {
			jQuery('.cmb2-id-gfb_service_custom_dates').show();
			jQuery('.cmb2-id-gfb-gbf-service-schedule-max-future-days, .cmb2-id-gfb_service_date_range').hide(); // Hide other field			
		}

    });	

	/**
	 * Add Color Picker to all inputs that have 'color-field' class
	 */
	jQuery('.color-field').wpColorPicker();
	
	

	/**
	 * Deselect Services Type
	 */	   
	jQuery('body').on('change', '.gfb_provider_service_type', function() {
		var service_type = jQuery(this).parent('label').attr('class');

		if( service_type == 'gfb_provider_service_slots' ) {
			jQuery('#gfb_dates_services input:checkbox').removeAttr("checked");
		}

		if( service_type == 'gfb_provider_service_dates' ) {
			jQuery('#gfb_time_slots_services input:checkbox').removeAttr("checked");
		}		

    });	

	/**
	 * ADD NEW CUSTOM DATE
	 */		
	jQuery('body').on('click', '.custom_dates_period .gfb_add_custom_date', function() {
        var cloned = jQuery(this).parent('.custom_dates_period').find('#custom_dates_period .custom-date').first().clone().removeAttr( 'style' );
		jQuery(this).parent('.custom_dates_period').find('#custom_dates_period').append( cloned );
    });	
	

	/**
	 * REMOVE CUSTOM DATE
	 */	   
	jQuery('body').on('click', '#custom_dates_period .custom-date .custom-date-delete', function() {
        jQuery(this).parent('.custom-date').fadeOut(150, function() {
			jQuery(this).remove();
		}); 
    });	

	
	
} ) ( jQuery );

