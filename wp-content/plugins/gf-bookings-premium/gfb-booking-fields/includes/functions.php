<?php
defined('ABSPATH') or exit; // Exit if accessed directly

function gfb_time_interval_opts () {

	$arr = array(
		'1'   => __('Every 1 minutes', 'gfb'),
		'5'   => __('Every 5 minutes', 'gfb'),
		'10'  => __('Every 10 minutes', 'gfb'),
		'15'  => __('Every 15 minutes', 'gfb'),
		'20'  => __('Every 20 minutes', 'gfb'),
		'25'  => __('Every 25 minutes', 'gfb'),
		'30'  => __('Every 30 minutes', 'gfb'),
		'45'  => __('Every 45 minutes', 'gfb'),
		'60'  => __('Every 1 hour', 'gfb'),
		'90'  => __('Every 1.5 hours', 'gfb'),
		'120' => __('Every 2 hours', 'gfb')
	);

	return apply_filters( 'change_gfb_time_intervals', $arr);
}

function gfb_time_duration_opts () {

	$arr = array(
		'5'   => __('5 minutes', 'gfb'),
		'10'  => __('10 minutes', 'gfb'),
		'15'  => __('15 minutes', 'gfb'),
		'20'  => __('20 minutes', 'gfb'),
		'25'  => __('25 minutes', 'gfb'),
		'30'  => __('30 minutes', 'gfb'),
		'45'  => __('45 minutes', 'gfb'),
		'60'  => __('1 hour', 'gfb'),
		'90'  => __('1.5 hours', 'gfb'),
		'120' => __('2 hours', 'gfb')
	);

	return apply_filters( 'change_gfb_time_durations', $arr);
}


/**
* Create time slots between two given times
* Created by developer53 for future purpose
*/
function SplitTime( $StartTime='', $EndTime='', $duration = 5, $interval = 5 ) {
    
    $ReturnArray = array ();// Define output
    $StartTime    = strtotime ($StartTime); //Get Timestamp
    $EndTime      = strtotime ($EndTime); //Get Timestamp

    $AddMins  = $duration * 60;
    $gap = $interval * 60;
    $i = 1;

    while ( $StartTime < $EndTime ) { //Run loop  
    	
    	if ( $i > 1 ) {
			$StartTime += $gap;		
    	}

    	//if ( ( $StartTime + $AddMins ) < ( $EndTime ) ) {
	        $ReturnArray['slots'][] = date ( "G:i:s", $StartTime ) . ' - ' . date ( "G:i:s", ( $StartTime + $AddMins ) );
	        $ReturnArray['add_break'][] = 'false';
	        $ReturnArray['slot_start_time'][] = date ( "G:i:s", $StartTime );
	        $ReturnArray['slot_end_time'][] = date ( "G:i:s", ( $StartTime + $AddMins ) );
	        $StartTime += $AddMins; //Endtime check
	    //}

        $i++;
    }

    return $ReturnArray;
}

/**
 * Convert Manual Offset to Proper UTC Timezone
 */ 
function convert_manual_offset ( $utc='' ) {
	if ( empty( $utc ) ) {
		return;
	}

	$utc = str_replace( 'UTC', '', $utc );
	$utc = str_replace( '.5', ':30', $utc );
	$utc = str_replace( '.75', ':45', $utc );

	return $utc;
}

/**
 * Valid Date Format: Y-m-j
 */
function gfb_valid_date_format($date_input)
{
	$d = DateTime::createFromFormat('Y-m-j', $date_input);
	return $d && ($d->format('Y-m-j') == $date_input);
}

/**
 * Valid Month Year Date Format: Y-m
 */
function gfb_valid_year_month_format($date_input)
{
	//$d = DateTime::createFromFormat('Y-m', $date_input);
	//return $d && $d->format('Y-m') === $date_input;
	return $date_input == date('Y-m', strtotime($date_input . '-01'));
}

/**
 * Valid Time Format: H:i //24h
 */
function gfb_valid_time_format($time_input)
{
	$t = DateTime::createFromFormat('H:i', $time_input);
	return $t && $t->format('H:i') === $time_input;
}

/**
 * Valid Date & Time Format: Y-m-j H:i
 */
function gfb_valid_date_time_format($date_time)
{
	$t = DateTime::createFromFormat('Y-m-j H:i', $date_time);
	return $t && $t->format('Y-m-j H:i') === $date_time;
}


/**
 * Get service end time duration
 */
function gfb_get_time_end($slot_start, $service_id)
{
	$duration = (int) get_post_meta($service_id, 'gfb_service_duration', true);

	$slot_end = new DateTime($slot_start);
	$interval = new DateInterval("PT" . $duration . "M");
	$slot_end->add($interval);
	$time_end = $slot_end->format('H:i') == '00:00' ? '23:59' : $slot_end->format('H:i');

	return $time_end;
}

/**
 * Translate calendar am_pm
 */
function gfb_get_form_translated_am_pm($form, $data)
{
	$am = gfb_get_form_translated_data($form, 'am');
	$pm = gfb_get_form_translated_data($form, 'pm');

	$find = array(
		'am',
		'pm',
	);

	$replace = array(
		$am,
		$pm,
	);

	return str_ireplace($find, $replace, $data);
}

/**
 * Translate calendar space
 */
function gfb_get_form_translated_space($form, $total)
{
	$data = gfb_get_form_translated_data($form, 'space');

	return str_ireplace('[total]', $total, $data);
}

/**
 * Translate calendar spaces
 */
function gfb_get_form_translated_spaces($form, $total)
{
	$data = gfb_get_form_translated_data($form, 'spaces');

	return str_ireplace('[total]', $total, $data);
}


/**
 * Translate Date & Time
 */
function gfb_get_form_translated_date_time($form, $month, $week, $day, $year, $time)
{
	$month      = strtolower($month);
	$data       = gfb_get_form_translated_data($form, 'date_time_' . $month);
	$long_weeks = gfb_get_form_translated_data($form, 'long_weeks');
	$week       = strtolower($week);
	$week       = isset($long_weeks[$week]) ? $long_weeks[$week] : $week;

	$time       = gfb_get_form_translated_am_pm($form, $time);

	$find = array(
		'[week_long]',
		'[day]',
		'[year]',
		'[time]',
	);

	$replace = array(
		$week,
		$day,
		$year,
		$time,
	);

	return str_ireplace($find, $replace, $data);
}

/**
 * Translate calendar heading
 */
function gfb_get_form_translated_month($form, $month, $year)
{
	$month = strtolower($month);
	$data  = gfb_get_form_translated_data($form, $month);

	return str_ireplace('[year]', $year, $data);
}

/**
 * Translate calendar slots availability
 */
function gfb_get_form_translated_slots_date($form, $month, $day, $year)
{
	$month = strtolower($month);
	$data = gfb_get_form_translated_data($form, 'slots_' . $month);

	$find = array(
		'[day]',
		'[year]',
	);

	$replace = array(
		$day,
		$year,
	);

	return str_ireplace($find, $replace, $data);
}

/**
 * Translate: Error message
 */
function gfb_get_form_translated_error_message($form, $error, $date = false)
{
	$data = gfb_get_form_translated_data($form, $error);

	if ($date) {
		return str_ireplace('[date]', $date, $data);
	} else {
		return $data;
	}
}

/**
 * Translate: Error message
 */
function gfb_get_form_translated_error_max_bookings($form, $date, $total)
{
	$data = gfb_get_form_translated_data($form, 'error_max_bookings');

	$find = array(
		'[date]',
		'[total]',
	);

	$replace = array(
		$date,
		$total,
	);

	return str_ireplace($find, $replace, $data);
}

/**
 * Translate: Client Service Title
 */
function gfb_get_translated_client_service($form = false, $service_name, $provider_name)
{
	$data = gfb_get_form_translated_data($form, 'client_service');

	$find = array(
		'[service_name]',
		'[provider_name]',
	);

	$replace = array(
		$service_name,
		$provider_name,
	);

	return str_ireplace($find, $replace, $data);
}


/**
 * Translate: Client Provider Title
 */
function gfb_get_translated_provider_service($form = false, $service_name, $client_name)
{
	$data = gfb_get_form_translated_data($form, 'provider_service');

	$find = array(
		'[service_name]',
		'[client_name]',
	);

	$replace = array(
		$service_name,
		$client_name,
	);

	return str_ireplace($find, $replace, $data);
}

/**
 * Default Translation Data
 */
function gfb_get_translated_data($translate)
{
	$lang = get_option('gfb_appointments_translation');

	if (isset($lang[$translate])) {
		return $lang[$translate];
	}

	$default = array(
		// Calendar week days short names
		'weeks'     => array('sun' => 'Sun', 'mon' => 'Mon', 'tue' => 'Tue', 'wed' => 'Wed', 'thu' => 'Thu', 'fri' => 'Fri',  'sat' => 'Sat'),

		// Calendar week days long names
		'long_weeks'     => array('sunday' => 'Sunday', 'monday' => 'Monday', 'tuesday' => 'Tuesday', 'wednesday' => 'Wednesday', 'thursday' => 'Thursday', 'friday' => 'Friday',  'saturday' => 'Saturday'),

		// Calendar month year
		'january'   => 'January [year]',
		'february'  => 'February [year]',
		'march'     => 'March [year]',
		'april'     => 'April [year]',
		'may'       => 'May [year]',
		'june'      => 'June [year]',
		'july'      => 'July [year]',
		'august'    => 'August [year]',
		'september' => 'September [year]',
		'october'   => 'October [year]',
		'november'  => 'November [year]',
		'december'  => 'December [year]',

		// Calendar time slots
		'slots_january'   => 'January [day], [year]',
		'slots_february'  => 'February [day], [year]',
		'slots_march'     => 'March [day], [year]',
		'slots_april'     => 'April [day], [year]',
		'slots_may'       => 'May [day], [year]',
		'slots_june'      => 'June [day], [year]',
		'slots_july'      => 'July [day], [year]',
		'slots_august'    => 'August [day], [year]',
		'slots_september' => 'September [day], [year]',
		'slots_october'   => 'October [day], [year]',
		'slots_november'  => 'November [day], [year]',
		'slots_december'  => 'December [day], [year]',

		// Calendar date/time
		'date_time_january'   => '[week_long], January [day] [year] at [time]',
		'date_time_february'  => '[week_long], February [day] [year] at [time]',
		'date_time_march'     => '[week_long], March [day] [year] at [time]',
		'date_time_april'     => '[week_long], April [day] [year] at [time]',
		'date_time_may'       => '[week_long], May [day] [year] at [time]',
		'date_time_june'      => '[week_long], June [day] [year] at [time]',
		'date_time_july'      => '[week_long], July [day] [year] at [time]',
		'date_time_august'    => '[week_long], August [day] [year] at [time]',
		'date_time_september' => '[week_long], September [day] [year] at [time]',
		'date_time_october'   => '[week_long], October [day] [year] at [time]',
		'date_time_november'  => '[week_long], November [day] [year] at [time]',
		'date_time_december'  => '[week_long], December [day] [year] at [time]',

		// AM/PM
		'am' => 'AM',
		'pm' => 'PM',

		// Capacity
		'space'  => '[total] space available',
		'spaces' => '[total] spaces available',

		// Front-end shortcodes
		'manage_text'       => 'Manage schedule',
		'schedule'          => 'Schedule',
		'breaks'            => 'Breaks',
		'holidays'          => 'Holidays',
		'schedule_updated'  => 'Work schedule updated.',
		'upcoming'          => 'Upcoming',
		'past'              => 'Past',
		'client_service'    => '[service_name] with [provider_name]',
		'provider_service'  => '[service_name] with [client_name]',
		'no_appointments'   => 'You do not have any appointments!',
		'user_set_app_pending' => 'Set appointment status to pending',
		'user_set_app_pending_btn_yes' => 'Set to pending',
		'add_to_calendar'   => 'Add to calendar',
		'apple_calendar'    => 'Apple calendar',
		'google_calendar'   => 'Google calendar',
		'outlook_calendar'  => 'Outlook calendar',
		'yahoo_calendar'    => 'Yahoo calendar',
		'bookable_date'     => 'Full day',
		'status_completed'  => 'Completed',
		'status_publish'    => 'Confirmed',
		'status_payment'    => 'Pending payment',
		'status_pending'    => 'Pending',
		'status_cancelled'  => 'Cancelled',
		'status_failed'     => 'Failed',
		'confirm_button'    => 'Confirm',
		'cancel_button'     => 'Cancel',
		'update_button'     => 'Update',
		'reschedule_button' => 'Reschedule',
		'confirm_text'      => 'Confirm appointment',
		'cancel_text'       => 'Cancel appointment',
		'reschedule_text'   => 'Reschedule appointment',
		'close_button'      => 'Close',
		'optional_text'     => 'Optional message',
		'reschedule_optional_text' => 'Optional message',
		'app_confirmed'     => 'Appointment has been confirmed.',
		'app_cancelled'     => 'Appointment has been cancelled.',
		'app_rescheduled'   => 'Appointment has been rescheduled',
		'app_set_pending'   => 'Appointment has been set to pending',
		'error'             => 'Something went wrong.',
		'unselected_time_date' => 'Please select time and date',
		'current_date_time' => 'Current date and time',

		// Appointment Cost
		'app_cost_text'  => 'Appointment Cost',

		// Validation messages
		'error_required'           => 'This field is required',
		'error_reached_max'        => 'You have reached the maximum number of booking allowed for [date]',
		'error_required_date'      => 'Date was not selected',
		'error_max_bookings'       => 'Maximum of [total] bookings allowed for [date]',
		'error_required_service_category'   => 'Service Category was not selected',
		'error_required_service'   => 'Service was not selected',
		'error_required_staff'   => 'Staff was not selected',
		'error_booked_date'        => 'You already booked [date]',
		'error_date_valid'         => 'Date [date] is not available.',
		'error_slot_valid'         => 'Time slot on [date] is not available',
		'error_required_slot'      => 'Time was not selected',
		'error_services_form'      => 'Add booking services field to form',
		'error_service_valid'      => 'Service not found',
		'error_required_provider'  => 'Provider not selected.',
		'error_providers_service'  => 'Providers service not found.',
		'error_no_services'        => 'No service found.',
	);

	if (isset($default[$translate])) {
		return $default[$translate];
	}

	return '';
}


/**
 * Form Translation Data
 */
function gfb_get_form_translated_data($form_id, $translate)
{
	$form = GFAPI::get_form($form_id);
	$form_lang = rgar($form, 'gfbgbfappointments_translation');

	if (isset($form_lang[$translate])) {
		return $form_lang[$translate];
	} else {
		return gfb_get_translated_data($translate);
	}
}

/**
 * Sort Dates
 */
function gfb_date_format_sort($a, $b)
{
	return strtotime($a) - strtotime($b);
}

/**
 * Sort DateTime
 */
function gfb_date_time_sort($a, $b)
{

	try {
		$sort = new DateTime($a['date']) > new DateTime($a['date']);
		return $sort;
	} catch (Exception $e) {
		$sort = new DateTime($a['date_time']) > new DateTime($a['date_time']);
		return $sort;
	}

	return new DateTime($a) > new DateTime($b);
}


/**
 * Get Slots Total
 */
function gfb_get_slots_total($form_id, $service_id, $provider_id, $bookings)
{
	if (!class_exists('gfb_calendar')) {
		require_once(gfb_base_path . '/gbf-fields/gbf-calendar.php');
	}

	$now = gfb_current_date_with_timezone();
	$gfb_calendar  = new gfb_calendar($form_id, $now->format('n'), $now->format('Y'), $service_id, $provider_id);

	$service_price = 0;
	foreach ($bookings as $key => $booking) {
		$dateTime       = new DateTime(sprintf('%s %s', $booking['date'], $booking['time']), new DateTimeZone(gfb_time_zone()));
		$service_price += $booking['price'];
	}

	return $service_price;
}

/**
 * Get Slots Total
 */
function gfb_get_slot_price($form_id, $date, $service_id, $provider_id)
{
	if (!class_exists('gfb_calendar')) {
		require_once(gfb_base_path . '/gbf-fields/gbf-calendar.php');
	}

	$gfb_calendar = new gfb_calendar($form_id, $date->format('n'), $date->format('Y'), $service_id, $provider_id);
	$slots       = $gfb_calendar->get_slots(clone $date);
	$time_slot   = $date->format('H:i');
	return $slots[$time_slot]['price'];
}


/**
 * Add Appointment Cost To Total
 */
add_filter('gform_product_info', 'gfb_add_ga_appointment_fee', 10, 3);
function gfb_add_ga_appointment_fee($product_info, $form, $lead)
{
	if (gfb_field_type_exists($form, 'gfb_appointment_services')) {
		if (is_numeric(gfb_get_field_type_value($form, 'gfb_appointment_services'))) {
			
			// Service ID
			$service_id = absint( gfb_get_field_type_value( $form, 'gfb_appointment_services' ) );
			// Provider ID
			$provider_id = absint( gfb_get_field_type_value( $form, 'gfb_appointment_providers' ) );
			// Service Price
			global $wpdb;
			$service_maping_tbl = $wpdb->prefix . 'gfb_staff_service_mapping';			
			$service_result = $wpdb->get_results( 'SELECT service_id, service_price FROM '.$service_maping_tbl.' WHERE service_id = "'.$service_id.'" AND staff_id = "'.$provider_id.'" AND is_deleted=0', ARRAY_A );				
				//return $service_result;	
			if(!empty($service_result)){
				$service_price = gfb_to_money($service_result[0]['service_price']);
			}else{
				global $gfbServiceObj;
				$serviceDetails = $gfbServiceObj->gfbServiceDetails($service_id);
				if(!empty($serviceDetails)){
					$service_price = gfb_to_money($serviceDetails[0]['service_price']);
				}else{
					$service_price= gfb_to_money(0);
				}
			}
			$form_id = $form['id'];
			$app_cost_text  = gfb_get_form_translated_data($form_id, 'app_cost_text');
			$app_cost_text  = esc_html($app_cost_text);
			
			/**
			* Single Booking
			*/
			$product_info['products']['appointment_cost'] = array('name' => $app_cost_text, 'price' => $service_price, 'quantity' => 1);
			
		}
	}

	return $product_info;
}
/**
 * Service Max Bookings
 */
function gfb_get_service_max_bookings($service_id)
{
	$range = range(1, 150);
	$max_bookings = (int) get_post_meta($service_id, 'gfb_service_max_bookings', true);

	if (in_array($max_bookings, $range)) {
		return $max_bookings;
	} else {
		return 3;
	}
}

/**
 * Service Max Selection
 */
function gfb_get_service_max_selection($service_id)
{
	$range = range(1, 150);
	$max_selection = (int) get_post_meta($service_id, 'gfb_service_max_selection', true);

	if (in_array($max_selection, $range)) {
		return $max_selection;
	} else {
		return 3;
	}
}

/**
 * Service Prevent Double Bookings
 */
function gfb_get_service_double_bookings($service_id)
{
	$defaults = array('yes', 'no');
	$double_bookings = (string) get_post_meta($service_id, 'gfb_service_double_bookings', true);

	if (in_array($double_bookings, $defaults)) {
		return $double_bookings;
	} else {
		return 'yes';
	}
}



/**
 * Field Type Exists
 */
function gfb_field_type_exists($form, $field_type)
{
	if (isset($form['fields'])) {
		foreach ($form['fields'] as $field) {
			if ($field['type'] == $field_type) {
				return true;
			}
		}
	}
	return false;
}

/**
 * Get Field Type Value
 */
function gfb_get_field_type_value($form, $field_type)
{
	if (isset($form['fields'])) {
		foreach ($form['fields'] as $field) {
			if ($field['type'] == $field_type) {

				$id = $field['id'];
				$input = "input_{$id}";

				if (isset($_POST[$input])) {
					if (is_array($_POST[$input])) {
						return $_POST[$input];
					} else {
						return esc_html($_POST[$input]);
					}
				}

				return false;
			}
		}
	}

	return '';
}



/**
 * Get Field Value BY ID 
 */
function gfb_get_field_id_value($form, $field_id)
{
	if (isset($form['fields'])) {
		foreach ($form['fields'] as $field) {
			if ($field['id'] == $field_id) {

				$id = $field['id'];
				$input = "input_{$id}";

				if (isset($_POST[$input])) {
					if (is_array($_POST[$input])) {
						return $_POST[$input];
					} else {
						return esc_html($_POST[$input]);
					}
				}
				return false;
			}
		}
	}

	return '';
}

/**
 * Get Field Name Value By ID
 */
function gfb_get_name_field_value_by_id($form,$field_id)
{

	if (isset($form['fields'])) {
		foreach ($form['fields'] as $field) {
			/* if ($field['type'] == 'name') { */
				$input_id = 'input_' . $field_id;
				$value = isset($_POST) ? $_POST : '';

				if (is_array($value)) {
					$prefix = trim(rgget($input_id . '_2', $value));
					$first  = trim(rgget($input_id . '_3', $value));
					$middle = trim(rgget($input_id . '_4', $value));
					$last   = trim(rgget($input_id . '_6', $value));
					$suffix = trim(rgget($input_id . '_8', $value));

					$name = $prefix;
					$name .= !empty($first) ? " $first" : $first;
					$name .= !empty($middle) ? " $middle" : $middle;
					$name .= !empty($last) ? " $last" : $last;
					$name .= !empty($suffix) ? " $suffix" : $suffix;
					$name  = esc_html($name);
				} else {
					$name = esc_html($value);
				}

				$name = trim($name);
				return $name;
			/* } */
		}
	}

	return '';
}





/**
 * Get Field Type Value
 */
function gfb_get_name_field_value($form)
{

	if (isset($form['fields'])) {
		foreach ($form['fields'] as $field) {
			if ($field['type'] == 'name') {

				$field_id = $field['id'];
				$input_id = 'input_' . $field_id;

				$value = isset($_POST) ? $_POST : '';

				if (is_array($value)) {
					$prefix = trim(rgget($input_id . '_2', $value));
					$first  = trim(rgget($input_id . '_3', $value));
					$middle = trim(rgget($input_id . '_4', $value));
					$last   = trim(rgget($input_id . '_6', $value));
					$suffix = trim(rgget($input_id . '_8', $value));

					$name = $prefix;
					$name .= !empty($first) ? " $first" : $first;
					$name .= !empty($middle) ? " $middle" : $middle;
					$name .= !empty($last) ? " $last" : $last;
					$name .= !empty($suffix) ? " $suffix" : $suffix;
					$name  = esc_html($name);
				} else {
					$name = esc_html($value);
				}

				$name = trim($name);
				return $name;
			}
		}
	}

	return '';
}


/**
 * Service Time Format Display
 */
function gfb_service_time_format_display($service_id)
{
	// Time Format
	$time_format = get_post_meta($service_id, 'gfb_service_time_format', true);
	if ($time_format && in_array($time_format, array('12h', '24h'))) {
		$time_format = $time_format;
	} else {
		$time_format = '12h'; // 12h format
	}

	// Time Format
	$remove_am_pm = get_post_meta($service_id, 'gfb_service_remove_am_pm', true);
	if ($remove_am_pm && in_array($remove_am_pm, array('no', 'yes'))) {
		$remove_am_pm = $remove_am_pm;
	} else {
		$remove_am_pm = 'no'; // 12h format
	}


	$am_pm    = $remove_am_pm == 'no' ? 'A' : '';
	$display  = $time_format == '24h' ? "G:i {$am_pm}" : "g:i {$am_pm}";

	return $display;
}


/**
 * Current Date & Time with TimeZone
 */
function gfb_current_date_with_timezone( $staff_id = '' )
{
	// echo "staff id is " . $staff_id;
	// die();
	$timezone = gfb_time_zone( $staff_id );

	$now = new DateTime();
	$now->setTimezone(new DateTimeZone($timezone));

	return $now;
}

/**
 * Get Settings TimeZone
 */
function gfb_time_zone( $staff_id='' ) {
	
	$staff_timezone = '';

	if ( $staff_id != '' && get_option( 'gfb_staff_timezone_' . $staff_id ) != '' ) {
		$staff_timezone = get_option( 'gfb_staff_timezone_' . $staff_id );
	} else {
		$staff_timezone = wp_timezone_string();	
	}
	
	return $staff_timezone;
}




/**
 * Schedule Max Future Minutes Options
 */
function gfb_schedule_lead_time_minutes()
{
	$options =  array(
		'no'   => 'No lead time',
		'30'   => '30 minutes',
		'60'   => '1 hour',
		'120'  => '2 hours',
		'180'  => '3 hours',
		'240'  => '4 hours',
		'300'  => '5 hours',
		'360'  => '6 hours',
		'420'  => '7 hours',
		'480'  => '8 hours',
		'720'  => '12 hours',
		'1080' => '18 hours',
		'1440' => '24 hours',
		'2880' => '48 hours',
		'4320' => '3 days',
		'5760' => '4 days',
		'7200' => '5 days',
		'8640' => '6 days',
		'10080' => '7 days',
		'20160' => '14 days',
		'43200' => '30 days',
	);
	return $options;
}

/**
 * Schedule Max Future Days Options
 */
function gfb_schedule_max_future_days()
{
	$options =  array(
		'1'   => '1 day',
		'2'   => '2 days',
		'3'   => '3 days',
		'4'   => '4 days',
		'5'   => '5 days',
		'6'   => '6 days',
		'7'   => '7 days',
		'8'   => '8 days',
		'9'   => '9 days',
		'10'  => '10 days',
		'14'  => '2 weeks',
		'21'  => '3 weeks',
		'28'  => '4 weeks',
		'35'  => '5 weeks',
		'42'  => '6 weeks',
		'60'  => '2 months',
		'90'  => '3 months',
		'120' => '4 months',
		'150' => '5 months',
		'180' => '6 months',
		'210' => '7 months',
		'240' => '8 months',
		'270' => '9 months',
		'300' => '10 months',
		'330' => '11 months',
		'365' => '1 year',
		'730' => '2 years'
	);
	return $options;
}


/**
 * gfb_in_array_r multidimensional
 */
function gfb_in_array_r($needle, $haystack, $strict = false)
{
	foreach ($haystack as $item) {
		if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && gfb_in_array_r($needle, $item, $strict))) {
			return true;
		}
	}
	return false;
}

/*
* Notifications Dynamic Populate Conditional Logic Routing
*/
add_filter('gform_routing_field_types', 'gfb_add_gform_routing_field_types');
function gfb_add_gform_routing_field_types($field_types)
{
	$field_types = array_merge($field_types, array('gfb_appointment_services', 'gfb_appointment_providers'));
	return $field_types;
}

/*
* Show service category title on entry list with add_filter
*/
add_filter('gform_entries_field_value', 'gform_entries_list_service_category_title', 10, 4);
function gform_entries_list_service_category_title($value, $form_id, $field_id, $entry)
{

	$form         = GFAPI::get_form($form_id);
	$field        = RGFormsModel::get_field($form, $field_id);
	$value_fields = array('gfb_appointment_services_category',);

	if (is_object($field) && in_array($field->get_input_type(), $value_fields)) {
		//$value = $field->get_value_entry_detail( RGFormsModel::get_lead_field_value( $entry, $field ), '', true, 'text' );
		$post_id = absint($value);
		
		$categoryObj = new GravityFormBookingCategory();
		$categoryDetails = $categoryObj->gfbCategoryDetails($post_id);
		if(!empty($categoryDetails)){
			$value = $categoryDetails[0]['category_name']; 
			return esc_html( $value );
		}else{
			return esc_html( $value );
		}
	}

	return $value; //$value;

} 

/*
/*
* Show service title on entry list with add_filter
*/
add_filter('gform_entries_field_value', 'gfb_gform_entries_list_service_title', 10, 4);
function gfb_gform_entries_list_service_title($value, $form_id, $field_id, $entry)
{

	$form         = GFAPI::get_form($form_id);
	$field        = RGFormsModel::get_field($form, $field_id);
	$value_fields = array('gfb_appointment_services',);

	if (is_object($field) && in_array($field->get_input_type(), $value_fields)) {
		//$value = $field->get_value_entry_detail( RGFormsModel::get_lead_field_value( $entry, $field ), '', true, 'text' );
		$post_id = absint($value);
		global $gfbServiceObj;
		$serviceDetails = $gfbServiceObj->gfbServiceDetails($post_id);
		if(!empty($serviceDetails)){
			$serviceTitle= $serviceDetails[0]['service_title'];
			$value = $serviceTitle; 
			return esc_html( $value );
		}else{
			return esc_html( $value );
		}
		
	}

	return $value; //$value;

}

/*
* Show provider title on entry list with add_filter
*/
add_filter('gform_entries_field_value', 'gfb_gform_entries_list_provider_title', 10, 4);
function gfb_gform_entries_list_provider_title($value, $form_id, $field_id, $entry)
{

	$form         = GFAPI::get_form($form_id);
	$field        = RGFormsModel::get_field($form, $field_id);
	$value_fields = array('gfb_appointment_providers');

	//print_r( $value . '<br>');

	if (is_object($field) && in_array($field->get_input_type(), $value_fields)) {
		//$value = $field->get_value_entry_detail( RGFormsModel::get_lead_field_value( $entry, $field ), '', true, 'text' );
		$post_id = absint($value);
		global $gfbStaff;	
		$staffDetail = $gfbStaff->getStaffFullDetails($post_id);;	
		if(!empty($staffDetail)){
			$staffName = $staffDetail[0]['staff_name'];
			$value = $staffName;
			return esc_html($value);
		}else{
			return esc_html($value);
		}
		if ($value == 0) {
			return 'No preference';
		} 
	}

	return $value; //$value;

}
/*
* Dynamic Populate services Choices for Conditional Logic
*/
add_filter('gform_pre_render',       'gfb_populate_services_cat_for_conditional_logic');
add_filter('gform_pre_validation',   'gfb_populate_services_cat_for_conditional_logic');
add_filter('gform_pre_submission',   'gfb_populate_services_cat_for_conditional_logic');
add_filter('gform_admin_pre_render', 'gfb_populate_services_cat_for_conditional_logic');
function gfb_populate_services_cat_for_conditional_logic($form) {
	if (isset($form['fields'])) {
		foreach ($form['fields'] as &$field) {
			if ($field->type != 'gfb_appointment_services_category') {
				continue;
			}
			$categorylist = new GravityFormBookingCategory();
			$categories = $categorylist->gfbListCategories();
			$choices = array();
			// The Loop
			 if ($categories) {
				foreach ($categories as $resultData):
					
					$choices[] = array('value' => $resultData['category_id'], 'text' => $resultData['category_name']);
				endforeach;
				$field->choices = $choices;
			}
		}  
	}
	return $form;
}
/*
* Dynamic Populate services Choices for Conditional Logic
*/
add_filter('gform_pre_render',       'gfb_populate_services_for_conditional_logic');
add_filter('gform_pre_validation',   'gfb_populate_services_for_conditional_logic');
add_filter('gform_pre_submission',   'gfb_populate_services_for_conditional_logic');
add_filter('gform_admin_pre_render', 'gfb_populate_services_for_conditional_logic');
function gfb_populate_services_for_conditional_logic($form) {
	if (isset($form['fields'])) {
		foreach ($form['fields'] as &$field) {
			if ($field->type != 'gfb_appointment_services') {
				continue;
			}
			global $gfbServiceObj;
			$servicesList = $gfbServiceObj->gfbServiceNameList();
			$choices = array();
			// The Loop
			 if ($servicesList) {
				foreach ($servicesList as $resultData):
					
					$choices[] = array('value' => $resultData['service_id'], 'text' => $resultData['service_title']);
				endforeach;
				$field->choices = $choices;
			}
		}  
	}
	return $form;
}

/*
* Dynamic Populate providers Choices for Conditional Logic
*/
add_filter('gform_pre_render',       'gfb_populate_providers_for_conditional_logic');
add_filter('gform_pre_validation',   'gfb_populate_providers_for_conditional_logic');
add_filter('gform_pre_submission',   'gfb_populate_providers_for_conditional_logic');
add_filter('gform_admin_pre_render', 'gfb_populate_providers_for_conditional_logic');
function gfb_populate_providers_for_conditional_logic($form)
{
	//echo 'working!';
	//wp_die();
	if (isset($form['fields'])) {
		foreach ($form['fields'] as &$field) {
			if ($field->type != 'gfb_appointment_providers') {
				continue;
			}
			global $gfbStaff;
			/* Staff Id List */
			$staffList = $gfbStaff->getStaffNameList();
			$choices = array();
			// The Loop
			 if ($staffList) {
				foreach ($staffList as $resultData):
					
					$choices[] = array('value' => $resultData['staff_id'], 'text' => $resultData['staff_name']);
				endforeach;
				$field->choices = $choices;
			}
		}  
	}
	return $form;
}


/*
* GF Entry Calendar date
*/
 add_filter('gform_entries_field_value', 'gfb_gform_entries_list_calendar_date', 10, 4);
function gfb_gform_entries_list_calendar_date($value, $form_id, $field_id, $entry)
{
	$form         = GFAPI::get_form($form_id);
	$field        = RGFormsModel::get_field($form, $field_id);
	$value_fields = array('gfb_appointment_calendar');

	if (is_object($field) && in_array($field->get_input_type(), $value_fields)) {
		$dates = explode('&lt;br&gt', $value);

		if (count($dates) > 1) {
			return 'Multiple bookings';
		} elseif (count($dates) == 1) {
			return reset($dates);
		} else {
			return '';
		}
	}

	return $value; //$value;
} 


/**
 * Get GravityForms Entry IDS
 */
 function gfb_get_gravity_form_entries_ids()
{
	$forms = RGFormsModel::get_forms(null, 'title');
	$ids = array();

	if (isset($forms)) {
		foreach ($forms as $form) {
			$form_id = $form->id;

			if (GFAPI::get_entries($form_id)) {
				//if(RGFormsModel::get_leads($form_id) ) {
				foreach (GFAPI::get_entries($form_id) as $entry) {
					$ids[$entry['id']] = "{$entry['id']} - Form: {$form->title}";
				}
			}
		}
	}

	return $ids;
} 



/**
 * Provider Users Options
 */
function get_gfb_provider_users()
{
	$users = get_users();
	$users_array = array();

	if ($users) {
		// Array of WP_User objects.
		foreach ($users as $user) {
			$users_array[$user->ID] = $user->user_login;
		}
	}

	return $users_array;
}


/**
 * GF - Get Currency Symbol
 */
function gfb_get_currency_symbol()
{

	$gf_currency = gfb_current_currency();

	$symbol_left  = !empty($gf_currency['symbol_left']) ? $gf_currency['symbol_left'] . $gf_currency['symbol_padding'] : '';
	$symbol_right = !empty($gf_currency['symbol_right']) ? $gf_currency['symbol_padding'] . $gf_currency['symbol_right'] : '';

	$gf_currency_code = $symbol_left . $symbol_right;

	return $gf_currency_code;
}

/**
 * GF - Price to Money
 */
function gfb_to_money($money)
{

	$gf_currency = gfb_current_currency();

	$negative = '';

	if (strpos(strval($money), '-') !== false) {
		$negative = '-';
		$money   = floatval(substr($money, 1));
	}


	if ($money == '0') {
		$negative = '';
	}

	$symbol_left  = !empty($gf_currency['symbol_left']) ? $gf_currency['symbol_left'] . $gf_currency['symbol_padding'] : '';
	$symbol_right = !empty($gf_currency['symbol_right']) ? $gf_currency['symbol_padding'] . $gf_currency['symbol_right'] : '';

	return $negative . $symbol_left . $money . $symbol_right;
}

/**
 * GF - Current Currency Symbol
 */
function gfb_current_currency()
{

	if (!class_exists('RGCurrency')) {
		require_once(GFCommon::get_base_path() . '/currency.php');
	}

	return RGCurrency::get_currency(GFCommon::get_currency());
}


/**
 * User Profile Fields: Add Phone Field to Contact Info
 */
function gfb_user_phone_field($contactmethods)
{
	// Add Phone
	if (!array_key_exists('phone', $contactmethods)) {
		$contactmethods['phone'] = 'Phone';
	}

	return $contactmethods;
}
add_filter('user_contactmethods', 'gfb_user_phone_field', 10, 1);

/**
 * GFB - Set Export Values
 */
add_filter('gform_export_field_value', 'gfb_set_export_values', 10, 4);
function gfb_set_export_values($value, $form_id, $field_id, $entry)
{

	$formFields = GFAPI::get_form($form_id)['fields'];
	foreach ($formFields as $formField) {
		if ($formField->id == $field_id) {
			if ($formField->type) {
				$fieldType = $formField->type;
			}
		}
	}
	if ($fieldType) {
		switch ($fieldType) {
			
			case 'gfb_appointment_services_category':
				$categoryObj = new GravityFormBookingCategory();
				$categoryDetails = $categoryObj->gfbCategoryDetails($value);
				$value = $categoryDetails[0]['category_name'];
				break;
			case 'gfb_appointment_services':
				global $gfbServiceObj;
				$serviceDetails = $gfbServiceObj->gfbServiceDetails($value);
				$serviceTitle= $serviceDetails[0]['service_title'];
				$value = $serviceTitle;
				break;
			case 'gfb_appointment_providers':
				global $gfbStaff;	
				$staffDetail = $gfbStaff->getStaffFullDetails($value);	
				$staffName = $staffDetail[0]['staff_name'];
				$provider = $staffName;
				$value = $provider ? $provider : 'No preference';
				break;
		}
	}
	return $value;
}

/**
 * GFB - Get Field Type Value
 */
function gfb_new_get_field_type_value($form, $field_type)
{
	if (isset($form['fields'])) {
		$fieldCount = 0;
		foreach ($form['fields'] as $field) {
			if ($field['type'] === $field_type) {
				$fieldCount++;
			}
		}
		foreach ($form['fields'] as $field) {
			if ($field['type'] == $field_type) {
				if ($fieldCount > 1) {
					if (strpos($field['cssClass'], 'ga-field') !== false) {
						$id = $field['id'];
						$input = "input_{$id}";
						if (isset($_POST[$input])) {
							if (is_array($_POST[$input])) {
								return $_POST[$input];
							} else {
								return esc_html($_POST[$input]);
							}
						}
					}
				} else {
					return gfb_get_field_type_value($form, $field_type);
				}
			}
		}
	}

	return '';
}


add_filter('gform_pre_replace_merge_tags', function ($text, $form, $entry, $url_encode, $esc_html, $nl2br, $format) {

	foreach ($form['fields'] as $field) {
		if ($field['type'] == 'gfb_appointment_calendar') {
			$id = $field['id'];
			$label = $field['label'];
			$tag = '{' . $label . ':' . $id . '}';
			$key = "{$id}_date";
			if (isset($entry[$key])) {
				$text = str_replace($tag, $entry[$key], $text);
			}
			return $text;
		}
	}
	return $text;
}, 10, 7);

/**
 * GFB - After Submission 
 */
add_action('gform_after_submission', 'gfb_after_submission', 10, 2);
function gfb_after_submission($entry, $form)
{

	foreach ($form['fields'] as $field) {
		if ($field['type'] == 'gfb_appointment_calendar') {
			$id = $field['id'];
			$key = "{$id}_date";
			$entry[$id] = isset($entry[$key]) ? $entry[$key] : '';
		}
	}
}

/**
 * GFB - Add Customer 
 */
add_action( 'gform_user_registered', 'gfb_add_customer', 10, 4 );
function gfb_add_customer( $user_id, $feed, $entry, $user_pass ) {
	$user_details = get_user_by('ID',$user_id);
	$name = $user_details->user_login;
	$email = $user_details->user_email;
	$created_date = date('Y-m-d H:i:s');
	$ip = $_SERVER['REMOTE_ADDR']; 
	global $wpdb;
	$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";
	/* Check Email Already Exists In Customer Table*/
	$results = $wpdb->get_results( 'SELECT * FROM '.$gfb_customer_mst.' WHERE customer_email= "'.$email.'"',ARRAY_A );
	if(empty($results)){
	/* Insert Into Customer Table */
		$wpdb->insert($gfb_customer_mst, array('customer_name' => $name, 'customer_email' => $email,'created_date'=>$created_date,'ip_address'=>$ip) ); 
		$lastid = $wpdb->insert_id;
		/* Add Customer id into customer table */
		update_user_meta( $user_id, 'customer_id', $lastid);
	}else{
		$lastid = $results[0]['customer_id'];
		update_user_meta( $user_id, 'customer_id', $lastid);
	}
}	

	
/**
 * GFB - Check name or email fiels excet in the form 
 */
add_filter( 'gform_pre_render', 'gfb_check_fileds' );
function gfb_check_fileds( $form ) {
	$gfb_check_calendar_field = "";
	$gfb_check_email_field = "";	
	$gfb_check_name_field = "";
	$gfb_addon_name_field_id = "";
	$gfb_addon_email_field_id = "";
	$check_addon_name_field="";
	$check_addon_email_field="";	
	
	if (is_array(isset($form['fields'])) || is_object(isset($form['fields']))){
		foreach( $form['fields'] as $key=>$fields ) {
			if($fields['type'] == 'gfb_appointment_calendar'){
				$gfb_check_calendar_field = "CalendarFieldExists"; 
			}
		} 
	}
	if(!empty($form['gfb_booking_addon'])){
		foreach( $form['gfb_booking_addon'] as $key=>$fields_value ) {
			if($key == 'customer_name' && $fields_value != ""){
				$gfb_addon_name_field_id = $fields_value;
				$gfb_check_name_field = "NameFieldExistss"; 
			}
			if($key == 'customer_email' && $fields_value != ""){
				$gfb_addon_email_field_id = $fields_value;
				$gfb_check_email_field = "EmailFieldExists"; 
			}
		}
	}	
	if(!empty($gfb_addon_name_field_id) || !empty($gfb_addon_email_field_id)){
		foreach( $form['fields'] as $key=>$fields ) {
			if($fields['id'] == $gfb_addon_name_field_id){
				$check_addon_name_field = "NameFieldExists"; 
			}
			if($fields['id'] == $gfb_addon_email_field_id){
				$check_addon_email_field = "EmailFieldExists"; 
			}
		} 
	}
	
	if(!empty($gfb_check_calendar_field)){
		if(empty($gfb_check_email_field) || empty($gfb_check_name_field) || empty($check_addon_name_field) || empty($check_addon_email_field)){
			echo $msg = '<div class="popup-block-main"><div class="gfbAjaxLoader" id="gfb_loader_img"> <img class="gfb-ajax-loader" src="'. GFB_AJAX_LOADER .'" alt="" /> </div><div class="gfbUI">Kindly Set Email Or Name Fields For Gravity Forms > Forms > Setting > Gravity Forms Booking > User Form Settings</div></div>';
			die();
		}
	}
	
	return $form;
}

/**
 * GFB - Customer Appointment Page Redrict 
 */	
add_action( 'gform_after_submission', 'gfb_customer_appointment_page_redrict', 10, 2 );
function gfb_customer_appointment_page_redrict($entry, $form ) {
	$gfb_appointment_calendar = "not empty";
	if(!gfb_has_fields($form,'gfb_appointment_calendar')){
		$gfb_appointment_calendar = 'empty';
	}
	
	$emial_field_id = $form['gfb_booking_addon']['customer_email'];
	$email = $entry[$emial_field_id];				
	$user_email="";
	if(!empty($email)){
		$is_user_email = get_user_by('email', $email);
		if(!empty($is_user_email)){
			$user_email = 'not empty';	
		}else{
			$user_email = 'empty';	
		}
	}
	
	
	/* For Page Redirect*/
	if($gfb_appointment_calendar == 'not empty'){
		if($user_email == 'not empty'){
			$url = site_url().'/customer-appointments/';
			//wp_redirect($url);
			//exit;
		}
	}
}
add_filter( 'gform_confirmation', function ( $confirmation, $form, $entry ) {
	global $wp;
	$client_id = get_option('gfb_client_id');
	$redirect_url = home_url($wp->request);
	$state = 	base64_encode($entry['id']);
	$auth_url = 'https://accounts.google.com/o/oauth2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=offline&state='.$state.'&approval_prompt=force';

	if ( get_option( 'gfb_gc_2way_sync', 0 ) ) {
    	$confirmation .= '<a href="'.$auth_url.'" class="add-gcalendar">Add To My Calendar</a>';
    }

    return $confirmation;
    
}, 11, 3 );	

add_filter('gform_pre_render','redirect_custom');
function redirect_custom ($form) {
	if(isset($_GET['code']) && ! is_admin() && isset($_GET['state'])){
		global $wp;
		global $wpdb;
		global $gfbStaff;
		global $gfbServiceObj;
		global $gfbTimeSlotObj;
		require_once(GFB_ADMIN_DIR.'classes/gravityformbooking.google.calendar.api.php');
		try {
			$capi = new GFB_GoogleCalendarApi();
			$client_id = get_option('gfb_client_id');
			$client_secrete = get_option('gfb_client_secrete');
			$redirect_url = home_url($wp->request);
			$data = $capi->GetAccessToken($client_id, $redirect_url, $client_secrete, $_GET['code']);
			$data["createtime"] = time();
			$access_token = $data['access_token'];
			// Get user calendar timezone
			$user_timezone = $capi->GetUserCalendarTimezone($access_token);
			$calendar_id = 'primary';
			$entry_id= base64_decode($_GET['state']);	
			$entry = GFAPI::get_entry( $entry_id);
			$form_id = $entry['form_id'];
			$form = GFAPI::get_form( $form_id );
			

			// displays the types of every field in the form
			foreach ( $form['fields'] as $field ) {
				if($field->type == 'gfb_appointment_services'){
					$serviceField=$field->id;
				}
				if($field->type == 'gfb_appointment_calendar'){
					$calendarField=$field->id;
				}
			}
			$apt_date = substr($entry[$calendarField . '_date'], 0, strpos($entry[$calendarField . '_date'], "at"));
			$appointment_date = date('Y-m-d', strtotime($apt_date));

			$service_id = $entry[$serviceField];
			$slot_meeping_id = gform_get_meta( $entry_id, $calendarField.'_slot_meeping' );
			$service = $gfbServiceObj->gfbGetServiceTitle($service_id);
			
			$timeslot = $gfbTimeSlotObj->gfbGetTimeSlotValue($slot_meeping_id);
			
			$start_time = date('Y-m-d',strtotime($appointment_date)).'T'.$timeslot[0]['slot_start_time'];
			$end_time = date('Y-m-d',strtotime($appointment_date)).'T'.$timeslot[0]['slot_end_time'];
			
			$event_time = array(				
				'start_time' =>$start_time,
				'end_time'   =>$end_time
			);
			
		
			$servie_name = $service[0]['service_title'];		
			
			// Create event on primary calendar
			$event_id = $capi->CreateCalendarEvent( $calendar_id, $servie_name, $event_time, $user_timezone, $access_token);
			echo '<div class="success added-calendar">'.
			__('Your booking has been added to google calendar.').'</div>';
			wp_redirect(get_permalink());
            exit();
		}

		catch(Exception $e) {
			echo $e->getMessage();
		}
	}
	return $form;

}
/**
 * GFB - Has Fields 
 */	
function gfb_has_fields($form,$field_type){
	foreach( $form['fields'] as $fields ) {
		if($fields['type'] ==$field_type){
			return true;
		}
	}
	return false;
}	
	

/*--------------------Start Service Category--------------------*/
/**
* gfb_service_ctg_settings
**/
add_action( 'gform_field_standard_settings', 'gfb_service_ctg_settings', 10, 2 );
function gfb_service_ctg_settings( $position, $form_id) {
	$form_fields = GFFormsModel::get_form_meta($form_id)['fields'];

	foreach($form_fields as $key => $val){
		if($val['type'] == 'gfb_appointment_services_category'){
			$value = $val['defaultValue'];
		}
	}
	//create settings on position 25 (right after Field Label)
    if ( $position == 25 ) {
		
		$service_ctg_options = '';
		$service_ctg_options .= '<option value="">Select Category</option>';
		// The Query Select Services Categories
		$categorylist = new GravityFormBookingCategory();
		$categories = $categorylist->gfbListCategories();
		// The Loop
		if ($categories) {
			foreach ($categories as $resultData):
				$selected = @$value == $resultData['category_id'] ? ' selected="selected"' : '';
				$service_ctg_options .= '<option value="'.$resultData['category_id'].'"'.$selected.'>'. esc_attr($resultData['category_name'], 'gfb').'</option>';
			endforeach;
		}
		
	?>
        <li class="gfb_service_ctg field_setting">
            <label for="field_service_ctg_label" class="section_label">
                <?php esc_html_e( 'Set Default Value', 'gravityforms' ); ?>
                <?php gform_tooltip( 'form_field_service_ctg_value'); ?>
            </label>
            <select id="field_service_ctg_label" name="field_service_ctg" class="fieldwidth-3" onchange="SetInputServiceSetting(jQuery(this).val());" />
				<?= $service_ctg_options ?> 
			</select>
		</li>
		<?php
    }
}

/**
* gfb_editor_service_ctg_script
**/ 
add_action( 'gform_editor_js', 'gfb_editor_service_ctg_script' );
function gfb_editor_service_ctg_script(){
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "gfb_appointment_services_category"
        fieldSettings.gfb_appointment_services_category += ', .gfb_service_ctg';
		//binding to the load field settings event to initialize the checkbox
        jQuery(document).on('gform_load_field_settings', function(event, field, form){
            //jQuery('#field_service_ctg_label').attr('checked', field.encryptField == true);
			
        });
    </script>
    <?php
}

/**
* gfb_add_service_ctg_tooltips
**/  
add_filter( 'gform_tooltips', 'gfb_add_service_ctg_tooltips' );
function gfb_add_service_ctg_tooltips( $tooltips ) {
   $tooltips['form_field_service_ctg_value'] = "<h6>Service Category</h6>Use this if you want to set defalut value";
   return $tooltips;
}
/*--------------------End Service Category--------------------*/

/*--------------------Start Booking Service--------------------*/
/**
* gfb_services_settings
**/
add_action( 'gform_field_standard_settings', 'gfb_services_settings', 10, 2 );
function gfb_services_settings( $position, $form_id ) {
	//create settings on position 25 (right after Field Label)
    if ( $position == 25 ) {
		
		$gfb_services_options = '';
		$gfb_services_options .= '<option value="">Select Services</option>';
		// The Query Select Services
		/* Staff Id List */
		global $gfbServiceObj;
		$servicesList = $gfbServiceObj->gfbServiceNameList();
		// The Loop
		 if ($servicesList) {
			foreach ($servicesList as $resultData):
				
				$gfb_services_options .= '<option value="'.$resultData['service_id'].'">'.$resultData['service_title'].'</option>';
			endforeach;
		} 
		
	?>
        <li class="gfb_services field_setting">
            <label for="field_gfb_services_label" class="section_label">
                <?php esc_html_e( 'Set Default Value', 'gravityforms' ); ?>
                <?php gform_tooltip( 'form_field_gfb_services_value'); ?>
            </label>
            <select id="field_gfb_services_label" class="fieldwidth-3" onchange="SetInputServiceSetting(jQuery(this).val());" />
				<?= $gfb_services_options ?> 
			</select>
			<p class="fieldwidth-3" style="margin: 0px;"><?php esc_html_e( 'Make sure you are selecting the service that is linked with the default service category that you have selected', 'gravityforms' ); ?></p>
        </li>
        <?php
    }
}

/**
* gfb_editor_gfb_services_script
**/ 
add_action( 'gform_editor_js', 'gfb_editor_gfb_services_script' );
function gfb_editor_gfb_services_script(){
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "gfb_appointment_services"
        fieldSettings.gfb_appointment_services += ', .gfb_services';
		//binding to the load field settings event to initialize the checkbox
        jQuery(document).on('gform_load_field_settings', function(event, field, form){
            //jQuery('#field_gfb_services_label').attr('checked', field.encryptField == true);
		});
    </script>
    <?php
}

/**
* gfb_add_gfb_service_tooltips
**/  
add_filter( 'gform_tooltips', 'gfb_add_gfb_service_tooltips' );
function gfb_add_gfb_service_tooltips( $tooltips ) {
   $tooltips['form_field_gfb_services_value'] = "<h6>Booking Service</h6>Use this if you want to set defalut value";
   return $tooltips;
}
/*--------------------End Booking Service--------------------*/
/*--------------------Start Booking Provider--------------------*/
/**
* gfb_booking_provider_settings
**/
add_action( 'gform_field_standard_settings', 'gfb_booking_provider_settings', 10, 2 );
function gfb_booking_provider_settings( $position, $form_id ) {
	//create settings on position 25 (right after Field Label)
    if ( $position == 25 ) {
		
		$booking_provider_options = '';
		$booking_provider_options .= '<option value="">Select Provider</option>';
			// The Query Select staffList
			global $wpdb;
			global $gfbStaff;
			/* Staff Id List */
			$staffList = $gfbStaff->getStaffNameList();
			//The Loop
			if ($staffList) {
				foreach ($staffList as $resultData):
					$booking_provider_options .= '<option value="'.$resultData['staff_id'].'">'.$resultData['staff_name'].'</option>';
				endforeach;
			} 
		
	?>
        <li class="gfb_booking_pro field_setting">
            <label for="field_booking_provider_label" class="section_label">
                <?php esc_html_e( 'Set Default Value', 'gravityforms' ); ?>
                <?php gform_tooltip( 'form_field_booking_provider_value'); ?>
            </label>
            <select id="field_booking_provider_label" class="fieldwidth-3" onchange="SetInputfieldBookingBroviderSetting(jQuery(this).val());" />
				<?= $booking_provider_options ?> 
			</select>
			<p class="fieldwidth-3" style="margin: 0px;"><?php esc_html_e( 'Make sure you are selecting the Staff/Provider that is linked with the default service which you have selected.', 'gravityforms' ); ?></p>
        </li>
        <?php
    }
}

/**
* gfb_editor_booking_provider_script
**/ 
add_action( 'gform_editor_js', 'gfb_editor_booking_provider_script' );
function gfb_editor_booking_provider_script(){
    ?>
    <script type='text/javascript'>
        //adding setting to fields of type "gfb_appointment_services"
        fieldSettings.gfb_appointment_providers += ', .gfb_booking_pro';
		//binding to the load field settings event to initialize the checkbox
        jQuery(document).on('gform_load_field_settings', function(event, field, form){
            //jQuery('#field_booking_provider_label').attr('checked', field.encryptField == true);
		});
    </script>
    <?php
}

/**
* gfb_add_booking_provider_tooltips
**/  
add_filter( 'gform_tooltips', 'gfb_add_booking_provider_tooltips' );
function gfb_add_booking_provider_tooltips( $tooltips ) {
   $tooltips['form_field_booking_provider_value'] = "<h6>Booking Provider</h6>Use this if you want to set defalut value";
   return $tooltips;
}
/*--------------------End Booking Provider--------------------*/

/**
* Update Payment Status
**/ 
add_action( 'gform_post_payment_action', function ( $entry, $action ) {
   
	if($action['payment_status'] == 'Paid'){
		global $wpdb;
		$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
		$payments_array = array(
			'transaction_id' => $action['transaction_id'],
			'payment_status' => 1,		
		);
		$where_array = array('entry_id' =>$entry['id']);
		$update_data = $wpdb->update($gfb_payments_mst, $payments_array , $where_array );
	}
}, 10, 2 );


 