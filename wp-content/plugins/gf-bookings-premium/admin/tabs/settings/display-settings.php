<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

?>
<div class="tab-section">

    <div class="tab-title">
        <h4 class="gfb_section-title"><?php _e('Display Settings', 'gfb'); ?></h4>
    </div>
    
    <div class="tab-body">
    	<form name="display_setting_form" id="display_setting_form" action="#" method="post">
    		<div class="form-section">

    			<div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_gradient_color_top"><?php _e('Calendar Gradient: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_gradient_color_top" class="gfb_color_field" value="<?php echo get_option('gfb_cal_gradient_color_top', '#FFFFFF'); ?>" >
                        <input type="color" name="gfb_cal_gradient_color_bottom" class="gfb_color_field" value="<?php echo get_option('gfb_cal_gradient_color_bottom', '#FAFAFA'); ?>" >
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_text_color"><?php _e('Calendar Text Color: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_text_color" class="gfb_color_field" value="<?php echo get_option('gfb_cal_text_color', '#333333'); ?>" >                        
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_text_bg_color"><?php _e('Calendar Selected Text Color: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_text_bg_color" class="gfb_color_field" value="<?php echo get_option('gfb_cal_text_bg_color', '#00D0AC'); ?>" >                        
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_text_selected_color"><?php _e('Calendar Selected Text Background Color: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_text_selected_color" class="gfb_color_field" value="<?php echo get_option('gfb_cal_text_selected_color', '#FFFFFF'); ?>" >                      
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_available_day_bg_color"><?php _e('Calendar Available Days Background Color: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_available_day_bg_color" class="gfb_color_field" value="<?php echo get_option('gfb_cal_available_day_bg_color', '#00D0AC'); ?>" >                      
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="gfb_cal_available_day_text_color"><?php _e('Calendar Available Days Text Color: ', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">                          
                        <input type="color" name="gfb_cal_available_day_text_color" class="gfb_color_field" value="<?php echo get_option('gfb_cal_available_day_text_color', '#FFFFFF'); ?>" >                      
                    </div>
                
                </div>

                <div style="clear:both;">&nbsp;</div>

    			<div class="form-group-elements">                    
                    <div class="form-element">
                        <?php submit_button('Save Display Settings'); ?>
                    </div>
                </div>
    		</div>
    	</form>
    </div>
</div>