<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


?>
<div class="wrap">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Time Slots', 'gfb'); ?></h2>
		</div>        

		<div class="gfb_maintab-content">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>

			<div class="timeslot-table">
            
            	<?php require_once( GFB_ADMIN_TAB . 'timeSlots/viewTimeSlot.php' ); ?>

			</div>

		</div>

	</div>

</div><!-- /wrap -->