<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	/* Weekdays */
	$weekdays = array(
		'0' => 'Sunday',
		'1' => 'Monday',
		'2' => 'Tuesday',
		'3' => 'Wednesday',
		'4' => 'Thursday',
		'5' => 'Friday',
		'6' => 'Saturday'
	);

	global $gfbTimeSlotObj;
?>

<?php foreach( $weekdays as $dayno => $dayname ) { ?>

	<div class="timeslot-table-row">
    
        <div class="timeslot-weekday">
            <strong><?php echo esc_attr($dayname); ?></strong>
            <div class="timeslot-table-btns">
                <a href="#" class="btn-red gfb-clear-timeslots" data-weekday="<?php echo esc_attr($dayno); ?>" data-dayname="<?php echo esc_attr($dayname); ?>"><?php _e('Clear', 'gfb'); ?></a>
                
                <a href="#addTimeSlotModal" class="btn-green gfb-add-timeslot" data-weekday="<?php echo esc_attr($dayno); ?>"><?php _e('Add', 'gfb'); ?></a>
            </div>
        </div>
        
        <div id="addTimeSlotModal" class="popup-modal mfp-hide white-popup">
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
            
			<?php require_once( GFB_ADMIN_TAB . 'timeSlots/addTimeSlot.php' );  ?>
        </div>
        
        <div id="editTimeSlotModal" class="popup-modal mfp-hide white-popup">
        
        	<div class="gfbAjaxLoader" id="gfb_loader_img">
                <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
            </div>
        
            <div class="popup-block-main">
                <div class="popup-block-main-title"><?php _e('Edit Time Slot', 'gfb'); ?></div>    
                <div class="popup-block-main-body">    
                    <img class="gfb-ajax-loader" src="<?php echo GFB_AJAX_LOADER; ?>" alt="" />
                </div>                                
            </div>
            
        </div>
        
        <?php 
		$timeSlotList = $gfbTimeSlotObj->gfbListTimeSlot($dayno);
	
		if( empty( $timeSlotList ) ) { 
		?>
        	<div class="timeslot-box">
                <div class="timeslot-box-title" data-dayname="<?php echo esc_attr($dayname); ?>"><?php _e('No time slots.', 'gfb'); ?></div>                
            </div>
        <?php } else { ?>
        
        	<?php foreach($timeSlotList as $timeslot) { ?>
            
                <div class="timeslot-box search-fade">
                    <div class="timeslot-box-title"><?php echo esc_attr($timeslot['time_slot_name']); ?></div>
                    
                    <div class="timeslot-box-meta"><span class="dashicons dashicons-clock"></span> <?php echo $timeslot['slot_start_time']; ?> - <?php echo $timeslot['slot_end_time']; ?></div>
                    
                    <div class="timeslot-box-meta"><span class="dashicons dashicons-exerpt-view"></span> <?php echo esc_attr($timeslot['max_appointment_capacity']); ?><?php _e(' space available.', 'gfb'); ?></div>
                    
                    <div class="timeslot-table-btns">
                    
                        <a href="#editTimeSlotModal" class="btn-blue edit-slot" data-time_slot_id="<?php echo $timeslot['time_slot_id']; ?>" data-day="<?php echo $timeslot['weekday']; ?>"><?php _e('Edit', 'gfb'); ?></a>
                        
                        <a href="#" class="btn-red delete-slot" data-time_slot_id="<?php echo $timeslot['time_slot_id']; ?>"><?php _e('Delete', 'gfb'); ?></a>
                    </div>
                </div>
            
        	<?php } ?>
            	
        <?php } ?>
        
    </div>
    
<?php } 