<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
?> 
<div class="popup-block-main">

    <div class="popup-block-main-title"><?php _e('Add Time Slot', 'gfb'); ?></div>
    
    <div class="popup-block-main-body">
    
        <form name="time_slot_form" class="time-slot-form" id="time_slot_form" method="post"> 
        
            <div class="form-section">
            
            	<div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="time_slot_name"><?php _e('Title', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="time_slot_name" id="time_slot_name" class="input-main notallowspecial" value="" maxlength="100" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="slot_start_time"><?php _e('Start Time', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="slot_start_time" id="slot_start_time" class="input-main timespicker start_time_picker" autocomplete="off" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="slot_end_time"><?php _e('End Time', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="slot_end_time" id="slot_end_time" class="input-main timespicker end_time_picker" autocomplete="off" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="max_appointment_capacity"><?php _e('Max Appointment Capacity', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="max_appointment_capacity" id="max_appointment_capacity" class="input-main notallowspecialalpha" value="" maxlength="3" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                    	<?php submit_button('Save Time Slot'); ?>
                	</div>
                </div>
                
            </div>
            
        </form>
    
    </div>

</div> 