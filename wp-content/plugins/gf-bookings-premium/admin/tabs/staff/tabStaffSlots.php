<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



global $gfbStaff;

/* Weekdays */
$weekdays = array(
	'0' => 'sunday',
	'1' => 'monday',
	'2' => 'tuesday',
	'3' => 'wednesday',
	'4' => 'thursday',
	'5' => 'friday',
	'6' => 'saturday'
);

$days = [ 'Monday' => 'mon', 'Tuesday' => 'tue', 'Wednesday' => 'wed', 'Thursday' => 'thu', 'Friday' => 'fri', 'Saturday' => 'sat', 'Sunday' => 'sun' ];

global $gfbTimeSlotObj;

?> 
<h4 class="gfb_section-title"><?php _e("Timings Tab", "gfb"); ?></h4>

<div class="timeslot-table">
    <form name="add_staff_timezone" id="add_staff_timezone" method="post">
        <div class="gfb_field">
            <label class="gfb_field-label"><?php echo __( 'Timezone: ', 'gfb' ); ?></label>
            <div class="gfb_field-control" style="display:flex;">
                <select name="staff_timezone" id="staff_timezone">                
                    <?php echo wp_timezone_choice( get_option( 'gfb_staff_timezone_' . esc_attr( $staffDetail[0]['staff_id'] ) ) ); ?>
                </select>

                <input type="submit" name="submit" id="submit" class="button button-primary" style="margin-left: 10px;" value="Save Timezone">
            </div>
        </div>
    </form>

    <?php 
    if ( get_option( 'gfb_fullday_booking', 0 ) ) {
        // full day booking
        ?>
        <form name="gfb_add_staff_fullday_time_slot_new" id="gfb_add_staff_fullday_time_slot_new" method="post">
            <div class="global_fullday_slot_wrapper">
                <br><h4><?php _e('Enable Days For Booking', 'gfb'); ?></h4>
                <ul class="gfb_fullday_booking_wrapper">
                <?php 
                foreach ( $weekdays as $day_key => $day ) {

                    $gfb_fullday_booking = get_option( 'gfb_fullday_booking_slots', array() );
                    $staff_id = esc_attr( $staffDetail[0]['staff_id'] );
                    $enabled = 0;
                    $slot_cap = 1;
                    if ( is_array( $gfb_fullday_booking ) && isset( $gfb_fullday_booking['staff_' . $staff_id ] ) ) {
                        $staff_fullday_details = $gfb_fullday_booking['staff_' . $staff_id ];
                        if ( is_array( $staff_fullday_details ) && isset( $staff_fullday_details[$day] ) ) {
                            $current_day = $staff_fullday_details[$day];
                            $enabled  = isset( $current_day['enabled'] ) ? $current_day['enabled'] : 0;
                            $slot_cap = isset( $current_day['slot_cap'] ) ? $current_day['slot_cap'] : 1;
                        }
                    }                    

                    echo '<li>
                        <span>'. $day .'</span>
                        <label class="toggleswitch">
                          <input type="checkbox" value="1" name="'. $day .'[enabled]" class="gfb_fullday_booking" id="gfb_fullday_'. $day .'_booking" ' . checked($enabled, 1, false) . ' />
                        </label>
                        <div class="global_slot_capacity">
                            <label class="gfb_small_label">'. esc_html__( 'Slot Capacity', 'gfb' ) .'</label>
                            <input type="number" min="1" step="1" name="'. $day .'[slot_cap]" class="form-control floating-label" value="'. esc_attr( $slot_cap ) .'" />
                        </div>
                    </li>';
                }
                ?>
                </ul>
            </div>

            <a href="#" id="all_fullday_submit">SAVE ALL</a>
        </form>
        <?php
    } else {
        // time slot booking
        ?>
        <form name="gfb_add_staff_slot_time_new" id="gfb_add_staff_slot_time_new" method="post">
            <?php 
            $duration = get_option( 'gfb_time_duration' );
            $interval = get_option( 'gfb_time_interval' );
            ?>
            <input type="hidden" id="gfb_time_duration" value="<?php echo $duration; ?>">
            <input type="hidden" id="gfb_time_interval" value="<?php echo $interval; ?>">            
            <div class="global_time_slot_wrapper">
                <br><h4><?php _e('Add Time Slot', 'gfb'); ?></h4>
                <div id="gfb-timing-response"><!--error will add here when ajax request call--></div>
                <?php 
                foreach ( $weekdays as $day_key => $day ) {
                    $startTime = get_option( 'gfb_staff_'.esc_attr( $staffDetail[0]['staff_id'] ).'_'.$day.'_start_time' );
                    $endTime = get_option( 'gfb_staff_'.esc_attr( $staffDetail[0]['staff_id'] ).'_'.$day.'_end_time' );
                    $slot_cap = get_option( 'gfb_staff_'.esc_attr( $staffDetail[0]['staff_id'] ).'_'.$day.'_slot_cap' );
                    ?>
                    <div class="global_<?php echo $day ?>_slot global_slot_tracker">
                        <div class="time_frame">
                            <span style="width: 100px;margin-right: 5px; text-transform: Capitalize"><?php echo $day; ?></span>
                            <div class="start_time">
                                <label class="gfb_small_label"><?php echo esc_html__( 'Start time', 'gfb' ); ?></label>
                                <input type="text" name="global_<?php echo $day ?>_start" class="global_time_picker_<?php echo $day ?>_start form-control floating-label" placeholder="Start From" value="<?php echo $startTime; ?>">    
                            </div>
                            <!-- <span style="margin:0 5px">To</span> -->
                            <div class="end_time">
                                <label class="gfb_small_label"><?php echo esc_html__( 'End time', 'gfb' ); ?></label>
                                <input type="text" name="global_<?php echo $day ?>_end" class="global_time_picker_<?php echo $day ?>_end form-control floating-label" placeholder="End To" value="<?php echo $endTime; ?>" disabled>    
                            </div>
                        </div>

                        <div class="global_slot_capacity">
                            <label class="gfb_small_label"><?php echo esc_html__( 'Slot Capacity', 'gfb' ); ?></label>
                            <input type="number" min="1" step="1" name="global_slot_capacity_<?php echo $day; ?>" placeholder="Slot Capacity" class="global_slot_capacity_<?php echo $day; ?> form-control floating-label" value="<?php echo $slot_cap; ?>" />
                        </div>

                        <div class="save-time-wrapper">
                            <input type="submit" name="submit" value="Save <?php echo $day ?>" class="button button-primary save-slots save_slots_<?php echo $day ?>">
                        </div>

                        <?php 
                        $available_slots = get_option( 'gfb_staff_'.esc_attr( $staffDetail[0]['staff_id'] ).'_slots_for_'.$day );
                        ?>
                        <div class="global_breaktime_slot_wrapper <?php echo ( is_array( $available_slots['slots'] ) && count( $available_slots['slots'] ) > 0 ) ? 'show_break' : ''; ?> ">                           
                            <label class="gfb_small_label"><?php echo esc_html__( 'Add Breaks', 'gfb' ); ?></label>
                            <select name="break_for_<?php echo $day ?>[]" style="width:450px; padding: 12px;" id="break_for_<?php echo $day ?>" multiple="multiple">
                                <?php  
                                if ( is_array( $available_slots['slots'] ) && count( $available_slots['slots'] ) > 0 ) {
                                    foreach ( $available_slots['slots'] as $key => $slot ) {
                                        
                                        if ( isset( $available_slots['add_break'] ) && 'true' == $available_slots['add_break'][ $key ] ) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = '';
                                        }

                                        echo '<option value="'. $key .'" '. $selected .' >' . $slot . '</option>';
                                    }
                                }                                        
                                ?>
                            </select>
                        </div>
                    </div>
                    <?php 
                }
                ?>            

            </div>
            
            <a href="#" id="all_submit">Update Time Slots</a>
            <a href="#" id="all_clear" style="margin-right:15px">Clear All</a>

        </form>
        <?php
    }
    ?>
</div>