<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Staff_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'staff',     //singular name of the listed records
				'plural'    => 'staffs',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		function get_columns()
		{
			$columnNames = array(
				'cb' => '<input type="checkbox" />',
				'staff_name' => 'Staff Name',
				'staff_designation' => 'Designation',			
				'staff_phone' => 'Phone',
				'service_id' => 'Service',
				'edit' => 'Edit',
				'delete' => 'Delete'				
			);
			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{
			switch( $column_name ) 
			{ 				
				case 'staff_name':
				case 'staff_designation':
				case 'staff_phone':
					if( !empty($item[$column_name])){
						return $item[ $column_name ];
					}else{
						return '-';
					}
					
				case 'service_id':
					if( empty($item["service_id"])){
						return '<span class="gfb_badge badge-red">Service Not Assigned</span>';
					}
					else
					if( !empty($item["service_id"])){
						return '<span class="gfb_badge badge-blue">Service Assigned</span>';
					}
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function column_cb($item)
		{
			return sprintf( '<input type="checkbox" name="staff_id[]" value="%s">',$item['staff_id']);
		}
		
		function column_delete($item)
		{
			if( isset($_REQUEST['paged']) ) {
				$paged = $_REQUEST['paged'];	
			}
			else {
				$paged = 1;
			}
			
			
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$appointment_res = $wpdb->get_results( "SELECT appointment_id
			FROM ".$gfb_appointments_mst." 
			WHERE is_deleted=0 AND staff_id=".$item['staff_id']."", ARRAY_A);
			
			if(!empty($appointment_res)){
			
				$is_transfar_appoinemnts = get_user_meta($item['user_id'], 'transfar_staff_id' , true );
				if(empty($is_transfar_appoinemnts)){
					$actions = array(
					'delete' => sprintf('<a href="#" data-stfid="'.$item['staff_id'].'" class="gfb_grid-btn btn-red transfar_appointments_msg"><span class="dashicons dashicons-no"></span></a>', $item['staff_id']),
				);
				}else{		
					$actions = array(
						'delete' => sprintf('<a href="#" data-stfid="'.$item['staff_id'].'" class="gfb_grid-btn btn-red gfb_delete_staff"><span class="dashicons dashicons-no"></span></a>', $item['staff_id']),
					);
				}
			}else{
				$actions = array(
					'delete' => sprintf('<a href="#" data-stfid="'.$item['staff_id'].'" class="gfb_grid-btn btn-red gfb_delete_staff"><span class="dashicons dashicons-no"></span></a>', $item['staff_id']),
				);
			}
	
			return $this->row_actions($actions, true);
		}
		
		function column_edit($item)
		{
			$actions = array(
				'edit' => sprintf('<a data-stfid="'.base64_encode($item['staff_id']).'" href="#" name="edit_staff" id="edit_staff" class="gfb_grid-btn btn-blue edit-staff-modal"><span class="dashicons dashicons-edit"></span></a>', $item['staff_id']),
				
			);
			
			return sprintf('%1$s', $this->row_actions($actions, true) );
		}
		
		function get_bulk_actions() 
		{
			$actions = array(
				'delete' => 'Delete',
			);
			return $actions;
		}
		
		function process_bulk_action()
		{
			global $wpdb;
			$gfb_staff_mst = $wpdb->prefix . "gfb_staff_mst";

			if ('delete' === $this->current_action()) {
				$ids = isset($_REQUEST['staff_id']) ? $_REQUEST['staff_id'] : array();
				if (is_array($ids)) $ids = implode(',', $ids);
	
				if (!empty($ids)) {
					
					$deleteResult = $wpdb->query("UPDATE ".$gfb_staff_mst." SET is_deleted=1 WHERE staff_id IN(".$ids.")");
				
					$plugins_url = admin_url().'admin.php?page=gravity-form-booking-staffs&paged='.$_REQUEST['paged'];
					
					if($deleteResult)
					{
						echo "<script>jQuery(document).ready(function() { window.location.replace('".$plugins_url."'); });</script>";
					}
					unset($deleteResult);
				}
			}
			
		}
		
		function get_sortable_columns() {}
		
		function prepare_items() 
		{
			global $wpdb;
			$perPage = 7;
			$currentPage = $this->get_pagenum();

			$gfb_staff_mst = $wpdb->prefix . "gfb_staff_mst";
			$gfb_services_mst = $wpdb->prefix . "gfb_services_mst";
			$gfb_staff_service_mapping = $wpdb->prefix . 'gfb_staff_service_mapping';
			
			if( isset($_GET['category-filter']) && $_GET['category-filter'] != '' ) {
				
				$filter = ' AND cm.category_id="'.base64_decode($_GET['category-filter']).'"';
			}
			elseif( isset($_GET['staffnm-filter']) && $_GET['staffnm-filter'] <> '' ) {
				$filter = " AND sm.staff_title LIKE '%".$_GET['staffnm-filter']."%'";			
			}
			elseif( isset($_GET['price-filter']) && $_GET['price-filter'] > 0 ) {
				$filter = " AND sm.staff_price LIKE '%".$_GET['price-filter']."%'";			
			}
			else {				
				$filter = '';
			}
	
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->process_bulk_action();
			
			$staff_results = $wpdb->get_results('SELECT sm.staff_id, sm.user_id, sm.staff_name, sm.staff_designation, sm.staff_email, sm.staff_pic, sm.staff_phone, sm.staff_info, sm.staff_gcal_id,ssm.service_id FROM '.$gfb_staff_mst.' as sm left join '.$gfb_staff_service_mapping.' as ssm on sm.staff_id=ssm.staff_id and ssm.is_deleted=0 left join '.$gfb_services_mst.' as srm on ssm.service_id=srm.service_id and srm.is_deleted=0 WHERE sm.is_deleted=0 group by sm.staff_id ', ARRAY_A);
			
			$totalItems = count($staff_results);
			
			$staff_rows = array_slice($staff_results,(($currentPage-1)*$perPage),$perPage);
			
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage) // calculate pages count
			));
			
			$this->items = $staff_rows;
		}
	}