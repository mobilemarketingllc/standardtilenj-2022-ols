<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
$categoryObj = new GravityFormBookingCategory();
$categoryDetails = $categoryObj->gfbCategoryDetails($_POST['category_id']);
	
?>     
<form name="edit_category_form" class="category-form" id="edit_category_form" method="post"> 

    <div class="form-section">
    
    	<div class="form-group-elements">
    
            <div class="form-label">                
                <label class="label-main" for="category_name"><?php _e('Category', 'gfb'); ?></label>
            </div>
            
            <div class="form-element">
                <input type="text" name="category_name" id="category_name" class="input-main notallowspecial" value="<?php echo $categoryDetails[0]['category_name']; ?>" maxlength="100" />
            
                <input type="hidden" name="category_id" id="category_id" class="input-main" value="<?php echo $categoryDetails[0]['category_id']; ?>" readonly="readonly" />
            </div>
        
        </div>
        
        <div class="form-group-elements">
        	<div class="form-element">
                <?php wp_nonce_field('edit_category_nonce_field', 'edit_category_nonce', true, true); ?>
                <?php submit_button('Update Category'); ?>
            </div>
        </div>
        
    </div>
    
</form>