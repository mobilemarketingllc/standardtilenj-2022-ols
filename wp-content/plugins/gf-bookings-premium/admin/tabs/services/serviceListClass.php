<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Service_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'service',     //singular name of the listed records
				'plural'    => 'services',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		function get_columns()
		{
			$columnNames = array(
				'cb' => '<input type="checkbox" />',
				'category_name' => 'Category',
				'service_title' => 'Service',			
				'service_price' => 'Price',
				'service_color' => 'Color',
				'edit' => 'Edit',
				'delete' => 'Delete'				
			);
			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{
			switch( $column_name ) 
			{ 				
				case 'category_name':
				case 'service_title':
					return $item[ $column_name ];
				case 'service_price':
					return get_option('gfb_currency_symbol').$item[ $column_name ];
				case 'service_color':
					return '<span class="gfb_badge" style="background-color:#'.$item["service_color"].'"></span>';
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function column_cb($item)
		{
			return sprintf( '<input type="checkbox" name="service_id[]" value="%s">',$item['service_id']);
		}
		
		function column_delete($item)
		{
			if( isset($_REQUEST['paged']) ) {
				$paged = $_REQUEST['paged'];	
			}
			else {
				$paged = 1;
			}
			
			$actions = array(
				'delete' => sprintf('<a href="#" data-serid="%s" class="gfb_grid-btn btn-red gfb_delete_service"><span class="dashicons dashicons-no"></span></a>', $item['service_id']),
			);
	
			return $this->row_actions($actions, true);
		}
		
		function column_edit($item)
		{
			$actions = array(
				'edit' => sprintf('<a data-serid="%s" href="#editServiceHtml" name="edit_service" id="edit_service" class="gfb_grid-btn btn-blue edit-service-modal"><span class="dashicons dashicons-edit"></span></a>', $item['service_id']),
				
			);
			
			return sprintf('%1$s', $this->row_actions($actions, true) );
		}
		
		function get_bulk_actions() 
		{
			$actions = array(
				'delete' => 'Delete',
			);
			return $actions;
		}
		
		function process_bulk_action()
		{
			global $wpdb;
			$gfb_services_mst = $wpdb->prefix . "gfb_services_mst";

			if ('delete' === $this->current_action()) {
				$ids = isset($_REQUEST['service_id']) ? $_REQUEST['service_id'] : array();
				if (is_array($ids)) $ids = implode(',', $ids);
	
				if (!empty($ids)) {
					
					$deleteResult = $wpdb->query("UPDATE ".$gfb_services_mst." SET is_deleted=1 WHERE service_id IN(".$ids.")");
				
					$plugins_url = admin_url().'admin.php?page=gravity-form-booking-services&paged='.$_REQUEST['paged'];
					
					if($deleteResult)
					{
						echo "<script>jQuery(document).ready(function() { window.location.replace('".$plugins_url."'); });</script>";
					}
					unset($deleteResult);
				}
			}
			
		}
		
		function get_sortable_columns() {}
		
		function prepare_items() 
		{
			global $wpdb;
			$perPage = 7;
			$currentPage = $this->get_pagenum();

			$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
			$gfb_services_mst = $wpdb->prefix . "gfb_services_mst";
			
			if( isset($_GET['category-filter']) && $_GET['category-filter'] != '' ) {
				
				$filter = ' AND cm.category_id="'.base64_decode($_GET['category-filter']).'"';			
			}
			elseif( isset($_GET['servicenm-filter']) && $_GET['servicenm-filter'] <> '' ) {
				$filter = " AND sm.service_title LIKE '%".$_GET['servicenm-filter']."%'";			
			}
			elseif( isset($_GET['price-filter']) && $_GET['price-filter'] > 0 ) {
				$filter = " AND sm.service_price LIKE '%".$_GET['price-filter']."%'";			
			}
			else {				
				$filter = '';
			}
	
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->process_bulk_action();
			
			$service_results = $wpdb->get_results("SELECT sm.service_id, sm.service_title, TRIM(LEADING '$' FROM service_price) as service_price, sm.service_color, sm.category_id, cm.category_name FROM ".$gfb_services_mst." as sm LEFT JOIN ".$gfb_categories_mst." as cm ON sm.category_id = cm.category_id WHERE sm.is_deleted=0 ".$filter." ORDER BY sm.service_title asc", ARRAY_A);
		
			$totalItems = count($service_results);
			
			$service_rows = array_slice($service_results,(($currentPage-1)*$perPage),$perPage);
			
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage) // calculate pages count
			));
			
			$this->items = $service_rows;
		}
	}