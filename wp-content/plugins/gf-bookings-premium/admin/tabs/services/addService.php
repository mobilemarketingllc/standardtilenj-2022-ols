<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



$categorylist = new GravityFormBookingCategory();
$categories = $categorylist->gfbListCategories();

?> 
<div class="popup-block-main">

    <div class="popup-block-main-title"><?php _e('Add Service', 'gfb'); ?></div>
    
    <div class="popup-block-main-body">
    
        <form name="service_form" class="service-form" id="service_form" method="post">
        
            <div class="form-section">
            
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="category_id"><?php _e('Category', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <select name="category_id" id="category_id" class="input-main">                            	
                            <option value=""><?php _e("Select Category", "gfb"); ?></option>
                            <?php foreach( $categories as $category ) { ?>
                            
                                <option value="<?php echo $category['category_id']; ?>"><?php echo $category['category_name']; ?></option>
                                
                            <?php } ?>
                        </select>
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="service_title"><?php _e('Service Title', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="service_title" id="service_title" class="input-main notallowspecial" placeholder="Enter service name" maxlength="100" />
                    </div>
                
                </div>
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="service_color"><?php _e('Service Color', 'gfb'); ?></label>
                    </div>
                    
                    <div class="form-element">
                        <input type="text" name="service_color" id="service_color" class="input-main" value="#6d3bbf" />
                    </div>
                
                </div>                
                
                <div class="form-group-elements">
            
                    <div class="form-label">                
                        <label class="label-main" for="service_price"><?php _e('Service Price ('.get_option('gfb_currency_symbol').')', 'gfb'); ?><p class="description"><?php _e('( in '.get_option('gfb_currency_symbol').' )', 'gfb'); ?></p></label>
                    </div>
                    
                    <div class="form-element">                    	
                        <input type="text" placeholder="Price" name="service_price" id="service_price" class="input-main notallowspecialalpha" maxlength="7" />                        
                    </div>
                
                </div>
                
                <div class="form-group-elements">
                    <div class="form-element">
                        <?php wp_nonce_field('service_nonce_field', 'service_nonce', true, true); ?>
                        <?php submit_button('Save Service'); ?>
                    </div>
                </div>
                
            </div>
            
        </form>
    
    </div>

</div>