<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	global $gfbEmail;

?>
<div class="">

	<div class="gfb_wrap">
		
		<div class="gfb_maintab-title">
			<h2><?php _e('Email Notification', 'gfb'); ?></h2>
		</div>        

		<div class="gfb_maintab-content">
        	<h4 class="gfb_section-title"><?php _e('Codes used for email notification', 'gfb'); ?></h4>
            
            <table class="gfb_maintable_codes">
            	<tbody>
                	<tr>
						<td style="width:50%;">
                        	<table class="gfb_maintable_codes">
							<?php 
                            $codes = $gfbEmail->gfbCodes(); 
                            $index = 0;                                
                            foreach($codes as $code => $desc){
								
								if ($index > 0 and $index % 7 == 0) {
									echo '</table></td>';
									echo '<td style="width:50%;"><table class="gfb_maintable_codes">';
								}
								
								echo '<tr>';
                                    echo '<td><strong>'.$code.'</strong></td>';
                                    echo '<td>'.$desc.'</td>';
                                echo '</tr>';
                                $index++;
                            }
                            ?>
                            </table>
						</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <br />
        <div class="gfb_maintab-content">
        	<?php require_once( GFB_ADMIN_TAB . 'email/notification-form.php' ); ?>
        </div>
       
	</div>

</div><!-- /wrap -->