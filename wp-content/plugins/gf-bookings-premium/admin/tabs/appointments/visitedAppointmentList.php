<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
if( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Visited_Appointment_List_Tbl extends WP_List_Table {
	
	function __construct()
	{
		global $status, $page;
				
		//Set parent defaults
		parent::__construct( array(
			'singular'  => 'appointment',     //singular name of the listed records
			'plural'    => 'appointments',    //plural name of the listed records
			'ajax'      => false
		) );		
	}
	
	public function no_items() {
			_e( 'No appointments found.', 'gfb' );
	}
	
	function get_columns()
	{
		$columnNames = array(
			'customer_name' => __('Customer Name'),		
			'service_title' => __('Service'),
			'staff_name' => __('Staff Name'),				
			'appointment_date' => __('Appointment Date'),
			'time_slot' => __('Time'),				
			'status' => __('Status'),			
		);
		return $columnNames;	
	}	
	
	function column_default( $item, $column_name ) 
	{	
		$staff_timezone = '';

		if ( get_option( 'gfb_staff_timezone_' . $item['staff_slot_mapping_id'] ) != '' ) {
			$staff_timezone = get_option( 'gfb_staff_timezone_' . $item['staff_slot_mapping_id'] );
		} else {
			$staff_timezone = wp_timezone_string();
		}

		if ( $staff_timezone != '' ) {
			$staff_timezone = '<small style="font-weight:bold;">Timezone: ' . $staff_timezone . '</small>';
		}

		switch( $column_name ) 
		{ 
			case 'customer_name':
				return $item[ $column_name ];
			case 'appointment_date':
				return $item[ $column_name ];
			case 'time_slot':
				return $item[ $column_name ] . '<br>' . $staff_timezone;
			case 'service_title':
				return $item[ $column_name ];
			case 'staff_name':
				return $item[ $column_name ];
			case 'status':
				return '<span class="gfb_badge badge-green">Visited</span>';
			default:
				return print_r( $item, true ) ; 
		}
	}
	
	function get_bulk_actions() {}
	
	function get_sortable_columns() {}
	
	function process_bulk_action() {}
	
	function prepare_items() 
	{
		global $wpdb;
		global $current_user;
		$perPage = 10;
		$currentPage = $this->get_pagenum();
		$get_userid = get_current_user_id();
		
		$columns = $this->get_columns();
		$hidden = array();
		$sortable = $this->get_sortable_columns();
		$this->_column_headers = array($columns, $hidden, $sortable);
		$this->process_bulk_action();
		
		/* TABLES DEFINED */
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
		$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
		$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
		$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
		$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
		$users = $wpdb->prefix . 'users';
		
		/* QUERY */
		if( isset($_GET['customer-filter']) && $_GET['customer-filter'] != '' ) {
			
			$filter = ' AND cm.customer_id="'.base64_decode($_GET['customer-filter']).'"';			
		}
		elseif( isset($_GET['service-filter']) && $_GET['service-filter'] != '' ) {
			
			$filter = ' AND sm.service_id="'.base64_decode($_GET['service-filter']).'"';				
		}
		elseif( isset($_GET['staff-filter']) && $_GET['staff-filter'] != '' ) {
			
			$filter = ' AND sfm.staff_id="'.base64_decode($_GET['staff-filter']).'"';			
		}
		elseif( (isset($_GET['appointment-stdt-filter']) && $_GET['appointment-stdt-filter'] <> '') && (isset($_GET['appointment-enddt-filter']) && $_GET['appointment-enddt-filter'] <> '') ) {
				
			$filter = ' AND am.appointment_date BETWEEN "'.$_GET['appointment-stdt-filter'].'" AND "'.$_GET['appointment-enddt-filter'].'"';			
		}
		elseif( isset($_GET['status-filter']) && $_GET['status-filter'] != '' ) {
			
			$filter = ' AND am.status="'.base64_decode($_GET['status-filter']).'"';			
		}
		else {				
			$filter = '';
		}
		
		if ( $current_user->has_cap( 'gfb_staff_role' ) ) {
			
			$appointment_results = $wpdb->get_results('SELECT am.appointment_id, cm.customer_name,  DATE_FORMAT(am.appointment_date, "%d %M, %Y") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot , am.status
		FROM '.$gfb_appointments_mst.' AS am 
		INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
		INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
		INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id 
		INNER JOIN '.$users.' AS um ON sfm.user_id = um.ID AND um.ID='.$get_userid.'
		INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id
		INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
		WHERE am.is_deleted=0 and am.status=4 and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" '.$filter.' ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A);
						
		}
		elseif ( $current_user->has_cap( 'administrator' ) ) {
			
			$appointment_results = $wpdb->get_results('SELECT am.appointment_id, cm.customer_name,  DATE_FORMAT(am.appointment_date, "%d %M, %Y") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot , am.status
		FROM '.$gfb_appointments_mst.' AS am 
		INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id
		INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id 
		INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
		INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
		INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id 
		WHERE am.is_deleted=0 and am.status=4 and am.appointment_date="'.base64_decode($_REQUEST['appdate']).'" '.$filter.' ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A);
		
		}				
		
		/* COUNT APPOINTMENTS */
		$totalItems = count($appointment_results);
		
		$appointment_rows = array_slice($appointment_results,(($currentPage-1)*$perPage),$perPage);
		
		$this->set_pagination_args(array(
			'total_items' => $totalItems, // total items defined above
			'per_page' => $perPage, // per page constant defined at top of method
			'total_pages' => ceil($totalItems / $perPage), // calculate pages count				
		));
		
		$this->items = $appointment_rows;
	}

}