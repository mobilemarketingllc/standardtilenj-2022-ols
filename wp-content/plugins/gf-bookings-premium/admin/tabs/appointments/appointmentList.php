<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	if( ! class_exists( 'WP_List_Table' ) ) {
		require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	
	class Appointment_List_Tbl extends WP_List_Table {
		
		function __construct()
		{
			global $status, $page;
					
			//Set parent defaults
			parent::__construct( array(
				'singular'  => 'appointment',     //singular name of the listed records
				'plural'    => 'appointments',    //plural name of the listed records
				'ajax'      => false
			) );		
		}
		
		public function no_items() {
				_e( 'No appointments found.', 'gfb' );
		}
		
		public function get_columns() {

			if( is_user_logged_in() ) {
				$user = wp_get_current_user();
				$roles = ( array ) $user->roles;
			} else {
				$roles = array();
			}

			if ( 'admin' != get_option( 'gfb_appointment_policy' ) ) {

				$columnNames = array(
					'booking_ref_no' => __('Booking Number'),	
					'customer_name' => __('Customer Name'),
					'customer_email' => __('Customer Email'),	
					'service_title' => __('Service'),
					'staff_name' => __('Staff Name'),				
					'appointment_date' => __('Appointment Date'),
					'slot_count' => __('Slots'),
					'time_slot' => __('Time'),				
					'status' => __('Status'),
					'visit' => __('Visit'),
					'cancel' => __('Cancelled'),
					'conformed' => __('Confirm'),
					'delete' => __('Delete')			
				);				

			} else {

				if ( is_array( $roles ) && count( $roles ) > 0 && in_array( 'gfb_staff_role', $roles ) ) {
					$columnNames = array(
						'booking_ref_no' => __('Booking Number'),	
						'customer_name' => __('Customer Name'),
						'customer_email' => __('Customer Email'),	
						'service_title' => __('Service'),
						'staff_name' => __('Staff Name'),				
						'appointment_date' => __('Appointment Date'),
						'slot_count' => __('Slots'),
						'time_slot' => __('Time'),				
						'status' => __('Status'),
						'visit' => __('Visit'),		
					);
				} else {
					$columnNames = array(
						'booking_ref_no' => __('Booking Number'),	
						'customer_name' => __('Customer Name'),
						'customer_email' => __('Customer Email'),	
						'service_title' => __('Service'),
						'staff_name' => __('Staff Name'),				
						'appointment_date' => __('Appointment Date'),
						'slot_count' => __('Slots'),
						'time_slot' => __('Time'),				
						'status' => __('Status'),
						'visit' => __('Visit'),
						'cancel' => __('Cancelled'),
						'conformed' => __('Confirm'),
						'delete' => __('Delete')			
					);
				}				
			}			

			return $columnNames;	
		}	
		
		function column_default( $item, $column_name ) 
		{	
			$staff_timezone = '';

			if ( get_option( 'gfb_staff_timezone_' . $item['staff_id'] ) != '' ) {
				$staff_timezone = get_option( 'gfb_staff_timezone_' . $item['staff_id'] );
			} else {
				$staff_timezone = wp_timezone_string();
			}

			if ( $staff_timezone != '' ) {
				$staff_timezone = '<small style="font-weight:bold;">Timezone: ' . $staff_timezone . '</small>';
			}

			switch( $column_name ) 
			{ 	
				case 'booking_ref_no':
					return $item[ $column_name ];	
				case 'customer_name':
					return $item[ $column_name ];
				case 'customer_email':
					return $item[ $column_name ];
				case 'appointment_date':
					return $item[ $column_name ];
				case 'slot_count':
					return $item[ $column_name ];
				case 'time_slot':
					if ( '00:00 - 00:00' == $item[ $column_name ] ) {
						return __('Full Day', 'gfb') . '<br>' . $staff_timezone;
					} else {
						return $item[ $column_name ] . '<br>' . $staff_timezone;
					}					
				case 'service_title':
				case 'staff_name':
					return $item[ $column_name ];
				case 'status':
					if( $item["status"] == 1 ){
						return '<span class="gfb_badge badge-yellow">Pending</span>';
					}
					elseif( $item["status"] == 2 ){
						if($item["payment_type"]==3)
						{
							return '<span class="gfb_badge badge-blue">Awaiting</span>';
						}
						else
						{
							return '<span class="gfb_badge badge-blue">Awaiting</span>';
						}
						
					}
					elseif( $item["status"] == 3 ){
						return '<span class="gfb_badge badge-red">Cancelled</span>';
					}
					elseif( $item["status"] == 4 ){
						return '<span class="gfb_badge badge-green">Visited</span>';
					}
				default:
					return print_r( $item, true ) ; 
			}
		}
		
		function column_conformed($item)
		{			
			$actions = array(
				'conformed' => sprintf('<a data-appointment_id="%s" href="#" class="gfb_grid-btn btn-yellow appointment-conformed" style="color:#fff"><span class="dashicons dashicons-visibility"></span></a>', $item['appointment_id']));
			
			$user = wp_get_current_user();
			$allowed_roles = array( 'administrator' );
			if ( $item['status'] == '1'  || ( is_admin() && array_intersect( $allowed_roles, $user->roles ) ) ) {	
				return $this->row_actions($actions, true);
			}
			else {
				return sprintf('<span class="gfb_grid-btn not-allowed"><span class="dashicons dashicons-hidden"></span></span>');	
			}
		}
		function column_delete($item)
		{			
			$actions = array(
				'delete' => sprintf('<a data-appointment_id="%s" href="#" class="gfb_grid-btn btn-red gfb_delete_appointment"><span class="dashicons dashicons-trash"></span></a>', $item['appointment_id']));
			
			$user = wp_get_current_user();
			$allowed_roles = array( 'administrator' );
			if ( $item['status'] == '1'  || ( is_admin() && array_intersect( $allowed_roles, $user->roles ) ) ) {	
				return $this->row_actions($actions, true);
			}
			else {
				return sprintf('<span class="gfb_grid-btn not-allowed"><span class="dashicons dashicons-hidden"></span></span>');	
			}
		}
		
		function column_visit($item)
		{			
			$actions = array(
				'visit' => sprintf('<a href="#" data-appointment_id="%s"  class="gfb_grid-btn btn-green appointment-visit"><span class="dashicons dashicons-yes"></span></a>', $item['appointment_id']),
			);
			
			$user = wp_get_current_user();
			$allowed_roles = array( 'administrator' );
			if( $item['status'] == '2'  || ( is_admin() && array_intersect( $allowed_roles, $user->roles ) ) ) {	
				return $this->row_actions($actions, true);
			}
			else {
				return sprintf('<span class="gfb_grid-btn not-allowed"><span class="dashicons dashicons-hidden"></span></span>');
			}
		}
		
		function column_cancel($item)
		{			
			$actions = array(
				'cancel' => sprintf('<a href="#" data-appointment_id="%s" class="gfb_grid-btn btn-blue appointment-cancel"><span class="dashicons dashicons-no"></span></a>', $item['appointment_id']),
			);
			
			$user = wp_get_current_user();
			$allowed_roles = array( 'administrator' );
			if( $item['status'] == '2'  || ( is_admin() && array_intersect( $allowed_roles, $user->roles ) ) ) {	
				return $this->row_actions($actions, true);
			}
			else {
				return sprintf('<span class="gfb_grid-btn not-allowed"><span class="dashicons dashicons-hidden"></span></span>');
			}
		}
		
		function get_bulk_actions() {}
		
		function get_sortable_columns() {}
		
		function process_bulk_action()
		{
			global $wpdb;
			$gfb_appointments_mst = $wpdb->prefix . "gfb_appointments_mst";

			if ('delete' === $this->current_action()) {
				
				$ids = isset($_REQUEST['appointment_id']) ? $_REQUEST['appointment_id'] : array();
				
				if (is_array($ids)) {
					$ids = implode(',', $ids);
				}
	
				if (!empty($ids)) {

					$deleteResult = $wpdb->query("UPDATE ".$gfb_appointments_mst." SET is_deleted=1 WHERE appointment_id IN(".$ids.") AND status=2");
				
					$plugins_url = admin_url().'admin.php?page=gravity-form-booking-appointments';
					
					if($deleteResult)
					{
						echo "<script>jQuery(document).ready(function() { window.location.replace('".$plugins_url."'); });</script>";
					}
					unset($deleteResult);
				}
			}
		}
		
		function prepare_items() 
		{
			global $wpdb;
			global $current_user;
			//print_r($current_user);exit;
			$perPage = 10;
			$currentPage = $this->get_pagenum();
			$get_userid = get_current_user_id();
			
			$columns = $this->get_columns();
			$hidden = array();
			$sortable = $this->get_sortable_columns();
			$this->_column_headers = array($columns, $hidden, $sortable);
			$this->process_bulk_action();
			
			/* TABLES DEFINED */
			$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
			$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';
			$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
			$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
			$gfb_staff_schedule_mapping = $wpdb->prefix . 'gfb_staff_schedule_mapping';
			$gfb_time_slot_mst = $wpdb->prefix . 'gfb_time_slot_mst';	
			$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';	
			$users = $wpdb->prefix . 'users';	
			
			/* QUERY */
			if( isset($_GET['customer-filter']) && $_GET['customer-filter'] != '' ) {
				
				$filter = ' AND cm.customer_id="'.base64_decode($_GET['customer-filter']).'"';
			}
			elseif( isset($_GET['service-filter']) && $_GET['service-filter'] != '' ) {
				
				$filter = ' AND sm.service_id="'.base64_decode($_GET['service-filter']).'"';			
			}
			elseif( isset($_GET['staff-filter']) && $_GET['staff-filter'] != '' ) {
				
				$filter = ' AND sfm.staff_id="'.base64_decode($_GET['staff-filter']).'"';		
			}
			elseif( (isset($_GET['appointment-stdt-filter']) && $_GET['appointment-stdt-filter'] <> '') && (isset($_GET['appointment-enddt-filter']) && $_GET['appointment-enddt-filter'] <> '') ) {
				
				$filter = ' AND am.appointment_date BETWEEN "'.date('Y-m-d', strtotime($_GET['appointment-stdt-filter'])).'" AND "'.date('Y-m-d', strtotime($_GET['appointment-enddt-filter'])).'"';			
			}
			elseif( isset($_GET['status-filter']) && $_GET['status-filter'] != '' ) {
				
				$filter = ' AND am.status="'.base64_decode($_GET['status-filter']).'"';	
			}
			elseif( isset($_GET['appointment-bookingnum-filter']) && $_GET['appointment-bookingnum-filter'] != '' ) 			{
				$filter = ' AND am.booking_ref_no="'.$_GET['appointment-bookingnum-filter'].'"';		
			}
			else {				
				$filter = '';
			}
			
			if ( $current_user->has_cap( 'gfb_staff_role' ) ) {
				
				$appointment_results = $wpdb->get_results('SELECT sfm.staff_id,booking_ref_no,slot_count,pm.payment_type,am.appointment_id, cm.customer_name, cm.customer_email,  DATE_FORMAT(am.appointment_date, "%d %M, %Y") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, am.status 
			FROM '.$gfb_appointments_mst.' AS am 
			INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id and cm.is_deleted=0
			INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
			INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id and sfm.is_deleted=0
			INNER JOIN '.$users.' AS um ON sfm.user_id = um.ID AND um.ID='.$get_userid.'
			INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id
			LEFT JOIN '.$gfb_payments_mst.' as pm on pm.appointment_id=am.appointment_id
			INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
			WHERE am.is_deleted=0 '.$filter.' ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A);
				
			} else {
				
					$appointment_results = $wpdb->get_results('SELECT sfm.staff_id,booking_ref_no,slot_count,pm.payment_type,am.appointment_id, cm.customer_name, cm.customer_email,  DATE_FORMAT(am.appointment_date, "%d %M, %Y") as appointment_date, am.staff_slot_mapping_id, sm.service_title, sfm.staff_name, CONCAT( DATE_FORMAT(tm.slot_start_time, "%H:%i")," - ",DATE_FORMAT(tm.slot_end_time, "%H:%i") ) as time_slot, am.status 
				FROM '.$gfb_appointments_mst.' AS am 
				INNER JOIN '.$gfb_customer_mst.' AS cm ON am.customer_id = cm.customer_id 
				INNER JOIN '.$gfb_services_mst.' AS sm ON am.service_id = sm.service_id
				INNER JOIN '.$gfb_staff_mst.' AS sfm ON am.staff_id = sfm.staff_id
				INNER JOIN '.$gfb_staff_schedule_mapping.' AS ssm ON am.staff_slot_mapping_id = ssm.staff_slot_mapping_id 
				LEFT JOIN '.$gfb_payments_mst.' as pm on pm.appointment_id=am.appointment_id
				INNER JOIN '.$gfb_time_slot_mst.' AS tm ON ssm.time_slot_id = tm.time_slot_id
				WHERE am.is_deleted=0 '.$filter.' ORDER BY am.appointment_date desc, am.appointment_id desc', ARRAY_A);

				// echo 'test 2';
			
			}
			
			/* COUNT APPOINTMENTS */
			$totalItems = count($appointment_results);
			
			$appointment_rows = array_slice($appointment_results,(($currentPage-1)*$perPage),$perPage);
			
			$this->set_pagination_args(array(
				'total_items' => $totalItems, // total items defined above
				'per_page' => $perPage, // per page constant defined at top of method
				'total_pages' => ceil($totalItems / $perPage), // calculate pages count				
			));
			
			$this->items = $appointment_rows;
		}

	}