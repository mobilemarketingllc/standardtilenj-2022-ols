(function( $ ){
	
	var $document = $(document),
		$window = $(window);	
		
	$document.ready(function(){
		
	var $holiday_calendar = $('#gfb-js-holidays-cal'),
		$ajax_loader = $(".gfb-ajax-loader-holiday"),
		$ui_current_day = $(".ui-datepicker-current-day"),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".gfb_maintab-content");
		
		var newdate = new Date();
		var datepickeryear = new Date(new Date().getFullYear(), 0, 1);
		var eventDates = {};
		 
		/* Get holiday list from db */
		if( $holiday_calendar.length > 0 ) { 
		
			$gfb_loader_img.show();
		
			var datecontent = {
				action : 'gfb_display_holiday_list',
			};
			
			$.post( js_holiday_ajax.ajaxurl, datecontent, function(response) {					
					/* remove loader */
					$gfb_loader_img.hide();
					
					objArr=$.parseJSON(response);
					
					$.each( objArr, function( key, value ) {						
						eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" );
					});
					
					/* Holiday Calendar */
					
					$holiday_calendar.datepicker({
						dateFormat: 'yy-mm-dd',
						changeYear:true,
						yearRange:"-68:+100",
						numberOfMonths:[4,3],
						todayHighlight: true,
						onSelect: function (dateText, inst) {
															
							//Check wheather selected date exists or not in array */
							if( $.inArray( dateText, objArr ) !== -1 ){
								
								$gfb_maintab_content.addClass("loading-block");
								
								/* remove holiday */
								var datecontent = {
									action		 : 'gfb_remove_holiday',
									holiday_date : dateText,
								};
								
								$.post( js_holiday_ajax.ajaxurl, datecontent, function(response) {

										var obj=$.parseJSON(response);
					
										if (obj.msg != 'false') {	
										
											$gfb_maintab_content.removeClass("loading-block");	
											swal({ title:obj.text, type:"success" });

											
											$(".ui-datepicker-current-day").removeClass('event');	
											
											/* Change update holiday array */	
											var datecontent = {
												action : 'gfb_display_holiday_list',
											};
											
											$.post( js_holiday_ajax.ajaxurl, datecontent, function(response) {					
													
												objArr=$.parseJSON(response);
												eventDates = {};
												
												$.each( objArr, function( key, value ) {						
													eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" );
												});
												
											});
										}	
										else {
											swal({ title:obj.text, type:"error" });	
										}						
									}
								);	
							}
							else
							{
								$gfb_maintab_content.addClass("loading-block");
									
								/* add holiday */						 
								var datecontent = {
									action		 : 'gfb_add_holiday',
									holiday_date : dateText,
								};
								
								$.post( js_holiday_ajax.ajaxurl, datecontent, function(response) {

										var obj=$.parseJSON(response);
					
										if (obj.msg != 'false') {	
											
											$gfb_maintab_content.removeClass("loading-block");
											swal({ title:obj.text, type:"success" });

											$(".ui-datepicker-current-day").addClass('event');
											
											/* Change update holiday array */	
											var datecontent = {
												action : 'gfb_display_holiday_list',
											};
											
											$.post( js_holiday_ajax.ajaxurl, datecontent, function(response) {					
													
												objArr=$.parseJSON(response);
												eventDates = {};
																								
												$.each( objArr, function( key, value ) {						
													eventDates[ new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" ) ] = new Date( ""+$.datepicker.formatDate('mm/dd/yy', new Date( value ) )+"" );
												});
												
											});
										}	
										else {
											swal({ title:obj.text, type:"error" });
										}
									}
								);	
							
							}
							
						},
						beforeShowDay: function( date ) {	

							var highlight = eventDates[date];
							
							if( highlight ) {
								 return [true, "event", highlight];
							} else {
								 return [true, '', ''];
							} 
				
						},		
						
					}).datepicker('setDate', datepickeryear);						
													
				}
			);	
		}
		 
		
	});
	
}(jQuery));