(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	var $timespicker = $(".timespicker"),
		$time_slot_form = $("#time_slot_form"),
		$gfb_add_timeslot = $(".gfb-add-timeslot"),
		$delete_slot = $(".delete-slot"),
		$edit_slot = $(".edit-slot"),
		$clearSlot = $(".gfb-clear-timeslots"),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".gfb_maintab-content"),
		$gfb_allday_booking = $(".gfb_fullday_booking");

		$gfb_allday_booking.toggleSwitch();
		$gfb_allday_booking.show();
		
		/* Time picker */
		$timespicker.timepicker({ 
			'timeFormat': 'H:i',
			'step': js_time_slot_ajax.slotinterval,
			'disableTextInput':true,
			'scrollDefault': 'now',
		});
		
		/* Add / Remove class on link click */		
		$gfb_add_timeslot.on('click', function(){
			$('.current-day').removeClass('current-day');
			$(this).addClass('current-day');
		});
		
		/* Open Time Slot Popup */
		$gfb_add_timeslot.magnificPopup({
			type: "inline",
			preloader: true,
			focus: "#time_slot_name",
		});
		
		/* Add Time Slot Form */
		$time_slot_form.validate({
			rules: {
				time_slot_name: "required",
				slot_start_time: "required",
				slot_end_time: "required",
				max_appointment_capacity: {
					required:true,
					digits: true,
					minlength:1,
					maxlength:3
				},
			},
			messages: {
				time_slot_name: "Please enter time slot name",
				slot_start_time: "Please select starting time of slot",
				slot_end_time: "Please select ending time of slot",
				max_appointment_capacity: {
					required:"Enter maximum no. of appointment you wish to book in particular time slot",
					digits: "Please enter numbers only",
					minlength:"You have to enter atleast 1 digit",
					maxlength:"You can to enter atmost 3 digit",
				},
			},
			submitHandler: function(){
				
				$(".mfp-content").addClass("loading-block");
				
				var wday =  $(".current-day").data('weekday');
			
				var dataVal = {
					action	 : 'gfb_add_new_timeslot',
					formdata : $time_slot_form.serialize(),
					slotday  : wday
				};
								
				jQuery.post(
					js_time_slot_ajax.ajaxurl, dataVal, function(response) {

						$(".mfp-content").removeClass("loading-block");
						var obj=$.parseJSON(response);
							
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								window.location.replace(obj.url);
							});
						}	
						else {
							swal({ title:obj.text, type:"error" });		
						}									
					}
				);	
				
				return false;
			}
		});
		
		/* Delete Time Slot */
		$delete_slot.on('click', function(e){
			e.preventDefault();
			var $this = $(this);
			var slotid = $(this).data('time_slot_id');
			
			swal({
			  	title: "Are you sure you want to delete this time slot?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
				else
				{			
					var dataVal = {
						action 		 : 'gfb_delete_timeslot',
						time_slot_id : slotid,
					};
					
					jQuery.post(
						js_time_slot_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							$gfb_maintab_content.removeClass("loading-block");	
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									$this.closest(".search-fade").fadeOut(1000);
								});	
								
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
		
		});
		
		// Time Slot Form Magnific Popup
		$edit_slot.magnificPopup({
			type: 'inline',
			preloader: true,
			focus: '#time_slot_name',
			callbacks: {
				open: function(){
					var time_slot_id = this.st.el['0'].dataset.time_slot_id;
					
					var dataVal = {
						action		 : 'gfb_display_edit_timeslot',
						time_slot_id : time_slot_id,
					};
					
					jQuery.post(
						js_time_slot_ajax.ajaxurl, dataVal, function(response) {

							$('#editTimeSlotModal .popup-block-main-body').html( response );	
							/* Time picker */
							$(".timespicker").timepicker({ 
								'timeFormat': 'H:i',
								'step': js_time_slot_ajax.slotinterval,
								'disableTextInput':true,
								'scrollDefault': 'now',
							});	
							
							/* Assign edit form */
							$('#edit_time_slot_form').validate({
								rules: {
									time_slot_name: "required",
									slot_start_time: "required",
									slot_end_time: "required",
									max_appointment_capacity: {
										required:true,
										digits: true
									},
								},
								messages: {
									time_slot_name: "Please enter time slot name",
									slot_start_time: "Please select starting time of slot",
									slot_end_time: "Please select ending time of slot",
									max_appointment_capacity: {
										required:"Enter maximum no. of appointment you wish to book in particular time slot",
										digits: "Please enter numbers only"
									},
								}, 
								submitHandler: function() {
									
									$(".mfp-content").addClass("loading-block");
									
									var data = {
										action		  		: 'gfb_edit_timeslot',
										timeslot_form 		: $('#edit_time_slot_form').serialize(),
										edit_timeslot_nonce : $('#edit_timeslot_nonce').val(),
									};	
										
									jQuery.post(
										js_time_slot_ajax.ajaxurl, data, function(response) {

											var obj=$.parseJSON(response);
											$(".mfp-content").removeClass("loading-block");
											if (obj.msg != 'false') {	
												swal({ title:obj.text, type:"success" }, function(){
													window.location.replace(obj.url);
												});
											}	
											else {
												swal({ title:obj.text, type:"error" });	
											}							
										}
									);
									return false;
								}
							});
										
						}
					);	
				}
			}
		});	
		
		/* Clear time slot */		
		$clearSlot.on('click', function(e){
			e.preventDefault();
			var $this = $(this);
			var slotid = $(this).data('weekday');
			
			swal({
				title: "Are you sure you want to clear all time slots?",
				text: "You will not be able to recover this time slots!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Clear It!",
				cancelButtonText: "No, Cancel It!",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function (isConfirm) 
			{				
				$gfb_maintab_content.addClass("loading-block");
				
				if (isConfirm) {				
					
					var dataVal = {
						action 	: 'gfb_clear_slot',
						weekday : slotid,
					};
					
					jQuery.post(
						js_time_slot_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
							$gfb_maintab_content.removeClass("loading-block");	
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									window.location.replace(obj.url);
								});
							}
							else {
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);			
				}
				else {
					$gfb_maintab_content.removeClass("loading-block");
					return false;
				}
			});
	
		});
		
	});
}(jQuery));