(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
		/*VARIABLES DEFIEND */
	var $payment_setting_form = $("#payment_setting_form"),
		$company_setting_form = $("#company_setting_form"),
		$google_calendar_form = $('#google_calendar_form'),
		$customcss_setting_form = $('#customcss_setting_form'),
		$email_setting_form = $('#email_setting_form'),
		$appointment_setting_form = $('#appointment_setting_form'),
		$general_setting_form = $('#general_setting_form'),
		$display_setting_form = $('#display_setting_form'),
		$mainTabBtn = $('.main-tab > ul > li a'),
		$gfb_prior_days_book_appointment = $("#gfb_prior_days_book_appointment"),
		$gfb_prior_months_book_appointment = $("#gfb_prior_months_book_appointment"),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_payment_return_url = $("#gfb_payment_return_url"),
		$is_paypal = $("#is_paypal"),
		$is_stripe = $("#is_stripe"),
		$is_cod = $("#is_cod"),
		$gfb_enable_test_mode_stripe = $("#gfb_enable_test_mode_stripe"),
		$currency_setting_form = $("#currency_setting_form"),
		$all_tab = $(".all-tab-container");
		$gfb_gc_2way_sync = $("#gfb_gc_2way_sync"),
		$gfb_fullday_booking = $("#gfb_fullday_booking"),
		
		
		/* Toggle Button */
		$is_paypal.toggleSwitch();
		$is_stripe.toggleSwitch();
		$is_cod.toggleSwitch();
		$gfb_gc_2way_sync.toggleSwitch();		
		$gfb_gc_2way_sync.show();
		$gfb_fullday_booking.toggleSwitch();
		$gfb_fullday_booking.show();
		$is_paypal.on("click", function(){
			if( $(this).prev('.NubWrapper').hasClass('Checked')) {
				$("#gfb_enable_test_mode").prop("disabled", false);	
				$("#gfb_paypal_email").prop("disabled", false);	
				$("#gfb_currency_code").prop("disabled", false);
			}
			else {
				$("#gfb_enable_test_mode").prop("disabled", true);	
				$("#gfb_paypal_email").prop("disabled", true);	
				$("#gfb_currency_code").prop("disabled", true);
			}
		});
		
		$is_stripe.on("click", function(){
			if( $(this).prev('.NubWrapper').hasClass('Checked')) {
				$("#gfb_enable_test_mode_stripe").prop("disabled", false);	
				$("#gfb_stripe_test_secret_key").prop("disabled", false);	
				$("#gfb_stripe_test_publishable_key").prop("disabled", false);
				$("#gfb_stripe_live_secret_key").prop("disabled", false);	
				$("#gfb_stripe_live_publishable_key").prop("disabled", false);	
			}
			else {
				$("#gfb_enable_test_mode_stripe").prop("disabled", true);	
				$("#gfb_stripe_test_secret_key").prop("disabled", true);	
				$("#gfb_stripe_test_publishable_key").prop("disabled", true);
				$("#gfb_stripe_live_secret_key").prop("disabled", true);	
				$("#gfb_stripe_live_publishable_key").prop("disabled", true);
			}
		});
		
		/* CHECK WHICH MODE IS ENABLE IN STRIPE */
		if( $gfb_enable_test_mode_stripe.val() == 'test' ) {
			$("#gfb_stripe_test_secret_key").prop("disabled", false);	
			$("#gfb_stripe_test_publishable_key").prop("disabled", false);
			$("#gfb_stripe_live_secret_key").prop("disabled", true);	
			$("#gfb_stripe_live_publishable_key").prop("disabled", true);	
		}
		else if( $gfb_enable_test_mode_stripe.val() == 'live' ) {
			$("#gfb_stripe_test_secret_key").prop("disabled", true);	
			$("#gfb_stripe_test_publishable_key").prop("disabled", true);
			$("#gfb_stripe_live_secret_key").prop("disabled", false);	
			$("#gfb_stripe_live_publishable_key").prop("disabled", false);		
		}
		
		/* CHECK WHICH MODE IN ENABLE IN STRIPE USING ONCHANGE EVENT */
		$gfb_enable_test_mode_stripe.on("change", function(){
			var mode = $(this).val();
			if( mode == 'test' ) {
				$("#gfb_stripe_test_secret_key").prop("disabled", false);	
				$("#gfb_stripe_test_publishable_key").prop("disabled", false);
				$("#gfb_stripe_live_secret_key").prop("disabled", true);	
				$("#gfb_stripe_live_publishable_key").prop("disabled", true);	
			}
			else if( mode == 'live' ) {
				$("#gfb_stripe_test_secret_key").prop("disabled", true);	
				$("#gfb_stripe_test_publishable_key").prop("disabled", true);
				$("#gfb_stripe_live_secret_key").prop("disabled", false);	
				$("#gfb_stripe_live_publishable_key").prop("disabled", false);		
			}
				
		});
		
		
		/* Payment Settings */
		$payment_setting_form.on('submit', function(e) {
			
			$all_tab.addClass("loading-block");	
			
			if( $is_paypal.prev('.NubWrapper').hasClass('Checked')) {				
				$all_tab.removeClass("loading-block"); 
				
				if( $("#gfb_paypal_email").val() == '' ){
					swal({ title:"Please enter your paypal email address", type:"error" });	
					return false;
				}
			}
			
			
			if($gfb_payment_return_url.val() == '') {
				$all_tab.removeClass("loading-block"); 
				swal({ title:"Please add Return URL for Payment", type:"error" });	
			}
			else {
					
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $payment_setting_form.serialize(),
					pyaction	   : 'paymentsettings',
				};	
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						$all_tab.removeClass("loading-block"); 
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});		
						}							
					}
				);
			}
				
            e.preventDefault();
        });
		
		/* Google Calendar Settings */
		$google_calendar_form.on('submit', function(e) {	
		
			$all_tab.addClass("loading-block");			
			
			var data = {
				action		   : 'gfb_update_settings_option',
				setting_option : $google_calendar_form.serialize(),
			};	
				
			jQuery.post(
				js_settings_ajax.ajaxurl, data, function(response) {
					$all_tab.removeClass("loading-block");
					var obj=$.parseJSON(response);
					
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" }, function(){
							$all_tab.removeClass("loading-block");
						});
					}	
					else {
						swal({ title:obj.text, type:"error" }, function(){
							$all_tab.removeClass("loading-block"); 
						});	
					}						
				}
			);			
				
            e.preventDefault();
        });
		
		/* Company Settings */
		$company_setting_form.validate({
			rules: {
				gfb_company_email: {
					email: true
				},
			},
			messages: {
				gfb_company_email: {
					email: "Please enter valid email address"
				},
			},  
			submitHandler: function() {
				
				$all_tab.addClass("loading-block");	
				
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $company_setting_form.serialize(),
				};	
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block");	
							});
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}						
					}
				);	
				
				return false;
			}
		});
		
		/* Custom CSS Settings */
		$customcss_setting_form.validate({
			submitHandler: function() {
				
				$all_tab.addClass("loading-block");	
				
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $customcss_setting_form.serialize(),
				};	
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block");	
							});
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}						
					}
				);	
				
				return false;
			}
		});
		
		/* Email Settings */
		$email_setting_form.validate({
			rules: {
				gfb_sender_email: {
					email: true
				},
			},
			messages: {
				gfb_sender_email: {
					email: "Please enter valid email address"
				},
			},  
			submitHandler: function() {	
			
				$all_tab.addClass("loading-block");			
			
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $email_setting_form.serialize(),
				};	
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block");
							});
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}						
					}
				);			
					
				return false;
			}
        });


        /* General Form Settings */
		$general_setting_form.validate({
			submitHandler: function() {		
			
				$all_tab.addClass("loading-block");	
		
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $general_setting_form.serialize(),
				};
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block");
								location.reload();
							});	
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}						
					}
				);
					
				return false;
			}
        });

        /* Display Form Settings */
		$display_setting_form.validate({
			submitHandler: function() {		
			
				$all_tab.addClass("loading-block");	
		
				var data = {
					action		   : 'gfb_update_settings_option',
					setting_option : $display_setting_form.serialize(),
				};
					
				jQuery.post(
					js_settings_ajax.ajaxurl, data, function(response) {
						
						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){
								$all_tab.removeClass("loading-block");
								location.reload();
							});	
						}	
						else {
							swal({ title:obj.text, type:"error" }, function(){
								$all_tab.removeClass("loading-block"); 
							});	
						}						
					}
				);
					
				return false;
			}
        });
		
		/* Appointment Form Settings */
		$appointment_setting_form.validate({			
			rules: {
				gfb_prior_days_book_appointment: {
					digits: true
				},
				gfb_prior_months_book_appointment: {
					digits: true
				},
			},
			messages: {
				gfb_prior_days_book_appointment: {
					digits: "Please enter numbers only"
				},
				gfb_prior_months_book_appointment: {
					digits: "Please enter numbers only"
				},
			},  
			submitHandler: function() {		
			
				$all_tab.addClass("loading-block");	
		
				if( $gfb_prior_months_book_appointment.val() == 0 ) {
					swal({ title:"'Prior Months to Book Appointment' option cannot be 0.", type:"error" }, function(){
						$gfb_prior_months_book_appointment.val(1);
						$all_tab.removeClass("loading-block");
					});		
				}
				else {		
				
					var data = {
						action		   : 'gfb_update_settings_option',
						setting_option : $appointment_setting_form.serialize(),
					};	
						
					jQuery.post(
						js_settings_ajax.ajaxurl, data, function(response) {
							
							var obj=$.parseJSON(response);
							
							if (obj.msg != 'false') {	
								swal({ title:obj.text, type:"success" }, function(){
									$all_tab.removeClass("loading-block");
									location.reload();
								});	
							}	
							else {
								swal({ title:obj.text, type:"error" }, function(){
									$all_tab.removeClass("loading-block"); 
								});	
							}						
						}
					);	
				}
					
				return false;
			}
        });
		
		$mainTabBtn.on('click', function(e){
			var tabID = $(this).attr('href');
			
			$('.active-tab').removeClass('active-tab');
			
			$('.tab-container'+tabID).addClass('active-tab');
			$(this).parents('li').addClass('active-tab');
			
			e.preventDefault();
		});
		
		//Currency Code
		$('#gfb_currency li').on('click', function(e){
			var chosenCurrency = $(this).text().split(' - ');
			var currencyCode = chosenCurrency[0];
			var currencySYmbol = chosenCurrency[2];
			$('#gfb_currency_code').val(currencyCode);
			$('#gfb_currency_symbol').val(currencySYmbol);
		});
		
		//Filter Currency
		$(function(){
  
		  $('#filter').on('focus', function(){
			$(this).select();
			$('#gfb_currency, #gfb_currency li').show(300);
		  });
		  
		  $('#filter').on('blur', function(){
			$('#gfb_currency').hide(300);
		  });
		  
		  $('#filter').on('keyup', function(e){
			var filterText = $(this).val().toLowerCase();
			$('#gfb_currency li').each(function(){
			  var $this = $(this);
			  var $thisText = $this.text().toLowerCase();
			  if( $thisText.indexOf(filterText) == -1 ){
				$this.hide();
			  } else {
				$this.show();
			  }
			});
		  });
		  
		  $('#gfb_currency li').on('click', function(){
			$('#filter').val( $(this).text() );
		  });
		  
		});
		
		//Currency save
		$currency_setting_form.on('submit', function(e) {	
			
			$all_tab.addClass("loading-block");			
			
			var data = {
				action		   : 'gfb_update_settings_option',
				setting_option : $currency_setting_form.serialize(),
			};	
				
			jQuery.post(
				js_settings_ajax.ajaxurl, data, function(response) {
					
					var obj=$.parseJSON(response);
					
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" }, function(){
							$all_tab.removeClass("loading-block");
						});
					}	
					else {
						swal({ title:obj.text, type:"error" }, function(){
							$all_tab.removeClass("loading-block"); 
						});	
					}						
				}
			);			
				
            e.preventDefault();
        });
		
	});
}(jQuery));