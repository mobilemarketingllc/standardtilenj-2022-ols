(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
		
	var $service_color = $('#service_color'),
		$addServiceModal = $(".add-service-modal"),
		$editServiceModal = $(".edit-service-modal"),
		$service_form = $('#service_form'),
		$category_filter = $('.gfb-filter-category'),
		$gfb_service_price_submit = $('#gfb_service_price_submit'),
		$gfb_price_filter = $('#gfb_price_filter'),
		$gfb_service_nm_submit = $('#gfb_service_nm_submit'),
		$gfb_service_nm_filter = $('#gfb_service_nm_filter'),
		$gfb_delete_service = $('.gfb_delete_service'),
		$gfb_loader_img = $("#gfb_loader_img"),
		$gfb_maintab_content = $(".gfb_maintab-content");		
		
		/* Add class to tr */
		$("td.service_title").parents("tr").addClass("delete-service-fade");
		
		/* Add Color picker for service (add form) */
		$service_color.wpColorPicker();		
		
		// Add Service Form Magnific Popup
		$addServiceModal.magnificPopup({
			type: "inline",
			preloader: true,
			focus: "#category_id",
			fixedContentPos: false
		});
		
		$service_form.validate({
			rules: {
				category_id: "required",
				service_title: "required",
				service_price: {
					required:true,
					number: true
				},
			},
			messages: {
				category_id: "Please select service category",
				service_title: "Please enter service title",
				service_price: {
					required:"Please enter service price",
					number: "Please enter numbers only"
				},
			},  
			submitHandler: function() {
				
				$(".mfp-content").addClass("loading-block");
				
				var data = {
					action		 : 'gfb_add_service',
					service_form : $service_form.serialize(),
				};	
					
				jQuery.post(
					js_services_ajax.ajaxurl, data, function(response) {
						
						$(".mfp-content").removeClass("loading-block");	

						var obj=$.parseJSON(response);
						
						if (obj.msg != 'false') {	
							swal({ title:obj.text, type:"success" }, function(){								
								window.location.replace(obj.url);
							});
						}	
						else {
							swal({ title:obj.text, type:"error" });		
						}
												
					}
				);
				return false;
			}
		});
		
		// Edit Service Form Magnific Popup
		$editServiceModal.magnificPopup({
			type: 'inline',
			focus: '#category_id',
			fixedContentPos: false,
			callbacks: {
				
				
				open: function(){
					var serid = this.st.el['0'].dataset.serid;
					
					var dataVal = {
						action	   : 'gfb_display_edit_service',
						service_id : serid,
					};
					
					jQuery.post(
						js_services_ajax.ajaxurl, dataVal, function(response) {
							$('#editServiceHtml .popup-block-main-body').html( response );	
							
							/* Add Color picker service (edit form) */
							$('#edit_service_color').wpColorPicker();
							
							/* add edit form */
							$('#edit_service_form').validate({
								rules: {
									category_id: "required",
									service_title: "required",
									service_price: {
										required:true,
										number: true
									},
								},
								messages: {
									category_id: "Please select service category",
									service_title: "Please enter service title",
									service_price: {
										required:"Please enter service price",
										number: "Please enter numbers only"
									},
								}, 
								submitHandler: function() {
									
									$(".mfp-content").addClass("loading-block"); 
									
									var data = {
										action		 : 'gfb_update_service',
										service_form : $('#edit_service_form').serialize(),
									};	
										
									jQuery.post(
										js_services_ajax.ajaxurl, data, function(response) {
											
											$(".mfp-content").removeClass("loading-block");	

											var obj=$.parseJSON(response);
											
											if (obj.msg != 'false') {	
												swal({ title:obj.text, type:"success" }, function(){
													window.location.replace(obj.url);
												});
											}	
											else {
												swal({ title:obj.text, type:"error" });	
											}							
										}
									);
									
									return false;
								}	
							});
						}
					);	
				}
			}
		});	
			
		/* Category Filter jQuery */
		$category_filter.on('change', function(){
			var categFilter = $(this).val();
			if( categFilter != '' ){
				if( categFilter == '*' ){
					document.location.href = 'admin.php?page=gravity-form-booking-services';     
				}
				else {
					document.location.href = 'admin.php?page=gravity-form-booking-services&category-filter="'+categFilter+'"';    
				}
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-services';    
			}
		});
		
		/* Service filter */
		$gfb_service_nm_submit.on("click", function(e){
			
			var servicenmFilter = $gfb_service_nm_filter.val();
						
			if( servicenmFilter != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-services&servicenm-filter='+servicenmFilter;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-services';    
			}
			
			e.preventDefault();
		});
		
		/* Price filter */
		$gfb_service_price_submit.on("click", function(e){
			
			var priceFilter = $gfb_price_filter.val();
			
			if( priceFilter != '' ){
				document.location.href = 'admin.php?page=gravity-form-booking-services&price-filter='+priceFilter;    
			}
			else {
				document.location.href = 'admin.php?page=gravity-form-booking-services';    
			}
			
			e.preventDefault();
		});
		
		/* Delete Service */
		$gfb_delete_service.on("click", function(e){
			e.preventDefault();
			var $this = $(this);
			var serid = $(this).data('serid');	
			
			swal({
			  	title: "Are you sure you want to delete this service?",
				text: "",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: '#DD6B55',
				confirmButtonText: 'Yes',
				cancelButtonText: "No",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(confirmDel)
			{
				$gfb_maintab_content.addClass("loading-block");
				
				if( !confirmDel )
				{
					$gfb_maintab_content.removeClass("loading-block");
					return false;	
				}
				else
				{			
					var dataVal = {
						action : 'gfb_delete_service',
						serid  : serid,
					};
					
					jQuery.post(
						js_services_ajax.ajaxurl, dataVal, function(response) {

							var obj=$.parseJSON(response);
								
							if (obj.msg != 'false') {									
								swal({ title:obj.text, type:"success" }, function(){
									$gfb_maintab_content.removeClass("loading-block");
									//$this.closest(".delete-service-fade").fadeOut(1000);
									window.location.reload();
								});									
							}
							else {
								$gfb_maintab_content.removeClass("loading-block");
								swal({ title:obj.text, type:"error" });	
							}							
						}
					);				
				}
				
			});	
		});
	});
}(jQuery));