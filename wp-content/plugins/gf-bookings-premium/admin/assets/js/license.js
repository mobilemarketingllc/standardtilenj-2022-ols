(function( $ ){
	
	var $document = $(document),
		$window = $(window);
	
	$document.ready(function(){
	
	var $license_form = $('#license_form'),
		$gfb_maintab_content = $(".gfb_maintab-content");
	
		$license_form.validate({
			rules: {
				purchase_key: "required",
				
			},
			messages: {
				purchase_key: "Please Insert Purchase Code",
				
			}, 			
			submitHandler: function() {
				
			$gfb_maintab_content.addClass("loading-block");
			
			var dataString = {
				action		  : "gfb_get_purchase_code",
				purchase_code : $("#purchase_key").val()
			};	
			
			$.post(
				js_license_ajax.ajaxurl,
				dataString,
				function(response){
					$gfb_maintab_content.removeClass("loading-block");
					var obj=$.parseJSON(response);
							
					if (obj.msg != 'false') {	
						swal({ title:obj.text, type:"success" }, function(){
							window.location.replace(obj.url);
						});
					}	
					else {
						swal({ title:obj.text, type:"error" });		
					}
				}
			);
			return false;
		   }
		});
	
		
	});
}(jQuery));