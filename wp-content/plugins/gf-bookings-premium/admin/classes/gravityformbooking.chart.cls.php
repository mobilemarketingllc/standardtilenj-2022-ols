<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
class GFB_GravityFormBookingChart {
	
	public $mainCalssFunction;
	
	function __construct() {
		$this->mainCalssFunction = new gfb_bookings_addon();
	}
	
	/* Pie Chart */
	function gfbAppoinmentStatus() {
		global $wpdb;
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		 
		$appointments_status_result = $wpdb->get_results( "SELECT count(appointment_id) as appointment, status as statusno, CASE status WHEN 1 THEN 'PAYMENT PENDING' WHEN 2 THEN 'AWAITING' WHEN 3 THEN 'CANCELLED' WHEN 4 THEN 'VISITED' end as status FROM ".$gfb_appointments_mst." WHERE is_deleted=0 group by status ", ARRAY_A );
		
		/* Appointment count list */
		$appoints = $this->mainCalssFunction->gfb_array_column($appointments_status_result, "appointment");		
		$appointment_arr = '['.implode(', ',$appoints).']';		
		
		/* Appointment status name */
		$appointstatus = $this->mainCalssFunction->gfb_array_column($appointments_status_result, "status");		
		$appointment_status_array = '["'.implode('","',$appointstatus).'"]';
		
		$appoint_result = array("appointment" => $appointment_arr, "status" => $appointment_status_array);
						
		return json_encode($appoint_result);
	}	
	
	/* Get Revenue & Booking by staff */
	function gfbGetEarNBookByStaff() {
		global $wpdb;		
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
		$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
		
		$er_bok_staff_result = $wpdb->get_results( "SELECT sm.staff_id, sm.staff_name, count(am.appointment_id) as appointments, COALESCE(SUM(pm.total_amt),0) as amount FROM ".$gfb_appointments_mst." am INNER JOIN ".$gfb_staff_mst." sm on am.staff_id=sm.staff_id LEFT JOIN ".$gfb_payments_mst." pm on am.appointment_id=pm.appointment_id WHERE am.is_deleted=0 GROUP BY am.staff_id", ARRAY_A );
		
		/*Staff list */
		$staff_res = $this->mainCalssFunction->gfb_array_column($er_bok_staff_result, "staff_name");
		$staff_array = '["'.implode('","',$staff_res).'"]';
		
		/* total no. of appointment */
		$appointment_res = $this->mainCalssFunction->gfb_array_column($er_bok_staff_result, "appointments");
		$appointment_array = '['.implode(',',$appointment_res).']';
		
		/* total earning */
		$earning_res = $this->mainCalssFunction->gfb_array_column($er_bok_staff_result, "amount");
		$earning_array = '['.implode(', ',$earning_res).']';
		
		$full_result = array("staff" => $staff_array, "appointments" => $appointment_array, "amount" => $earning_array);
						
		return json_encode($full_result);
	}	
	
	/* Get total earnings & booking by service */
	function gfbGetEarBokService() {
		global $wpdb;
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';
		$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
		
		$er_bok_service_result = $wpdb->get_results( "SELECT sm.service_id, sm.service_title, COALESCE(SUM(pm.total_amt),0) as amount, count(am.appointment_id) as appointments FROM ".$gfb_appointments_mst." am RIGHT JOIN ".$gfb_services_mst." sm ON sm.service_id=am.service_id and am.is_deleted=0 and am.status IN (2,3,4) LEFT JOIN ".$gfb_payments_mst." pm ON am.appointment_id=pm.appointment_id and pm.payment_status = 1 WHERE sm.is_deleted=0 GROUP BY sm.service_id", ARRAY_A );
						
		/*Service list */
		$service_res = $this->mainCalssFunction->gfb_array_column($er_bok_service_result, "service_title");
		$service_array = '["'.implode('","',$service_res).'"]';
		
		/* total no. of appointment */
		$appointment_res = $this->mainCalssFunction->gfb_array_column($er_bok_service_result, "appointments");
		$appointment_array = '['.implode(',',$appointment_res).']';
		
		/* total earning */
		$earning_res = $this->mainCalssFunction->gfb_array_column($er_bok_service_result, "amount");
		$earning_array = '['.implode(', ',$earning_res).']';
		
		$full_result = array("service" => $service_array, "appointments" => $appointment_array, "amount" => $earning_array);
						
		return json_encode($full_result);		
	}	
	
	/* Get payment chart type wise */
	function gfbGetPayment() {
		global $wpdb;
		$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
		
		$payment_result = $wpdb->get_results( "SELECT round(sum(pm1.total_amt) * 100 / pm2.total) as amount, CASE pm1.payment_type WHEN 2 THEN 'STRIPE' WHEN 3 THEN 'CASH ON DELIVERY' end as ptype FROM ".$gfb_payments_mst." pm1 CROSS JOIN ( SELECT SUM(total_amt) as total FROM ".$gfb_payments_mst." WHERE is_deleted=0 ) pm2 WHERE pm1.is_deleted=0 group by pm1.payment_type", ARRAY_A );
			
		/* payment percentage */
		$payment_amt = $this->mainCalssFunction->gfb_array_column($payment_result, "amount");		
		$payment_amt_arr = '['.implode(', ',$payment_amt).']';		
		
		/* payment name */
		$payment_type = $this->mainCalssFunction->gfb_array_column($payment_result, "ptype");		
		$payment_type_array = '["'.implode('","',$payment_type).'"]';
		
		$payment_result = array("amount" => $payment_amt_arr, "ptype" => $payment_type_array);
						
		return json_encode($payment_result);
	}	
	
	/* Total Customers */
	function gfbGetTotalCustomers() {
		global $wpdb;
		$gfb_customer_mst = $wpdb->prefix . 'gfb_customer_mst';		
		$total_customer = $wpdb->get_results( "SELECT COUNT(customer_id) AS total_customer FROM ".$gfb_customer_mst." WHERE is_deleted=0", ARRAY_A );						
		return $total_customer;	
	}
	
	/* Total Services */
	function gfbGetTotalServices() {
		global $wpdb;
		$gfb_services_mst = $wpdb->prefix . 'gfb_services_mst';		
		$total_services = $wpdb->get_results( "SELECT COUNT(service_id) AS total_services FROM ".$gfb_services_mst." WHERE is_deleted=0", ARRAY_A );						
		return $total_services;	
	}
	
	/* Total Staff */
	function gfbGetTotalStaff() {
		global $wpdb;
		$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';		
		$total_staff = $wpdb->get_results( "SELECT COUNT(staff_id) AS total_staff FROM ".$gfb_staff_mst." WHERE is_deleted=0", ARRAY_A );						
		return $total_staff;	
	}
	
	/* Total Collection */
	function gfbGetTodayTotalCollection($today_date) {
		global $wpdb;
		$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
		$total_collection = $wpdb->get_results( "SELECT SUM(total_amt) AS total_collection FROM ".$gfb_payments_mst." WHERE is_deleted=0 and payment_date LIKE '".$today_date."%'", ARRAY_A );						
		return $total_collection;	
	}
	
}
	
global $charts;
$charts = new GFB_GravityFormBookingChart();