<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; } 

class GravityFormBookingMenu {
	
	public function gfbCreateMenu() {	
	
		global $current_user;
	
		if ( $current_user->has_cap( 'gfb_staff_role' ) ) {
			
			add_menu_page( __( 'Gravity Forms Booking', 'gfb' ), __( 'Gravity Forms Booking', 'gfb' ), 'gfb_staff_role', 'gravity-form-booking-menu',  array( &$this , 'gfbMenuPages'), 'dashicons-calendar-alt', 23 );
			
			/* Calendar Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Calendar', 'gfb' ), __( 'Calendar', 'gfb' ), 'gfb_staff_role', 'gravity-form-booking-menu', array( $this, 'gfbMenuPages' ) );
			
			/* Appointments Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Appointments', 'gfb' ), __( 'Appointments', 'gfb' ), 'gfb_staff_role', 'gravity-form-booking-appointments', array( $this, 'gfbMenuPages' ) );
			
			
			/* Staff Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Personal Profile', 'gfb' ), __( 'Personal Profile', 'gfb' ), 'gfb_staff_role', 'gravity-form-booking-staff', array( $this, 'gfbMenuPages' ) );
			
			// /* How to use */
			// add_submenu_page( 'gravity-form-booking-menu', __( 'How To Use?', 'gfb' ), __( 'How To Use?', 'gfb' ), 'read', 'https://gravitymore.com/documentation/gravity-booking-form/' );
		}
		else {			
			
			add_menu_page( __( 'Gravity Forms Booking', 'gfb' ), __( 'Gravity Forms Booking', 'gfb' ), 'manage_options', 'gravity-form-booking-menu',  array( &$this , 'gfbMenuPages'), 'dashicons-calendar-alt', 23 );
			
			/* Dashboard Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Dashboard', 'gfb' ), __( 'Dashboard', 'gfb' ), 'manage_options', 'gravity-form-booking-menu', array( $this, 'gfbMenuPages' ) );	
			
			/* Calendar Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Calendar', 'gfb' ), __( 'Calendar', 'gfb' ), 'manage_options', 'gravity-form-booking-calendar', array( $this, 'gfbMenuPages' ) );
			
			/* Appointments Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Appointments', 'gfb' ), __( 'Appointments', 'gfb' ), 'manage_options', 'gravity-form-booking-appointments', array( $this, 'gfbMenuPages' ) );
			
			/* Category Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Service Categories', 'gfb' ), __( 'Service Categories', 'gfb' ), 'manage_options', 'gravity-form-booking-categories', array( $this, 'gfbMenuPages' ) );
			
			/* Services Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Services', 'gfb' ), __( 'Services', 'gfb' ), 'manage_options', 'gravity-form-booking-services', array( $this, 'gfbMenuPages' ) );
			
			/* Staff Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Staff', 'gfb' ), __( 'Staff', 'gfb' ), 'manage_options', 'gravity-form-booking-staff', array( $this, 'gfbMenuPages' ) );
			
			/* Time Slot Menu */
			//add_submenu_page( 'gravity-form-booking-menu', __( 'Time Slot', 'gfb' ), __( 'Time Slot', 'gfb' ), 'manage_options', 'gravity-form-booking-time-slot', array( $this, 'gfbMenuPages' ) );
			
			/* Holiday Menu */
			//add_submenu_page( 'gravity-form-booking-menu', __( 'Holiday', 'gfb' ), __( 'Holiday', 'gfb' ), 'manage_options', 'gravity-form-booking-holiday', array( $this, 'gfbMenuPages' ) );
			
			/* Customers Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Customers', 'gfb' ), __( 'Customers', 'gfb' ), 'manage_options', 'gravity-form-booking-customers', array( $this, 'gfbMenuPages' ) );
			
			/* Payment Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Payments', 'gfb' ), __( 'Payments', 'gfb' ), 'manage_options', 'gravity-form-booking-payment', array( $this, 'gfbMenuPages' ) );
			
			/* Email Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Email Notification', 'gfb' ), __( 'Email Notification', 'gfb' ), 'manage_options', 'gravity-form-booking-email', array( $this, 'gfbMenuPages' ) );
			
			/* Settings Menu */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Settings', 'gfb' ), __( 'Settings', 'gfb' ), 'manage_options', 'gravity-form-booking-settings', array( $this, 'gfbMenuPages' ) );

			/* Help/Support Link */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Support', 'gfb' ), __( 'Support', 'gfb' ), 'read', 'https://gravitybooking.com/contact-us/' );

			/* Technical Documentation */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Technical Documentation', 'gfb' ), __( 'Technical Documentation', 'gfb' ), 'read', 'https://gravitymore.com/documentation/gravity-booking-form/' );

			/* Contact Us */
			add_submenu_page( 'gravity-form-booking-menu', __( 'Customization', 'gfb' ), __( 'Customization', 'gfb' ), 'read', 'https://gravitybooking.com/custom-request' );

			// replace the slug with your external url
	    	//$menu[$menu_pos][2] = "http://www.example.com";
			
			if($current_user->has_cap( 'gfb_customer_role' )){
				/* How to use */
			}else{
			/* How to use */
			// add_submenu_page( 'gravity-form-booking-menu', __( 'How To Use?', 'gfb' ), __( 'How To Use?', 'gfb' ), 'read', 'https://gravitymore.com/documentation/gravity-booking-form/' );
			}
		}

	}
	
	public function gfbMenuPages() {
		
		global $current_user;
		if ( $current_user->has_cap( 'gfb_staff_role' ) ) {
			
			if( $_GET['page'] == 'gravity-form-booking-menu' ) {
				// Dashboard 
				require_once( GFB_ADMIN_TAB . 'calendar/calendar.php' );
			}
		}else if ( $current_user->has_cap( 'gfb_customer_role' ) ) {
			
			if( $_GET['page'] == 'gravity-form-booking-menu' ) {
				// Dashboard 
				require_once( GFB_ADMIN_TAB . 'calendar/calendar.php' );
			}
		}
		else{
			
			if( $_GET['page'] == 'gravity-form-booking-menu' ) {
				// Dashboard 
				require_once( GFB_ADMIN_TAB . 'dashboard/dashboard.php' );
			}		
		}
		
		
		
		
		
		
		
		if( $_GET['page'] == 'gravity-form-booking-calendar' ) {
			/* Calendar */
			require_once( GFB_ADMIN_TAB . 'calendar/calendar.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-appointments' ) {
			/* Appointment */
			require_once( GFB_ADMIN_TAB . 'appointments/listAppointments.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-time-slot' ) {
			/* Time Slot */
			require_once( GFB_ADMIN_TAB . 'timeSlots/timeSlotController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-holiday' ) {
			/* Category */
			require_once( GFB_ADMIN_TAB . 'holiday/holidayController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-categories' ) {
			/* Category */
			require_once( GFB_ADMIN_TAB . 'categories/listCategory.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-services' ) {
			/* Service */
			require_once( GFB_ADMIN_TAB . 'services/listService.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-staff' ) {
			/* Staff */
			require_once( GFB_ADMIN_TAB . 'staff/staffController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-customers' ) {
			/* Customer */
			require_once( GFB_ADMIN_TAB . 'customers/listCustomers.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-payment' ) {
			/* Payment */
			require_once( GFB_ADMIN_TAB . 'payment/listPayments.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-email' ) {
			/* Email */
			require_once( GFB_ADMIN_TAB . 'email/emailController.php' );
		}
		elseif( $_GET['page'] == 'gravity-form-booking-settings' ) {
			/* Settings */
			require_once( GFB_ADMIN_TAB . 'settings/settingsController.php' );
		}
		
	}
}

global $gfbmenu;
$gfbmenu = new GravityFormBookingMenu;