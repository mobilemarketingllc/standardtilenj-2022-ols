<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
class GravityFormBookingPayment {
	
	function __construct() {		
		add_action( 'init', array(&$this, 'gfbExportPaymentToCsv') );
		add_action( 'init', array(&$this, 'gfbExportPaymentToPdf') );
		add_action( 'wp_ajax_gfb_payment_status_change',  array(&$this, 'gfbPaymentStatusChange') );
	}	


	function gfbPaymentStatusChange() {

		if ( isset( $_POST['payment_id'] ) && '' != $_POST['payment_id']  ) {

			global $wpdb;
			
			$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
			$update_array = array(
				'payment_status' => $_POST['payment_status']
			);
			$where = array(
				'payment_id' => $_POST['payment_id']
			);

			$update_payment_status = $wpdb->update( $gfb_payments_mst, $update_array, $where);

			if ( $update_payment_status ) {
				echo $update_payment_status;
			}
		}

		wp_die();
 	}
	
	/* Export To CSV */
	
	function gfbExportPaymentToCsv() {
		
		if(isset($_REQUEST['gfb_payment_export_to_csv']) && $_REQUEST['gfb_payment_export_to_csv'] == 'export_csv') {
			
			global $wpdb;
			
			$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
			$gfb_appointments_mst = $wpdb->prefix . "gfb_appointments_mst";
			$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";	
			
			$paymentList = $wpdb->get_results( "SELECT am.booking_ref_no, cm.customer_name, cm.customer_email, DATE_FORMAT( pm.payment_date, '%M %d, %Y, %H:%i:%s' ) AS payment_date, pm.total_amt, pm.currency_code, CASE pm.payment_status WHEN 0 THEN 'PENDING' WHEN 1 THEN 'COMPLETED' END AS payment_status
			FROM ".$gfb_payments_mst." as pm
			INNER JOIN ".$gfb_appointments_mst." as am ON pm.appointment_id=am.appointment_id
			INNER JOIN ".$gfb_customer_mst." as cm ON am.customer_id=cm.customer_id
			WHERE pm.is_deleted=0 ORDER BY pm.payment_date desc, pm.payment_id desc", ARRAY_A );			
	
			header('Content-type: text/csv');
			header('Content-Disposition: attachment; filename="payment_report.csv"');
			header('Pragma: no-cache');
			header('Expires: 0');

			$file = fopen('php://output', 'w');
			
			$column_list = array("Booking Number", "Customer Name", "Customer Email", "Payment Date & Time", "Total Amount", "Currency", "Payment Status");

			fputcsv($file, $column_list);
					
			foreach ($paymentList as $payment) {
				fputcsv($file, $payment);
			}

			exit();
		}
	}
	
	/* Export To PDF */		
	function gfbExportPaymentToPdf() {
		
		if(isset($_REQUEST['gfb_payment_export_to_pdf']) && $_REQUEST['gfb_payment_export_to_pdf'] == 'gfb_export_pdf') {
			
			global $wpdb;
			global $gfbFpdfObj;
			global $title;
			
			$title = 'Payment List';
			$gfbFpdfObj->SetTitle($title);	
			$gfbFpdfObj->AddPage('L', 'A4');
			
			$gfb_payments_mst = $wpdb->prefix . "gfb_payments_mst";
			$gfb_appointments_mst = $wpdb->prefix . "gfb_appointments_mst";
			$gfb_customer_mst = $wpdb->prefix . "gfb_customer_mst";		
				
			$paymentList = $wpdb->get_results( "SELECT am.booking_ref_no, cm.customer_name, cm.customer_email, pm.transaction_id, DATE_FORMAT( pm.payment_date, '%M %d, %Y, %H:%i:%s' ) AS payment_date, pm.total_amt,pm.currency_code, CASE pm.payment_type WHEN 2 THEN 'STRIPE' WHEN 3 THEN 'CASH ON DELIVERY' END AS payment_type, CASE pm.payment_status WHEN 0 THEN 'PENDING' WHEN 1 THEN 'COMPLETED' END AS payment_status
			FROM ".$gfb_payments_mst." as pm
			INNER JOIN ".$gfb_appointments_mst." as am ON pm.appointment_id=am.appointment_id
			INNER JOIN ".$gfb_customer_mst." as cm ON am.customer_id=cm.customer_id
			WHERE pm.is_deleted=0 ORDER BY pm.payment_date desc, pm.payment_id desc", ARRAY_A );
			
			if($paymentList) {
			
				$width_cell=array(10, 45, 45, 45, 45, 30, 30, 35);
				$gfbFpdfObj->SetFont('Arial','B',12);
				
				$gfbFpdfObj->SetFillColor(255,255,255); // Background color of header 
				// Header starts /// 
				
				$gfbFpdfObj->Cell($width_cell[0],10,'ID',1,0,'C',true);
				$gfbFpdfObj->Cell($width_cell[1],10,'Booking Number',1,0,'C',true);
				$gfbFpdfObj->Cell($width_cell[2],10,'Customer Name',1,0,'C',true); 
				$gfbFpdfObj->Cell($width_cell[3],10,'Customer Email',1,0,'C',true); 
				$gfbFpdfObj->Cell($width_cell[4],10,'Date & Time',1,0,'C',true); 
				$gfbFpdfObj->Cell($width_cell[5],10,'Amount',1,0,'C',true); 				
				$gfbFpdfObj->Cell($width_cell[6],10,'Currency',1,0,'C',true); 				
				$gfbFpdfObj->Cell($width_cell[7],10,'Status',1,0,'C',true); 
				$gfbFpdfObj->Ln();
				//// Header ends ///////
				
				$gfbFpdfObj->SetFont('Arial','',9);
				$gfbFpdfObj->SetFillColor(235,236,236); // Background color of header 
				$fill=false; // to give alternate background fill color to rows 
				$countid=1;
				
				/// each record is one row  ///
				foreach ($paymentList as $row) {
					$gfbFpdfObj->Cell($width_cell[0],10,$countid,1,0,'C',$fill);
					$gfbFpdfObj->Cell($width_cell[1],10,$row['booking_ref_no'],1,0,'C',$fill);
					$gfbFpdfObj->Cell($width_cell[2],10,$row['customer_name'],1,0,'C',$fill);
					$gfbFpdfObj->Cell($width_cell[3],10,$row['customer_email'],1,0,'C',$fill);
					$gfbFpdfObj->Cell($width_cell[4],10,$row['payment_date'],1,0,'C',$fill);
					$gfbFpdfObj->Cell($width_cell[5],10,$row['total_amt'],1,0,'C',$fill);					
					$gfbFpdfObj->Cell($width_cell[6],10,$row['currency_code'],1,0,'C',$fill);					
					$gfbFpdfObj->Cell($width_cell[7],10,$row['payment_status'],1,0,'C',$fill);
					$gfbFpdfObj->Ln();
					$fill = !$fill; // to give alternate background fill  color to rows
					$countid++;
				}
				/// end of records ///
			
			}
			
			$gfbFpdfObj->Output("payment_list.pdf", "D");
		}
	}
	
	/* Stripe payment function */
	public static function gfbStripePayment($transaction_id, $amount, $payment_status, $appointment_id) {
		
		global $wpdb;
		global $gfbAppointmentObj;
		global $gfbStaff;
		$gfb_payments_mst = $wpdb->prefix . 'gfb_payments_mst';
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		$gfb_createdby = get_current_user_id();
		$gfb_ip = GFB_Core::gfbIpAddress();	
		$msg = ''; $mail = false;
		
		
		$stripe_array = array(
			'transaction_id' => ''.wp_kses_post(trim($transaction_id)).'',
			'payment_date'	 => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
			'payment_type'	 => 2,
			'total_amt'		 => ''.wp_kses_post(trim(substr($amount,0,-2))).'',
			'paid_amt' 		 => ''.wp_kses_post(trim(substr($amount,0,-2))).'',
			'payment_status' => ''.wp_kses_post(trim($payment_status)).'',
			'currency_code'  => ''.wp_kses_post(trim(get_option('gfb_currency_code'))).'',
			'appointment_id' => ''.wp_kses_post(trim($appointment_id)).'',
			'created_date'   => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
			'ip_address' 	 => ''.wp_kses_post($gfb_ip).'',
		);
					
		$insert_stripe_data = $wpdb->insert($gfb_payments_mst, $stripe_array);
		
		/*check if payment is successful than change status of appointment*/				
		if(trim($payment_status) == 1)
		{
			$appointment_array = array(					
				'status' 		=> 2,
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',		
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).''
			);
				
			$where_array = array('appointment_id' => ''.wp_kses_post(trim($appointment_id)).'');					
			$update_data = $wpdb->update($gfb_appointments_mst, $appointment_array , $where_array );
				
			if( !empty( get_option("gfb_client_id") ) && !empty( get_option("gfb_client_secrete") ) )	 
			{
				/* get appointment data for google sync entry*/
				$appointment_res = $gfbAppointmentObj->gfbAppointmentInfoById($appointment_id);
					
				$staffGoogleData = $gfbStaff->gfbStaffGoogleData($appointment_res[0]["staff_id"]);

				if($staffGoogleData[0]['staff_gcal_data'] != '')
				{
					$add_appointments_arg = array(
						'customer_id' 			=> ''.wp_kses_post(trim($appointment_res[0]["customer_id"])).'',
						'appointment_date' 		=> ''.wp_kses_post(trim($appointment_res[0]["appointment_date"])).'',
						'staff_slot_mapping_id' => ''.wp_kses_post(trim($appointment_res[0]["staff_slot_mapping_id"])).'',
						'service_id' 			=> ''.wp_kses_post(trim($appointment_res[0]["service_id"])).'',
						'staff_id'  			=> ''.wp_kses_post(trim($appointment_res[0]["staff_id"])).'',
						'status' 				=> ''.wp_kses_post(trim($appointment_res[0]["status"])).'',
						'booking_ref_no' 		=> ''.wp_kses_post(trim($appointment_res[0]["booking_ref_no"])).'',
					);
					$google_event_id = $gfbAppointmentObj->createGoogleCalendarEvent($add_appointments_arg);
					$appointment_array = array(
						'google_event_id' => $google_event_id,
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',		
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
										
					$where_array = array('appointment_id' => ''.wp_kses_post(trim($appointment_id)).'');
										
					$update_data = $wpdb->update($gfb_appointments_mst, $appointment_array , $where_array );

				}					
			}
			
			$mail = GFB_Core::gfbReplaceCode($appointment_id, "gfb_user_appointment_confirmation", "gfb_staff_appointment_request", "gfb_admin_appointment_request");
								
			if($update_data == true)
			{						
				return 1;
			}
			else {
				return 0;
			}
		}			
	}				
}

global $gfbPaymentObj;
$gfbPaymentObj = new GravityFormBookingPayment;
	
