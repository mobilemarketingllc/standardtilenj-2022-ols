<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
class GFB_GoogleCalendarApi
{
	public function createWatchRequest($access_token, $calendar_id) {	
		
		$url = sprintf("https://www.googleapis.com/calendar/v3/calendars/%s/events/watch",$calendar_id );
		$call_back_url = site_url('wp-json/gfb/pushnotif');
		$fields = json_encode(array(
			'id'        => mt_rand(),
			'type'      => 'web_hook',
			'address'   => $call_back_url
		));
		
		//echo $fields;exit;
		$headers[] = 'Content-Type: application/json';
		$headers[] = 'Authorization: Bearer ' . $access_token;
		
		/* send POST request */
		$channel = curl_init();
		curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($channel, CURLOPT_URL, $url);
		curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($channel, CURLOPT_POST, true);
		curl_setopt($channel, CURLOPT_POSTFIELDS, $fields);
		//curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 2);
		//curl_setopt($channel, CURLOPT_TIMEOUT, 3);
		$response = curl_exec($channel);
		
		$http_code = curl_getinfo($channel,CURLINFO_HTTP_CODE);
		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text' => $response) );
			die();
		}
		curl_close($channel);
		return $response;
	}
	
	public function getEvents($access_token, $calendar_id, $limit_events) {	
		
		$url = "https://www.googleapis.com/calendar/v3/calendars/$calendar_id/events";
		
		if($limit_events!='' && $limit_events!=0)
		{
			$url .="?maxResults=$limit_events";
		}
		//echo $url;exit;
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		$data = json_decode(curl_exec($ch), true);
		//print_r($data);exit;
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	
		if($http_code != 200)
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to fetch events') );
			die();
		}
			
		return $data;
	}
	
	public function getEventUpdates($access_token,$syncToken,$calendar_id) {	
		
		$url = "https://www.googleapis.com/calendar/v3/calendars/$calendar_id/events?syncToken=$syncToken";
		
		
		//echo $url;exit;
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		//print_r($data);exit;	
		/*if($http_code != 200)
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to fetch events') );
			die();
		}*/
			
		return $data;
	}
				
	function revokeToken($refreshToken) {
		
		$revoke_uri = 'https://accounts.google.com/o/oauth2/revoke?token='.$refreshToken ;
			
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $revoke_uri);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type' => 'application/x-www-form-urlencoded'));	
		
		$data = json_decode(curl_exec($ch), true);

		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);

		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to revoke account') );
			die();
		}
		return $http_code;
	}
	
	public function GetAccessToken($client_id, $redirect_uri, $client_secret, $code) {	
		
		
		$url = 'https://www.googleapis.com/oauth2/v4/token';			
		
		$curlPost = 'client_id=' . $client_id . '&redirect_uri=' . $redirect_uri . '&client_secret=' . $client_secret . '&code='. $code . '&grant_type=authorization_code';
		
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);

		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	
		if($http_code != 200) 
		{
			
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to receieve access token') );
			die();
		}
			
		return $data;
	}
	
	/* refrehing token after defined time */
	public function GetNewAccessToken($client_id, $client_secret, $refresh_token) {	
			
		$url = 'https://oauth2.googleapis.com/token';

		$curlPost = 'client_id=' . $client_id . '&client_secret=' . $client_secret . '&refresh_token=' . $refresh_token . '&grant_type=refresh_token';
		
		$ch = curl_init();	
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);	
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	
	
		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to receieve access token') );
			die();
		}
		return $data;
	}

	public function GetUserCalendarTimezone($access_token) {
				
		$url_settings = 'https://www.googleapis.com/calendar/v3/users/me/settings/timezone';
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_settings);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
		
		$data = json_decode(curl_exec($ch), true); 
		//print_r($data);exit;
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		//echo $http_code;
		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text_GetUserCalendarTimezone' => $data['error']['message'] ) );
			die();
		}
		else
		{
			return $data['value'];
		}
	}

	

	public function GetCalendarsList($access_token) {
		
		$url_parameters = array();

		$url_parameters['fields'] = 'items(id,summary,timeZone)';
		$url_parameters['minAccessRole'] = 'owner';

		$url_calendars = 'https://www.googleapis.com/calendar/v3/users/me/calendarList?'. http_build_query($url_parameters);
		
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_calendars);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token));	
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);	
		$data = json_decode(curl_exec($ch), true); 
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);		
		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to get calendars list') );
			die();
		}

		return $data['items'];
	}

	public function CreateCalendarEvent($calendar_id, $summary, $event_time, $event_timezone, $access_token) 
	{
		
		$url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events';
		
		$curlPost = array('summary' => $summary);
		$curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
		$curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);
		
		$ch = curl_init();
		//print_r($event_time);exit;
		curl_setopt($ch, CURLOPT_URL, $url_events);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));	
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));	
		$data = json_decode(curl_exec($ch), true);
		//print_r($data);exit;	
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	
					
		if($http_code != 200) 
		{
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to create event') );
			die();
		}
		
		return $data['id'];
	}
	
	public function gfbGetCalendarId($staffid) {
		global $wpdb;
		$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
		
		$getid = $wpdb->get_results("SELECT staff_gcal_id FROM ".$gfb_staff_mst." WHERE is_deleted=0 AND staff_id=".$staffid."", ARRAY_A);
				
		if( empty($getid[0]["staff_gcal_id"]) ) {
			return "primary";	
		}
		else {
			return $getid[0]["staff_gcal_id"];	
		}
		
	}
	
	public function gfbDeleteCalendarEvent($event_id, $calendar_id, $access_token) {
		$url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events/' . $event_id;

		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_events);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token, 'Content-Type: application/json'));		
		$data = json_decode(curl_exec($ch), true);
		$http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);
		if($http_code != 204) {
			echo json_encode( array('msg' => 'false', 'text' => 'Failed to delete event') );
			die();
		}
		else {
			return true;	
		}
	}
}