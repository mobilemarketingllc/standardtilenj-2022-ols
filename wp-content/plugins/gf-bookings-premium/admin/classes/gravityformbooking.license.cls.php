<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }

class GravityFormBookingLicense {
	
	public function __construct(){
		add_action( "wp_ajax_gfb_get_purchase_code", array(&$this, "gfbGetPurchaseCode") );
		add_action( "admin_enqueue_scripts", array(&$this, "gfbLicenseScript") );			
	}
	
	public function gfbGetPurchaseCode(){
		
		$purchase_code = trim($_POST["purchase_code"]);
		
		$url = "http://appbuddy.accrete.solutions/api/verifyPurchaseCode.php?pccde=".trim($purchase_code);

		$client = curl_init($url);
		curl_setopt($client, CURLOPT_URL, $url);
		curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($client, CURLOPT_TIMEOUT, 30);
		curl_setopt($client, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($client, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1" );
		$response = curl_exec($client);		
		$result = json_decode($response);

		if($result->status == 200){
			
			$this->gfbGenerateOption($result->data);
			echo json_encode( array('msg' => 'true', 'text' => $result->status_message, 'url' => admin_url().'admin.php?page=gravity-form-booking-menu' ) ); 
			exit;
		}
		elseif($result->status == 400){
			echo json_encode( array('msg' => 'false', 'text' => $result->status_message ) ); 
			exit;
		}
	}
	
	public function gfbGenerateOption($value) {
		return add_option("gfb_envato_purchase_code", $value);	
	}
	
	public function gfbLicenseScript(){		
		/* Scripts */
		wp_enqueue_script( 'jquery' );
		//wp_enqueue_script( 'gfb-bootstrap-material-datetimepicker-js', GFB_ADMIN_ASSET . 'js/bootstrap-material-datetimepicker.js', array( 'jquery' ), GFB_VERSION, true );
		//wp_enqueue_script( 'gfb-custom-timepicker-js', GFB_ASSET . 'js/sweetalert.min.js', array('jquery'), GFB_VERSION, true );
		//wp_enqueue_script( 'gfb-sweetalert-js', GFB_ASSET . 'js/sweetalert.min.js', array('jquery'), GFB_VERSION, true );
		wp_enqueue_script( 'gfb-license-js', GFB_ADMIN_ASSET . 'js/license.js', array('jquery'), GFB_VERSION, true );
		wp_enqueue_script( 'gfb-validate-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-license-js', 'js_license_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
		
		/* Styles */
		wp_enqueue_style( 'gfb-sweetalert-style', GFB_ASSET . 'css/sweetalert.css', array(), GFB_VERSION);
		wp_enqueue_style( 'gfb-bootstrap-material-datetimepicker-css', GFB_ADMIN_ASSET . 'css/bootstrap-material-datetimepicker.css', array(), GFB_VERSION );
		wp_enqueue_style( 'gfb-admin-style-css', GFB_ADMIN_ASSET . 'css/admin.style.css', array(), GFB_VERSION );

	}
	
	
}

global $gfblicense;
$gfblicense = new GravityFormBookingLicense;