<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
	class GravityFormBookingCategory {
		
		function __construct() {
			
			add_action( 'wp_ajax_gfb_add_category', array(&$this, 'gfbAddCategories') );
			add_action( 'wp_ajax_gfb_update_category', array(&$this, 'gfbUpdateCategory') );
			add_action( 'wp_ajax_gfb_delete_category', array(&$this, 'gfbDeleteCategory') );
			add_action( 'wp_ajax_gfb_display_edit_category', array(&$this, 'gfbEditCategoryForm') );
			add_action( 'init', array(&$this, 'gfbExportCategoryToCsv') );
			add_action( 'init', array(&$this, 'gfbExportCategoryToPdf') );
		}
		
		/* Export To CSV */		
		function gfbExportCategoryToCsv() {
			
			if(isset($_REQUEST['gfb_category_export_to_csv']) && $_REQUEST['gfb_category_export_to_csv'] == 'gfb_export_csv') {
				
				global $wpdb;
				
				$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
				
				$categoryList = $wpdb->get_results( "SELECT category_name FROM ".$gfb_categories_mst." WHERE is_deleted=0 ORDER BY category_name asc", ARRAY_A );	
		
				header('Content-type: text/csv');
				header('Content-Disposition: attachment; filename="category_list.csv"');
				header('Pragma: no-cache');
				header('Expires: 0');
	
				$file = fopen('php://output', 'w');
				
				$column_list = array("Category Name");
	
				fputcsv($file, $column_list);
						
				foreach ($categoryList as $category) {
					fputcsv($file, $category);
				}
	
				exit();
			}
		}
		
		/* Export To PDF */		
		function gfbExportCategoryToPdf() {
			
			if(isset($_REQUEST['gfb_category_export_to_pdf']) && $_REQUEST['gfb_category_export_to_pdf'] == 'gfb_export_pdf') {
				
				global $wpdb;
				global $gfbFpdfObj;
				global $title;
				
				$title = 'Service Category List';
				$gfbFpdfObj->SetTitle($title);				
				$gfbFpdfObj->AddPage();
				
				$gfb_categories_mst = $wpdb->prefix . "gfb_categories_mst";
				
				$categoryList = $wpdb->get_results( "SELECT category_id, category_name FROM ".$gfb_categories_mst." WHERE is_deleted=0 ORDER BY category_name asc", ARRAY_A );
				
				if($categoryList){
				
					$width_cell=array(80,100);
					$gfbFpdfObj->SetFont('Arial','B',14);
					
					$gfbFpdfObj->SetFillColor(255,255,255); // Background color of header 
					// Header starts /// 
					
					$gfbFpdfObj->Cell($width_cell[0],10,'ID',1,0,'C',true); // First header column 
					$gfbFpdfObj->Cell($width_cell[1],10,'NAME',1,0,'C',true); // Second header column
					$gfbFpdfObj->Ln();
					//// Header ends ///////
					
					$gfbFpdfObj->SetFont('Arial','',12);
					$gfbFpdfObj->SetFillColor(235,236,236); // Background color of header 
					$fill=false; // to give alternate background fill color to rows 
					
					$countid=1;
					/// each record is one row  ///
					foreach ($categoryList as $row) {
						$gfbFpdfObj->Cell($width_cell[0],10,$countid,1,0,'C',$fill);
						$gfbFpdfObj->Cell($width_cell[1],10,$row['category_name'],1,0,'C',$fill);
						$gfbFpdfObj->Ln();
						$fill = !$fill; // to give alternate background fill  color to rows
						$countid++;
					}
					/// end of records ///
				
				}
				
				$gfbFpdfObj->Output("category_list.pdf", "D");
			}
		}
		
		/* Category List */
		function gfbListCategories(){
			
			global $wpdb;
			$category_tbl = $wpdb->prefix . 'gfb_categories_mst';			
			$category_result = $wpdb->get_results( 'SELECT category_id, category_name FROM '.$category_tbl.' WHERE is_deleted=0', ARRAY_A );				
			return $category_result;	
		}
		
		/* Category Detail */
		function gfbCategoryDetails($cat_id){
			global $wpdb;
			$category_tbl = $wpdb->prefix . 'gfb_categories_mst';			
			$category_result = $wpdb->get_results( 'SELECT category_id, category_name FROM '.$category_tbl.' WHERE is_deleted=0 AND category_id='.$cat_id.'', ARRAY_A );				
			return $category_result;	
		}
		
		/* Edit Category Form */
		function gfbEditCategoryForm(){
			
			if (isset($_POST['category_id'])):
				require(GFB_ADMIN_TAB . 'categories/editCategory.php');
			endif;
			die();	
		}
		
		/* AJAX FUNCTION TO ADD CATEGORIES */		
		function gfbAddCategories() {
			
			parse_str($_POST["category_form"], $category_form);
			
			if ( ! isset( $category_form['category_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Cannot find nonce.') );
				die();
			}
		
			if ( ! wp_verify_nonce( $category_form['category_nonce'], 'category_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Invalid nonce value.') );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_categories_mst = $wpdb->prefix . 'gfb_categories_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($category_form['category_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Category title should not be blank.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT * FROM ".$gfb_categories_mst." WHERE is_deleted=0 AND category_name='".trim($category_form['category_name'])."'", ARRAY_A );
				
				if( empty( $check_exists ) ) 
				{
					$add_cat_arg = array(
						'category_name' => ''.wp_kses_post(trim($category_form['category_name'])).'',
						'created_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'created_date'  => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$insert_category = $wpdb->insert($gfb_categories_mst, $add_cat_arg);	
					
					if($insert_category)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Category added successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-categories') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Category not added successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'Category already exists.') );
					die();	
				}
				
			}
			
			die();
			
		}
		
		/* AJAX FUNCTION TO UPDATE CATEGORIES */
		function gfbUpdateCategory() {
			
			parse_str($_POST["category_form"], $category_form);
			
			if ( ! isset( $category_form['edit_category_nonce'] ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Cannot find nonce.') );
				die();
			}
		
			if ( ! wp_verify_nonce( $category_form['edit_category_nonce'], 'edit_category_nonce_field' ) ) {
				echo json_encode( array('msg' => 'false', 'text' => 'Invalid nonce value.') );
				die();
			}
			
			/* VARIABLE DEFINED */
			global $wpdb;
			$gfb_categories_mst = $wpdb->prefix . 'gfb_categories_mst';
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';
			
			/* CHECK VALIDATION */
			if( trim($category_form['category_name']) == '' )
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Category title should not be blank.') );
				die();
			}
			else
			{
				/* CHECK WHEATHER CATEGORY EXISTS OR NOT */
				$check_exists = $wpdb->get_results( "SELECT category_id FROM ".$gfb_categories_mst." WHERE is_deleted=0 AND category_id='".trim($category_form['category_id'])."'", ARRAY_A );
				
				if( !empty( $check_exists ) ) 
				{
					$update_cat_arg = array(
						'category_name' => ''.wp_kses_post(trim($category_form['category_name'])).'',
						'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
						'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
						'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
					);
					
					$update_cat_where = array(
						'category_id' => ''.trim($category_form['category_id']).'',
					);
					
					$update_category = $wpdb->update($gfb_categories_mst, $update_cat_arg, $update_cat_where);
					
					if($update_category)
					{		
						echo json_encode( array('msg' => 'true', 'text' => 'Category updated successfully.', 'url' => admin_url().'admin.php?page=gravity-form-booking-categories') );
						die();
					}
					else
					{
						echo json_encode( array('msg' => 'false', 'text' => 'Category not updated successfully.') );
						die();
					}
				}
				else
				{
					echo json_encode( array('msg' => 'false', 'text' => 'No category found.') );
					die();	
				}
				
			}
			
			die();
			
		}
		
		/* AJAX FUNCTION TO DELETE CATEGORY */
		function gfbDeleteCategory() {
			/* VARIABLE DEFINED */
			global $wpdb;			
			$gfb_categories_mst = $wpdb->prefix . 'gfb_categories_mst';			
			$gfb_createdby = get_current_user_id();
			$gfb_ip = GFB_Core::gfbIpAddress();
			$msg = '';				
			
			$update_cat_arg = array(
				'is_deleted'    => 1,
				'modified_by' 	=> ''.wp_kses_post($gfb_createdby).'',
				'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
				'ip_address' 	=> ''.wp_kses_post($gfb_ip).'',
			);
			
			$update_cat_where = array(
				'category_id' => ''.trim($_POST['category_id']).'',
			);
			
			$update_category = $wpdb->update($gfb_categories_mst, $update_cat_arg, $update_cat_where);	
			
			if($update_category)
			{		
				echo json_encode( array('msg' => 'true', 'text' => 'Category deleted successfully.') );
				die();
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Category not deleted successfully.') );
				die();
			}
			
		}
		
	}
	
	global $gfbCategoryObj;
	$gfbCategoryObj = new GravityFormBookingCategory;
	
