<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



class GravityFormBookingMaster {
	
	
	public function __construct() {
		add_action( 'admin_init', array( &$this, 'gfbInitCore' ) );
		add_action( 'plugins_loaded', array( &$this, 'gfbSetClasses' ));
		add_action( 'admin_init', array( &$this, 'gfbSetPluginOptions' ));
		if(isset($_GET['page'])){
			$queryString = $_GET['page'];
			$regex  = "/gravity-form-booking/";
			if(preg_match($regex, $queryString, $match)) 
			{
				add_action( 'admin_enqueue_scripts', array( &$this, 'gfbSetAminScripts' ) );
				add_action( 'admin_enqueue_scripts', array( &$this, 'gfbSetAdminStyles' ) );
				add_action( 'admin_enqueue_scripts', array( &$this, 'gfbLoadChartJS' ) );
			} 
		}
	}
	
	/* Add Core used in init() functionality */
	public static function gfbInitCore() {
		$Cores = array (
			'Tables.php',			
			'Insert.php',
		);
		
		foreach( $Cores as $Core ) {			
			require_once( GFB_ADMIN_CORE . $Core );	
		}
	}
	
	/* Add Classes */
	public function gfbSetClasses() {
		$classes = array(
			'gravityformbooking.google.calendar.api.php',
			'gravityformbooking.calendar.cls.php',
			'gravityformbooking.chart.cls.php',
			'gravityformbooking.category.cls.php',
			'gravityformbooking.appointment.cls.php',
			'gravityformbooking.holiday.cls.php',
			'gravityformbooking.service.cls.php',
			'gravityformbooking.settings.cls.php',
			'gravityformbooking.timeslot.cls.php',
			'gravityformbooking.customer.cls.php',
			'gravityformbooking.staff.cls.php',
			'gravityformbooking.email.cls.php',
			'gravityformbooking.payment.cls.php',
			'gravityformbooking.fpdf.cls.php',
		);	
		
		foreach($classes as $class){
			require_once( GFB_ADMIN_CLASS . $class );	
		}
	}
	
	/* Add Options */
	public function gfbSetPluginOptions() {
		if (class_exists('GFCommon')){
			$currency = RGCurrency::get_currency(GFCommon::get_currency());
			if (!empty($currency['symbol_left'])){
				$currency_symbol = $currency['symbol_left'];
			}
			else{
				$currency_symbol = $currency['symbol_right'];
			}
			$optionArguments = array(
				//Company Options
				'gfb_company_name' 				 => get_bloginfo( 'name' ),
				'gfb_company_address' 				 => '',
				'gfb_company_contact_no' 			 => '',
				'gfb_company_email' 				 => get_bloginfo('admin_email'),
				'gfb_company_website' 				 => '',
				//Appointment Options
				'gfb_time_interval'   				 => '',
				'gfb_prior_days_book_appointment'   => '2',
				'gfb_prior_months_book_appointment' => '2',				
				//Email Config Options
				'gfb_sender_name' 					 => get_bloginfo( 'name' ),
				'gfb_sender_email' 				 => get_bloginfo('admin_email'),
				'gfb_enable_test_mode' 			 => 'sandbox',
				
				'gfb_payment_return_url' 			 => '',
				//Stripe Options
				'gfb_enable_test_mode_stripe' 		 => 'test',
				'gfb_stripe_test_secret_key' 		 => '',
				'gfb_stripe_test_publishable_key' 	 => '',
				'gfb_stripe_live_secret_key'		 => '',
				'gfb_stripe_live_publishable_key'   => '',
				//Calender Options
				'gfb_client_id' 				     => '',
				'gfb_client_secrete' 				 => '',				
				//Custom Css Options
				'gfb_custom_css'					 => '',
				'is_stripe'							 => ''
				
			);
			update_option( 'gfb_currency_code', GFCommon::get_currency() );	
			update_option( 'gfb_currency_symbol', $currency_symbol );	
			foreach($optionArguments as $options => $value){
				add_option( $options, $value );	
			}
		}
	}
	
	/* Add Plugin Scripts */
	public function gfbSetAminScripts( $hook_suffix ) {
		
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script('jquery-ui-Core');
		wp_enqueue_script('jquery-ui-accordion');
		wp_enqueue_script('wp-color-picker');
		wp_enqueue_media();
		
		wp_enqueue_script( 'gfb-magnific-popup-js', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js', array('jquery'), GFB_VERSION, true );
		
		if($hook_suffix == 'gravity-forms-booking_page_gravity-form-booking-staff') {
		wp_enqueue_script( 'gfb-ui-datepicker-js', 'https://code.jquery.com/ui/1.9.2/jquery-ui.js', array('jquery'), GFB_VERSION, true );
		}
		else{
			wp_enqueue_script( 'gfb-ui-datepicker-js', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'), GFB_VERSION, true );
		}
		
		wp_enqueue_script( 'gfb-timepicker-js', GFB_ADMIN_ASSET . 'js/jquery.timepicker.min.js', array('jquery'), GFB_VERSION, true );
		
		wp_enqueue_script( 'gfb-moment-min-js', 'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.21.0/moment.js', array('jquery'), GFB_VERSION, true );
		
		wp_enqueue_script( 'gfb-full-calendar-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js', array('jquery'), GFB_VERSION, true );
		
		wp_enqueue_script( 'gfb-chosen-js', 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.3/chosen.jquery.min.js', array('jquery'), GFB_VERSION, true );
		wp_enqueue_script( 'gfb-sweetalert-js', GFB_ASSET . 'js/sweetalert.min.js', array('jquery'), GFB_VERSION, true );
		
		wp_enqueue_script( 'gfb-validate-js', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js', array('jquery'), GFB_VERSION, true );
		
		//For Currency
		wp_enqueue_script( 'gfb-country-select-js', 'https://cdnjs.cloudflare.com/ajax/libs/country-select-js/2.0.1/js/countrySelect.min.js', array('jquery'), GFB_VERSION, true );
		
		/* Admin Script */
		wp_enqueue_script( 'gfb-admin-js', GFB_ADMIN_ASSET . 'js/admin-scripts.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-admin-js', 'js_admin_script_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'export_csv' => GFB_ADMIN_TAB . 'payment/payment-export-csv.php',
		) );
		
		/* Holiday JS */
		wp_enqueue_script( 'gfb-holiday-js', GFB_ADMIN_ASSET . 'js/holiday.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-holiday-js', 'js_holiday_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
		
		/* Category JS */
		wp_enqueue_script( 'gfb-category-js', GFB_ADMIN_ASSET . 'js/categories.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-category-js', 'js_category_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
		
		/* Services JS */
		wp_enqueue_script( 'gfb-services-js', GFB_ADMIN_ASSET . 'js/services.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-services-js', 'js_services_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );

		// this was added by developer53 on 01 sep 2021 and commented on 02 sep 2021 start
		wp_enqueue_script( 'gfb-select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js', array( 'jquery' ), GFB_VERSION, true );

		wp_enqueue_script( 'gfb-bootstrap-material-datetimepicker', GFB_ADMIN_ASSET . 'js/bootstrap-material-datetimepicker.js', array('jquery', 'gfb-moment-min-js'), GFB_VERSION, true );
		// this was added by developer53 on 01 sep 2021 and commented on 02 sep 2021 end
		
		/* Staff JS */
		wp_enqueue_script( 'gfb-staff-js', GFB_ADMIN_ASSET . 'js/staff.js', array('jquery'), GFB_VERSION . '&t='. gmdate('ymdhis'), true );
		wp_localize_script( 'gfb-staff-js', 'js_staff_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'slotinterval' => get_option('gfb_time_interval'),
		) );
		
		/* Settings JS */
		wp_enqueue_script( 'gfb-settings-js', GFB_ADMIN_ASSET . 'js/settings.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-settings-js', 'js_settings_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
		
		/* Customer JS */
		wp_enqueue_script( 'gfb-customer-js', GFB_ADMIN_ASSET . 'js/customers.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-customer-js', 'js_customers_ajax', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
		) );
		
		/* Time Slot JS */
		wp_enqueue_script( 'gfb-time-slot-js', GFB_ADMIN_ASSET . 'js/time-slot.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-time-slot-js', 'js_time_slot_ajax', array(
			'ajaxurl' 		=> admin_url( 'admin-ajax.php' ),
			'slotinterval'  => get_option('gfb_time_interval'),
		) );
		
		/* Calendar JS */
		wp_enqueue_script( 'gfb-calendar-js', GFB_ADMIN_ASSET . 'js/calendar.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-calendar-js', 'js_calendar_js', array(
			'ajaxurl' 	 => admin_url( 'admin-ajax.php' ),
			'mindate' 	 => GravityFormBookingFrontEnd::gfbMinDate(),
			'maxdate' 	 => GravityFormBookingFrontEnd::gfbMaxDate(),
			'holidayarr' => GravityFormBookingFrontEnd::gfbHolidayJson(),
		));
		
		/* Appointment JS */
		wp_enqueue_script( 'gfb-appointment-js', GFB_ADMIN_ASSET . 'js/appointment.js', array('jquery'), GFB_VERSION, true );
		
		wp_localize_script( 'gfb-appointment-js', 'js_appointment_js', array(
			'ajaxurl' 	 => admin_url( 'admin-ajax.php' ),
			'mindate' 	 => GravityFormBookingFrontEnd::gfbMinDate(),
			'maxdate' 	 => GravityFormBookingFrontEnd::gfbMaxDate(),
			'holidayarr' => GravityFormBookingFrontEnd::gfbHolidayJson(),
			'adloader'   => GFB_INLINE_LOADER
		));
		
		/* TINY TOGGLE */
		wp_enqueue_script( 'gfb-toggle-js', GFB_ADMIN_ASSET . 'js/tinytools.toggleswitch.min.js', array('jquery'), GFB_VERSION, true );
		
		/* Email Notification JS */
		wp_enqueue_script( 'gfb-email-notification-js', GFB_ADMIN_ASSET . 'js/email-notification.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-email-notification-js', 'js_email_notify_ajax', array(
			'ajaxurl' 		=> admin_url( 'admin-ajax.php' ),
			'slotinterval'  => get_option('gfb_time_interval'),
		) );		
	}
	
	/* Add Plugin Styles */
	public function gfbSetAdminStyles(){
		
		wp_enqueue_style( 'wp-color-picker' );
		
		wp_enqueue_style('gfb-ui-datepicker-css', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css', array(), GFB_VERSION);
		
		wp_enqueue_style( 'gfb-timepicker-css', GFB_ADMIN_ASSET . 'css/jquery.timepicker.min.css', array(), GFB_VERSION);
		
		wp_enqueue_style( 'gfb-magnific-popup-css', 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css', array(), GFB_VERSION );
		
		wp_enqueue_style( 'gfb-chosen-css', 'https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.3/chosen.min.css', array(), GFB_VERSION );
					
		wp_enqueue_style( 'gfb-full-calendar-style', 'https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css', array(), GFB_VERSION);
		
		//For currency
		wp_enqueue_style( 'gfb-country-select-style', 'https://cdnjs.cloudflare.com/ajax/libs/country-select-js/2.0.1/css/countrySelect.min.css', array(), GFB_VERSION);
		
		wp_enqueue_style( 'gfb-sweetalert-style', GFB_ASSET . 'css/sweetalert.css', array(), GFB_VERSION);
		
		wp_enqueue_style( 'gfb-toggle-css', GFB_ADMIN_ASSET . 'css/tinytools.toggleswitch.min.css', array(), GFB_VERSION );
		
		wp_enqueue_style( 'gfb-admin-style-css', GFB_ADMIN_ASSET . 'css/admin.style.css', array(), GFB_VERSION );

		// this was added by developer53 on 01 sep 2021 and commented on 02 sep 2021 start
		wp_enqueue_style( 'gfb-select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css', array(), GFB_VERSION );

		wp_enqueue_style( 'gfb-bootstrap-material-datetimepicker', GFB_ADMIN_ASSET . 'css/bootstrap-material-datetimepicker.css', array(), GFB_VERSION);
		// this was added by developer53 on 01 sep 2021 and commented on 02 sep 2021 end	
		
	}
	
	function gfbLoadChartJS($hook) {
        $pth = 'toplevel_page_gravity-form-booking-menu';
        if($hook != $pth) {
        	return false;
        }
		
       	/* Charts.js */
		wp_enqueue_script( 'gfb-open-chart-js', 'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js', array('jquery'), GFB_VERSION, true );
		
		global $charts;
		
		/* Payment Chart By Payment Type */
		$getpaymentdetails = $charts->gfbGetPayment();	
		$getpaymentdetails_json = json_decode($getpaymentdetails, true);
		
		
		/* Total appointments & earnings by services */
		$getEarByService = $charts->gfbGetEarBokService();	
		$getEarByService_json = json_decode($getEarByService, true);
		
		/* Appointment Chart By Appointment Status */
		$getappointmentstatus = $charts->gfbAppoinmentStatus();
		$getappointmentstatus_json = json_decode($getappointmentstatus, true);
				//print_r($getappointmentstatus_json);exit;
		/* Earning & Total appointments by staff */
		$getBookByStaff = $charts->gfbGetEarNBookByStaff();
		$getStaffDataChart = json_decode($getBookByStaff,true);
		
		/* Chart JS */		
		wp_enqueue_script( 'gfb-chart-js', GFB_ADMIN_ASSET . 'js/charts.js', array('jquery'), GFB_VERSION, true );
		wp_localize_script( 'gfb-chart-js', 'js_chart_ajax', array(
			'ajaxurl' 			=> admin_url( 'admin-ajax.php' ),
			'getpaymentdetails_amount' => $getpaymentdetails_json["amount"],
			'getpaymentdetails_type' => $getpaymentdetails_json["ptype"],
			'ear_service' => ''.$getEarByService_json["service"].'',
			'app_service' => ''.$getEarByService_json["appointments"].'',
			'amt_service' => ''.$getEarByService_json["amount"].'',			
			'getappointmentstatus_appoint' => $getappointmentstatus_json["appointment"],
			'getappointmentstatus_status' => $getappointmentstatus_json["status"],
			'getStaffDataChart_staff' => $getStaffDataChart['staff'],
			'getStaffDataChart_amount' => $getStaffDataChart['amount'],
			'getStaffDataChart_appointments' => $getStaffDataChart['appointments'],
		) );
		
	}
}

global $master;
$master = new GravityFormBookingMaster();