<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }


	
class GravityFormBookingCalendar {
	public $mainCalssFunction;
	function __construct() {
		$this->mainCalssFunction = new gfb_bookings_addon();
		add_action( 'wp_ajax_get_total_appointments',  array(&$this, 'gfbGetCountTotalAppointments') );	

		add_filter( 'gform_entry_field_value', array($this, 'gfb_detail_in_entry'), 99, 4 );
	}

	public function gfb_detail_in_entry( $value, $field, $entry, $form ) {
		
		$entry_id = $entry['id'];
		$slot = gform_get_meta($entry_id, 'slot_qty');
		$provider_id = gform_get_meta($entry_id, 'provider_id');
		$staff_timezone = '';

		if ( get_option( 'gfb_staff_timezone_' . @$this->provider_id ) != '' ) {
			$staff_timezone = get_option( 'gfb_staff_timezone_' . $this->provider_id );
		} else {
			$staff_timezone = wp_timezone_string();
		}

		if ( 'gfb_appointment_calendar' ==  $field->get_input_type() ) {
			$value .= '<ul>';
			
			if ( $staff_timezone != '' && $provider_id != '' ) {
            	$value .= '<li>' . __( 'Timezone:', 'gfb' ) . ' ' . $staff_timezone . '</li>';	
            }

			if ($slot != '') {
                $value .= '<li>' . __( 'Slot Capacity:', 'gfb' ) . ' ' . $slot . '</li>';
            }
            
			$value .= '</ul>';
		}

		return $value;
	}
	
	/* Category List */
	function executeEditStaff($staff_id){
		
		global $wpdb;
		
		$staff_tbl = $wpdb->prefix . 'gfb_staff_mst';
				
		$staff_google_data = $wpdb->get_results( 'SELECT staff_gcal_data FROM '.$staff_tbl. ' where staff_id="'.$staff_id.'" and is_deleted=0', ARRAY_A );
		
		if ( empty($staff_google_data[0]['staff_gcal_data'] )) 
		{
			$authUrl = $this->createAuthUrl($staff_id);
		}
		else
		{
			$authUrl = null;
		}
		
		return $authUrl;
	}
	
	function createAuthUrl($staff_id)
	{			
		$client_id = get_option('gfb_client_id');
		$client_secrete = get_option('gfb_client-secrete');
		
		$redirect_url = get_site_url().'/wp-admin/admin.php?page=gravity-form-booking-staff';
		$state = 	base64_encode( $staff_id );

		$auth_url = 'https://accounts.google.com/o/oauth2/v2/auth?scope=' . urlencode('https://www.googleapis.com/auth/calendar') . '&redirect_uri=' . urlencode($redirect_url) . '&response_type=code&client_id=' . $client_id . '&access_type=offline&state='.$state.'&prompt=consent';
		
		return $auth_url;
	}
	
	// revoke permission from google account 
	function logout($staff_id)
	{
		global $wpdb;
		$capi = new GFB_GoogleCalendarApi();
		
		$staff_tbl = $wpdb->prefix . 'gfb_staff_mst';
		$staff_google_data = $wpdb->get_results( 'SELECT staff_gcal_data FROM '.$staff_tbl. ' where staff_id='.$staff_id.' and is_deleted=0', ARRAY_A );
				
		$google_data = json_decode($staff_google_data[0]['staff_gcal_data'],true);
		$result = $capi->revokeToken($google_data['refresh_token']);
		$updateStaffData = array( 'staff_gcal_data' =>'');
		$where = array ('staff_id' => $staff_id);
		
		$update = $wpdb->update($staff_tbl,$updateStaffData,$where);
	}
	
	
	/* Get Values for Pending, Cancelled & Completed Appointments */
	function gfbGetCountTotalAppointments() {
		global $wpdb;
		global $gfbFrontEndObj;
		$gfb_appointments_mst = $wpdb->prefix . 'gfb_appointments_mst';
		$gfb_staff_holiday_mapping = $wpdb->prefix . 'gfb_staff_holiday_mapping';
		$gfb_holiday_mst = $wpdb->prefix . 'gfb_holiday_mst';
		$appointments = array();
		$appointmentDates = array();
		$maxdate1 = $gfbFrontEndObj->gfbMaxDate();
		$maxdate1 = DateTime::createFromFormat("d F, Y", $maxdate1);
		$maxdate = $maxdate1->format('Y-m-d');
		
		$earlierDate = date('Y-m-01');
		$startdate = strtotime($earlierDate);
		$enddate = strtotime($maxdate);
		
		if( empty($_POST["staff_id"]) ) { //if-1
		
			/* Get holiday array between start date and end date */
			$holiday_array = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_holiday_mst." WHERE is_deleted=0 AND holiday_date BETWEEN '".$earlierDate."' AND '".$maxdate."'", ARRAY_A );
			
			
			
			$holiday_date = $this->mainCalssFunction->gfb_array_column($holiday_array, 'holiday_date');
						
			/* Get Appointment date array from start date to end date */
			for($i=$startdate; $i<$enddate; $i+=(60 * 60 * 24))
			{
				array_push($appointmentDates, date("Y-m-d", $i)); 
			}
						
			/* Get Appoinments count */		
			foreach($appointmentDates as $appDate) {
				
				$count_appointments = $wpdb->get_results( "SELECT am.appointment_date as start, count(am1.appointment_id) as pending, count(am4.appointment_id) as awaiting, count(am2.appointment_id) as cancelled, count(am3.appointment_id) as completed
	FROM ".$gfb_appointments_mst." as am
	LEFT JOIN ".$gfb_appointments_mst." as am1 on am.appointment_id=am1.appointment_id and am1.status=1 and am1.is_deleted=0
	LEFT JOIN ".$gfb_appointments_mst." as am2 on am.appointment_id=am2.appointment_id and am2.status=3 and am2.is_deleted=0
	LEFT JOIN ".$gfb_appointments_mst." as am3 on am.appointment_id=am3.appointment_id and am3.status=4 and am3.is_deleted=0
	LEFT JOIN ".$gfb_appointments_mst." as am4 on am.appointment_id=am4.appointment_id and am4.status=2 and am4.is_deleted=0
	WHERE am.is_deleted=0 AND am.appointment_date = '".$appDate."'", ARRAY_A );	
	
				if($count_appointments && !in_array($appDate, $holiday_date))
				{
					$appointmentArr = array();					
					foreach($count_appointments[0] as $key => $value) {	
						$appointmentArr[$key] = ''.$value.'';
						
						if($key == 'pending') {
							$appointmentArr['pending_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(1)."";	
						}
						
						if($key == 'awaiting') {
							$appointmentArr['awaiting_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(2)."";	
						}
						
						if($key == 'cancelled') {
							$appointmentArr['cancelled_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(3)."";	
						}
						
						if($key == 'completed') {
							$appointmentArr['completed_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(4)."";	
						}
					}	
					
					if( $appointmentArr['start'] <> '' ){
						array_push($appointments, $appointmentArr);	
					}
											
				}
	
			}
						
			/* Holiday Array */
			if($holiday_array)
			{
				foreach($holiday_array as $holidays)	
				{					
					$holidayArr = array();				
					$holidayArr['start']=$holidays['holiday_date'];
					$holidayArr['holiday']='1';
					array_push($appointments, $holidayArr);						
				}
			}
		} // if-1
		else { //else-1
				
			/* Get holiday array between start date and end date */
			$holiday_array = $wpdb->get_results( "SELECT holiday_date FROM ".$gfb_staff_holiday_mapping." WHERE is_deleted=0 AND staff_id=".$_POST["staff_id"]." AND holiday_date BETWEEN '".$earlierDate."' AND '".$maxdate."'", ARRAY_A );
			
			$holiday_date = $this->mainCalssFunction->gfb_array_column($holiday_array, 'holiday_date');
			

			
			/* Get Appointment date array from start date to end date */
			for($i = $startdate; $i < $enddate; $i = strtotime("+1 day", $i))
			{
				array_push($appointmentDates, date("Y-m-d", $i));
			}
					
			/* Get Appoinments count */		
			foreach($appointmentDates as $appDate) {
				
				$count_appointments = $wpdb->get_results( "SELECT am.appointment_date as start, count(am1.appointment_id) as pending,count(am4.appointment_id) as awaiting, count(am2.appointment_id) as cancelled, count(am3.appointment_id) as completed
	FROM ".$gfb_appointments_mst." as am
	LEFT JOIN ".$gfb_appointments_mst." as am1 on am.appointment_id=am1.appointment_id and am1.status=1
	LEFT JOIN ".$gfb_appointments_mst." as am2 on am.appointment_id=am2.appointment_id and am2.status=3
	LEFT JOIN ".$gfb_appointments_mst." as am3 on am.appointment_id=am3.appointment_id and am3.status=4
	LEFT JOIN ".$gfb_appointments_mst." as am4 on am.appointment_id=am4.appointment_id and am4.status=2 and am4.is_deleted=0
	WHERE am.is_deleted=0 AND am.staff_id=".$_POST["staff_id"]." AND am.appointment_date = '".$appDate."'", ARRAY_A );	
	
				if($count_appointments && !in_array($appDate, $holiday_date))
				{
					$appointmentArr = array();					
					
					foreach($count_appointments[0] as $key => $value) {	
						$appointmentArr[$key] = ''.$value.'';
						
						if($key == 'pending') {
							$appointmentArr['pending_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(1)."";	
						}
						
						if($key == 'awaiting') {
							$appointmentArr['awaiting_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(2)."";	
						}
						
						if($key == 'cancelled') {
							$appointmentArr['cancelled_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(3)."";	
						}
						
						if($key == 'completed') {
							$appointmentArr['completed_url']= ''.admin_url()."admin.php?page=gravity-form-booking-appointments&appdate='".base64_encode($appDate)."'&status=".base64_encode(4)."";	
						}
					}	
					
					if( $appointmentArr['start'] <> '' ){
						array_push($appointments, $appointmentArr);	
					}
											
				}
	
			}
						
			/* Holiday Array */
			if($holiday_array)
			{
				foreach($holiday_array as $holidays)	
				{					
					$holidayArr = array();				
					$holidayArr['start']=$holidays['holiday_date'];
					$holidayArr['holiday']='1';
					array_push($appointments, $holidayArr);						
				}
			}
			
		} //else-1
					
		echo json_encode($appointments);
		die();
	}
	
	
}

global $gfb_objCalendar;
$gfb_objCalendar = new GravityFormBookingCalendar();