<?php
/* Exit if accessed directly */
if ( ! defined( 'ABSPATH' ) ) { exit; }



require_once(GFB_ADMIN_DIR.'classes/gravityformbooking.google.calendar.api.php');
require_once(GFB_ADMIN_DIR.'classes/gravityformbooking.staff.cls.php');

	
	class GravityFormBookingSettings {
		
		function __construct() {
			
			add_action( 'wp_ajax_gfb_update_settings_option', array( $this, 'gfb_update_settings' ) );
		}
		
		function gfb_update_options($option_name, $option_value) {
			
			return update_option($option_name, $option_value);			
		}
		
		function gfb_update_settings() {
						
			parse_str($_POST['setting_option'], $options);

			// echo '<pre>';
			// print_r( $options );
			// echo '</pre>';

			if ( isset( $options['gfb_price_policy'] ) ) {
				update_option( 'gfb_price_policy', $options['gfb_price_policy'] );
			}

			if ( isset( $options['gfb_appointment_policy'] ) ) {
				update_option( 'gfb_appointment_policy', $options['gfb_appointment_policy'] );
			}

			if ( isset( $options['gfb_cal_gradient_color_top'] ) ) {
				update_option( 'gfb_cal_gradient_color_top', $options['gfb_cal_gradient_color_top'] );
			}

			if ( isset( $options['gfb_cal_gradient_color_bottom'] ) ) {
				update_option( 'gfb_cal_gradient_color_bottom', $options['gfb_cal_gradient_color_bottom'] );
			}

			if ( isset( $options['gfb_cal_text_color'] ) ) {
				update_option( 'gfb_cal_text_color', $options['gfb_cal_text_color'] );
			}

			if ( isset( $options['gfb_cal_text_bg_color'] ) ) {
				update_option( 'gfb_cal_text_bg_color', $options['gfb_cal_text_bg_color'] );
			}

			if ( isset( $options['gfb_cal_text_selected_color'] ) ) {
				update_option( 'gfb_cal_text_selected_color', $options['gfb_cal_text_selected_color'] );
			}

			if ( isset( $options['gfb_cal_available_day_bg_color'] ) ) {
				update_option( 'gfb_cal_available_day_bg_color', $options['gfb_cal_available_day_bg_color'] );
			}

			if ( isset( $options['gfb_cal_available_day_text_color'] ) ) {
				update_option( 'gfb_cal_available_day_text_color', $options['gfb_cal_available_day_text_color'] );
			}

			if ( isset( $options['hdn_appointment_setting_form'] ) ) {

				if( !in_array("gfb_fullday_booking",$options) ) {
					update_option("gfb_fullday_booking", 0 );
				} else {
					update_option("gfb_fullday_booking", 1 );
				}
			}
			
			if ( isset( $_post['break_for_mon'] ) ) {
				update_option( 'break_for_mon', $_post['break_for_mon'] );
			} else {
				update_option( 'break_for_mon', array() );
			}

			if( isset($_POST["pyaction"]) && $_POST["pyaction"] == "paymentsettings" ) {				
				

				if( !in_array("is_stripe",$options) ) {
					update_option("is_stripe",0);
				}
				
				if( !in_array("is_cod",$options) ) {
					update_option("is_cod",0);
				}
			}
			
			if( !in_array("gfb_gc_2way_sync",$options) ) {
				update_option("gfb_gc_2way_sync",0);
			}
			if($options['gfb_gc_2way_sync']==1){
				update_option("gfb_gc_2way_sync",1);
				$capi = new GFB_GoogleCalendarApi();
				$gfbStaff = new GravityFormBookingStaff();

				global $wpdb;
				$gfb_staff_mst = $wpdb->prefix . 'gfb_staff_mst';
				$gfb_staff_gcal_events = $wpdb->prefix . 'gfb_staff_gcal_events';			
				$staff_result = $wpdb->get_results( 'SELECT staff_id, staff_gcal_data,staff_gcal_id,staff_gcal_watch_data FROM '.$gfb_staff_mst.' WHERE is_deleted=0 ', ARRAY_A );
				if(!empty($staff_result))
				{
					foreach($staff_result as $res)
					{
						if($res['staff_gcal_data']!='')
						{

							$staff_id = $res['staff_id'];
							$access_token = json_decode($res['staff_gcal_data'])->access_token;
							$resource_id = json_decode($res['staff_gcal_watch_data'])->resourceId;
							$calendar_id = $res['staff_gcal_id'];
							$limit_events = $options['gfb_limit_gc_events'];
							
							if( $gfbStaff->gfbIsTokenExpire($staff_id)) {

								$jsontoken = json_decode( $gfbStaff->gfbStaffGoogleDataEncode($staff_id ));
								if( !empty( $jsontoken ) ) {
												
									$gfbStaff->gfbRefreshToken($jsontoken->refresh_token,$staff_id);
									$gcalJSONData = $gfbStaff->gfbStaffGoogleDataEncode($staff_id);
									$gcalData = json_decode($gcalJSONData);
									//print_r($gcalData);exit;
									$access_token = $gcalData->access_token;
								}				
							}
							
							
							
							
							$events = $capi->getEvents($access_token, $calendar_id, $limit_events);

							
							if(isset($events['nextSyncToken']))
							{
								$nextSyncToken = $events['nextSyncToken'];
							}
							else if(isset($events['nextPageToken']))
							{
								$nextSyncToken = $events['nextPageToken'];
							}
							
							//print_r($events);exit;
							if(!empty($events['items']))
							{

								foreach($events['items'] as $item)
								{
									//check exists
									$checkExists = $wpdb->get_results("SELECT gcalid FROM $gfb_staff_gcal_events where eventid='".$item['id']."' and is_deleted=0");
							
									if(empty($checkExists))
									{
										
										//save staff gcal events
										$eventArray = array(
											'eventid' => $item['id'],
											'summary' => ($item['summary'] == '') ? '-' : $item['summary'],
											'start' => date('Y-m-d H:i:s',strtotime($item['start']['dateTime'])),
											'end' => date('Y-m-d H:i:s',strtotime($item['end']['dateTime'])),
											'status' => $item['status'],
											'htmllink' => $item['htmlLink'],
											'timezone' => $item['start']['timeZone'],
											'created_by' => ''.wp_kses_post(get_current_user_id()).'',
											'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
											'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).''
										);
										//print_r($eventArray);
										if(!$wpdb->insert($gfb_staff_gcal_events, $eventArray))
										{

											echo json_encode( array('msg' => 'false', 'text' => __('Sorry! Error occurred while saving google calendar events.', 'gfb') ) );
											die();
										}
									}
								}
							}

							/* if( $gfbStaff->gfbIsWatchRequestExpire($staff_id)) {
								
								//create watch request 	
								$wathResponse = $capi->createWatchRequest($access_token, $calendar_id);
								
								$channel_id = json_decode($wathResponse)->id;
								$resource_id = json_decode($wathResponse)->resourceId;
								//update staff data
								$edit_staff_arg = array(
									'gcal_channel_id' => ''.wp_kses_post($channel_id).'',
									'gcal_resource_id' => ''.wp_kses_post($resource_id).'',
									'gcal_next_sync_token' => ''.wp_kses_post($nextSyncToken).'',
									'staff_gcal_watch_data' => ''.wp_kses_post($wathResponse).'',
									'modified_by' => ''.wp_kses_post(get_current_user_id()).'',
									'modified_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).'',
								);
								
								$where_arg = array(
									'staff_id' 	=> ''.wp_kses_post($staff_id).'',
								);							
								if(!$wpdb->update($gfb_staff_mst, $edit_staff_arg, $where_arg))
								{ 
									echo json_encode( array('msg' => 'false', 'text' => __('Sorry! Error occurred while saving watch response.', 'gfb')));
									die();
								}		
							} */
							
							/* //save nextSyncToken
							$gfb_staff_gcal_watch = $wpdb->prefix . 'gfb_staff_gcal_watch';
							$checkExists = $wpdb->get_results("SELECT staff_resource_id FROM $gfb_staff_gcal_watch where is_deleted=0 and next_sync_token='$nextSyncToken'");
							//print_r($checkExists);exit;
							
							if(empty($checkExists))
							{
								$watchData = array(
									'staff_id' 	=> ''.wp_kses_post($staff_id).'',
									'resource_id' => $resource_id,
									'next_sync_token' => $nextSyncToken,
									'created_by' => ''.wp_kses_post(get_current_user_id()).'',
									'created_date' => ''.wp_kses_post(current_time("Y-m-d H:i:s")).'',
									'ip_address' => ''.wp_kses_post(GFB_Core::gfbIpAddress()).''
								);

								//print_r($eventArray);
								if(!$wpdb->insert($gfb_staff_gcal_watch, $watchData))
								{

									echo json_encode( array('msg' => 'false', 'text' => __('Sorry! Error occurred while saving google calendar watch data.', 'gfb') ) );
									die();
								}
							} */
						}
					}
				}
			}

			foreach( $options as $key => $value ){
				update_option($key, $value);
			}
			
			if(update_option == true) {
				echo json_encode( array('msg' => 'true', 'text' => 'Settings Saved.') );
				die();				
			}
			else
			{
				echo json_encode( array('msg' => 'false', 'text' => 'Settings Not Saved.') );
				die();					
			}
		}
		
		/* Date Format List */
		function gfbDateFormat() {
			
			$dateFormat = array(
				'F j, Y',
				'Y-m-d',
				'm/d/Y',
				'd/m/Y'				
			);
			
			return json_encode($dateFormat);				
		}
		
		/* Time Format List */
		function gfbTimeFormat() {
			
			$timeFormat = array(
				'g:i a',
				'g:i A',
				'H:i',
				'H:i:s'				
			);
			
			return json_encode($timeFormat);
				
		}
		
		/* Time Zone */
		function gfbTimeZone()
		{
			$zones_array = array();
			$timestamp = time();
			
			foreach(timezone_identifiers_list() as $key => $zone) 
			{
				$zones_array[$key]['zone'] = $zone;
				$zones_array[$key]['diff_from_GMT'] = 'UTC/GMT ' . date('P', $timestamp);
			}
			
			return json_encode($zones_array);	
		}
		
	}
	
	global $gfbSettingsObj;
	$gfbSettingsObj = new GravityFormBookingSettings();
	
