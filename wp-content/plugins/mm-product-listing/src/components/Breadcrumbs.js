const Breadcrumbs = ({ type, productName = "products" }) => {
  const urlSource = "https://flooringatlanta-stg.mm-dev.agency/";
  return (
    <div id="breadcrumbs">
      <div class="container">
        <span>
          <span>
            <a href={urlSource}>Home &gt;</a>
          </span>
          <span>
            <a href={`${urlSource}/flooring`}>Flooring &gt;</a>
          </span>
          <span>
            <a href={`${urlSource}/flooring/${type}`}>
              {type.charAt(0).toUpperCase() + type.slice(1)} &gt;
            </a>
          </span>
          {productName === "products" ? (
            <span class="breadcrumb_last" aria-current="page">
              {type.charAt(0).toUpperCase() + type.slice(1)} Products
            </span>
          ) : (
            <>
              <span>
                <a href={`${urlSource}/flooring/${type}/products`}>
                  Products &gt;
                </a>
              </span>
              <span class="breadcrumb_last" aria-current="page">
                {productName}
              </span>
            </>
          )}
        </span>
      </div>
    </div>
  );
};

export default Breadcrumbs;
