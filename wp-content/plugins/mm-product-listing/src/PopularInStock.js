import React, { useState, useEffect } from "react";
import "./Products.css";
import ProductLoop from "./components/ProductLoop";

function PopularInStock() {
  const [products, setProducts] = useState([]);
  const [paged, setPage] = useState(1);
  const [itemsPerPage, setItemsPerPage] = useState(4);
  const [loading, setLoading] = useState(true);
  const wpProductFilter = window.wpProductFilter;
  const wpProductInSTock = window.wpProductInSTock;
  const inStock = window.wpProductInSTock.inStock;

  const catColor = wpProductInSTock.color_facet
    ? wpProductInSTock.color_facet.toLowerCase()
    : "";
  const catBrandFacet = wpProductInSTock.brand_facet
    ? wpProductInSTock.brand_facet.toLowerCase()
    : "";
  const catCollectionFacet = wpProductInSTock.collection_facet
    ? wpProductInSTock.collection_facet.toLowerCase()
    : "";
  const catStyleFacet = wpProductInSTock.style_facet
    ? wpProductInSTock.style_facet.toLowerCase()
    : "";
  const catShadeFacet = wpProductInSTock.shade_facet
    ? wpProductInSTock.shade_facet.toLowerCase()
    : "";
  const catShapeFacet = wpProductInSTock.shape_facet
    ? wpProductInSTock.shape_facet.toLowerCase()
    : "";
  const catFiberFacet = wpProductInSTock.fiber
    ? wpProductInSTock.fiber.toLowerCase()
    : "";

  const shortcodeLoadFacets = {
    color_facet: catColor,
    brand_facet: catBrandFacet,
    collection_facet: catCollectionFacet,
    style_facet: catStyleFacet,
    shade_facet: catShadeFacet,
    shape_facet: catShapeFacet,
    fiber: catFiberFacet,
  };

  let shortcodeFacets = Object.fromEntries(
    Object.entries(shortcodeLoadFacets).filter(([_, v]) => v != "")
  );

  useEffect(() => {
    fetchProducts();
  }, [paged]);

  const fetchProducts = async () => {
    const query = new URLSearchParams({
      category: wpProductInSTock.category,
      ...shortcodeFacets,
      paged,
      limit: itemsPerPage,
      in_stock: inStock,
    });

    try {
      const response = await fetch(`${wpProductFilter.apiEndpoint}?${query}`);
      const data = await response.json();
      setProducts(data.products_group_by);
    } catch (error) {
      console.error("Error fetching products:", error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      {Object.keys(products).length > 0 && (
        <div className="product-list-container">
          <div className="col-lg-12 col-md-12 col-sm-12 ">
            <div className="product-plp-grid product-grid swatch special_product_grid">
              {Object.keys(products).map((product, i) => {
                return (
                  <ProductLoop key={i} products={products[product]} rows={4} />
                );
              })}
            </div>
          </div>
        </div>
      )}

      {Object.keys(products).length == 0 && loading == false && (
        <div className="product-list-container">
          <div className="col-lg-12 col-md-12 col-sm-12 ">
            <h2>No Products Available</h2>
          </div>
        </div>
      )}
    </>
  );
}

export default PopularInStock;
